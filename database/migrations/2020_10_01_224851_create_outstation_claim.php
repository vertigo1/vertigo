<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutstationClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outstation_claim', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_id')->unique();
            $table->string('staff_id');
            $table->string('job_number');
            $table->string('description');
            $table->string('location');
            $table->string('staff_type');
            $table->string('accom_type');
            $table->string('allowance_rate');
            $table->date('travel_date_mob');
            $table->date('travel_date_demob');
            $table->date('outstation_date_start');
            $table->date('outstation_date_end');
            $table->string('total_allowance_day');
            $table->string('total_accommodation_day');
            $table->string('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outstation_claim');
    }
}
