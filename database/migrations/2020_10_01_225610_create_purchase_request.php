<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_id')->unique();
            $table->string('staff_id');
            $table->string('job_number');
            $table->date('date');
            $table->string('amount');
            $table->string('description');
            $table->string('supplier');
            $table->string('category');
            $table->boolean('status');
            $table->string('bank_slip');
            $table->date('payment_date');
            $table->boolean('payment_status');
            $table->string('payment_remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_request');
    }
}
