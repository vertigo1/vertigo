<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalAmountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_amount', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('staff_id');
            $table->float('January', 8, 2);
            $table->float('February', 8, 2);
            $table->float('March', 8, 2);
            $table->float('April', 8, 2);
            $table->float('May', 8, 2);
            $table->float('June', 8, 2);
            $table->float('July', 8, 2);
            $table->float('August', 8, 2);
            $table->float('September', 8, 2);
            $table->float('October', 8, 2);
            $table->float('November', 8, 2);
            $table->float('December', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_amount');
    }
}
