<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_application', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_id')->unique();
            $table->string('staff_id');
            $table->date('date_apply');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('total_day');
            $table->string('type_leave');
            $table->string('reason');
            $table->string('staff_handover');
            $table->string('work_scope');
            $table->date('approved_date');
            $table->date('update_date');
            $table->boolean('status');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_application');
    }
}
