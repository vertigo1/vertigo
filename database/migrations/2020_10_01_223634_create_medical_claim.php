<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_claim', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_id')->unique();
            $table->string('staff_id');
            $table->string('month_apply');
            $table->string('amount');
            $table->string('clinic_name');
            $table->string('reason');
            $table->date('date_receipt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_claim');
    }
}
