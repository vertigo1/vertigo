<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMileageClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mileage_claim', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_id');
            $table->string('staff_id');
            $table->string('job_number');
            $table->string('car_availability');
            $table->date('date_travel');
            $table->string('from');
            $table->string('to');
            $table->string('description');
            $table->string('distant');
            $table->string('fuel_price');
            $table->string('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mileage_claim');
    }
}
