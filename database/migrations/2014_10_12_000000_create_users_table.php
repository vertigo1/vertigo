<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('staff_id')->unique();
            $table->string('position');
            $table->string('salary');
            $table->string('site_allowance');
            $table->string('medical_claim');
            $table->boolean('in_liue');
            $table->string('annual_leave');
            $table->string('medical_leave');
            $table->string('liue_leave');
            $table->string('region');
            $table->string('password');
            $table->string('accessibility');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
