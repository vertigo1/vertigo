<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutgoingPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgoing_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_id');
            $table->string('subcont_name');
            $table->string('description');
            $table->date('expected_date');
            $table->string('amount');
            $table->boolean('status');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgoing_payment');
    }
}
