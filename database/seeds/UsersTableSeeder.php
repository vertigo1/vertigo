<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        if(count($users) < 1){
            $items = [   
                [
                    'staff_id' => 'admin',
                    'position' => 'HR',
                    'salary' => '5,000.00',
                    'site_allowance' => '500.00',
                    'medical_claim' => '50.00',
                    'in_liue' => '1',
                    'annual_leave' => '14',
                    'medical_leave' => '10',
                    'liue_leave' => '10',
                    'region' => 'KMM',
                    'password' => Hash::make('admin'),
                    'accessibility' => '1,2,3,4',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
            ];

            foreach ($items as $item) {
                $user = User::create($item);
            }
        }
    }
}
