<?php

use Illuminate\Database\Seeder;
use Modules\Form\Entities\MedicalAmount;
use Carbon\Carbon;

class MedicalAmountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amount = MedicalAmount::all();

        if(count($amount) < 1){
            $items = [   
                [
                    'staff_id' => 'admin',
                    'January' => 50,
                    'February' => 50,
                    'March' => 50,
                    'April' => 50,
                    'May' => 50,
                    'June' => 50,
                    'July' => 50,
                    'August' => 50,
                    'September' => 50,
                    'October' => 50,
                    'November' => 50,
                    'December' => 50,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
            ];

            foreach ($items as $item) {
                $amount = MedicalAmount::create($item);
            }
        }
    }
}
