<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    if(auth()->check()){
        return redirect('/form/index');
    }else{
        return view('login');
    }
});

Route::get('/login', 'Auth\LoginController@login_view')->name('login');
Route::post('/login', 'Auth\LoginController@login');


Route::middleware('auth')->group(function () {
    Route::get('logout', 'Auth\LoginController@logout');
});
