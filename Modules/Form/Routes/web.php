<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('form')->group(function() {
    Route::middleware(['auth'])->group(function () {
        Route::get('/index', 'FormController@index')->name('form');

        //Purchase Order
        Route::get('/purchase_order/add', 'FormController@add_purchase_order')->name('purchase_order.add');
        Route::post('/purchase_order/add', 'FormController@post_purchase_order')->name('purchase_order.post');
        // ------------------------------------------------------ //

        //Purchase Request
        Route::get('/purchase_request/add', 'FormController@add_purchase_request')->name('purchase_request.add');
        Route::post('/purchase_request/add', 'FormController@post_purchase_request')->name('purchase_request.post');
        // ------------------------------------------------------ //

        //Medical Claim
        Route::get('/medical_claim/add', 'FormController@add_medical_claim')->name('medical_claim.add');
        Route::post('/medical_claim/add', 'FormController@post_medical_claim')->name('medical_claim.post');
        // ------------------------------------------------------ //

        //Off In Liue
        Route::get('/in_liue/add', 'FormController@add_in_liue')->name('in_liue.add');
        Route::post('/in_liue/add', 'FormController@post_in_liue')->name('in_liue.post');
        // ------------------------------------------------------ //

        //Outstation Claim
        Route::get('/outstation_claim/add', 'FormController@add_outstation_claim')->name('outstation_claim.add');
        Route::post('/outstation_claim/add', 'FormController@post_outstation_claim')->name('outstation_claim.post');
        // ------------------------------------------------------ //

        //Leave Application
        Route::get('/leave_application/add', 'FormController@add_leave_application')->name('leave_application.add');
        Route::post('/leave_application/add', 'FormController@post_leave_application')->name('leave_application.post');
        // ------------------------------------------------------ //

        //Site Allowance
        Route::get('/site_allowance/add', 'FormController@add_site_allowance')->name('site_allowance.add');
        Route::post('/site_allowance/add', 'FormController@post_site_allowance')->name('site_allowance.post');
        // ------------------------------------------------------ //

        //Mileage Claim
        Route::get('/mileage_claim/add', 'FormController@add_mileage_claim')->name('mileage_claim.add');
        Route::post('/mileage_claim/add', 'FormController@post_mileage_claim')->name('mileage_claim.post');
        // ------------------------------------------------------ //

        //Timesheet
        Route::get('/timesheet', 'FormController@timesheet')->name('timesheet');
        // ======================================================//


        Route::get('/gettitle/{id}', 'FormController@getTitle')->name('form.gettitle');
    });
});
