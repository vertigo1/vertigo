@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.5rem;
        color: black;
    }

    #textarea, .textarea, #textarea-total-hour, #textarea-date-work, #textarea-start-hour, #textarea-end-hour {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        border: 1px solid #ced4da;
        min-height: 100%;
        padding: 2px;
        width: 100%;
        background: white;
        font-weight: 400;
        padding: .375rem .75rem;
    }

    .text-danger{
        font-size: 1rem;
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9" id="printSection">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2 font-13" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">OFF IN LIUE FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$index}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <b class="col-md-12 text-danger">* Requestor to attach all / any related documents.</b>
                            <b class="col-md-12 text-danger">* Site visit Safety Briefing not included.</b>
                            <b class="col-md-12 text-danger">* All application form required to be submitted within 1 month from execution date.</b>
                            <b class="col-md-12 text-danger">* Off In-Liue is not applicable during outstation.</b>
                            <b class="col-md-12 text-danger">* 16 working hours equal to 1 day leave.</b>
                            <b class="col-md-12 text-danger">* Only applicable on OFF-Day.</b>
                        </div>
                        <form id="in_liue" method="POST" action="{{route('in_liue.post')}}">
                        @csrf
                        <div class="row">
                            <label for="" class="col-md-2 font-13">STAFF ID</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="text" name="staff_id" value="{{Auth::user()->staff_id}}" readonly>
                            </div>

                            <label for="" class="col-md-2 font-13">JOB TITLE</label>
                            <div class="form-group col-md-4" style="min-height:110px;">
                                <div id="textarea" name="job_title" class="font-13"></div>
                                <input type="hidden" id="job_title" name="job_title">
                            </div>
                        </div>
                        <div class="row">
                            <label for="" class="col-md-2 font-13">DATE APPLY</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="date" name="date_apply" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" readonly>
                            </div>

                            <label for="" class="col-md-2 font-13">JOB NUMBER</label>
                            <div class="form-group col-md-4 font-13">
                                {{ Form::select('job_number', $orders, null,['required', 'class' => 'form-control select2 font-13', 'id' => 'job-number','placeholder'=> 'Choose Job Number' ] ) }} 
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="table-responsive" style="overflow-y: hidden;overflow-x: hidden;">
                                    <table id="table-liue" class="table table-bordered font-13" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th style="vertical-align:middle;text-align:center;width:25%">OFF DAY DESCRIPTION</th>
                                                <th style="vertical-align:middle;text-align:center;width:14%">DATE WORK</th>
                                                <th style="vertical-align:middle;text-align:center;width:13%">START HOUR</th>
                                                <th style="vertical-align:middle;text-align:center;width:13%">END HOUR</th>
                                                <th style="vertical-align:middle;text-align:center;width:10%">TOTAL HOUR</th>
                                                <th style="vertical-align:middle;text-align:center;width:25%">JOB DESCRIPTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="item">
                                                <td>
                                                    <div id="textarea-off-day-desc1" name="off_day_desc[]" class="font-13 textarea" contenteditable></div>
                                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="hidden" name="off_day_desc[]">
                                                </td>
                                                <td><input class="form-control input-group-lg font-13" autocomplete="off" type="date" name="date_work[]"></td>
                                                <td><input class="form-control input-group-lg font-13 start" autocomplete="off" type="time" id="start_hour1" name="start_hour[]"></td>
                                                <td><input class="form-control input-group-lg font-13 end" autocomplete="off" type="time" id="end_hour1" name="end_hour[]"></td>
                                                <td><input class="form-control input-group-lg font-13 total" style="text-align:center;" autocomplete="off" type="text" id="total_hour1" name="total_hour[]"></td>
                                                <td>
                                                    <div id="textarea-job-description1" name="job_description[]" class="font-13 textarea" contenteditable></div>
                                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="hidden" name="job_description[]">
                                                </td>
                                            </tr>
                                        <tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="row-btn" class="row">
                            <div class="form-group col-md-12">
                                <div class="col-md-2 mb-4 btn btn-primary add-liue" style="font-size:16px;float:right"><span><i class="fas fa-plus"></i> Add New Row</div> 
                            </div>
                        </div>

                        <div class="header">
                            <p class="col-md-12" style="text-align:center; font-weight: bold;">APPROVAL SECTION</p>
                        </div>

                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">Requestor to get approval from dedicated Superior</b>
                        </div>

                        <div class="row" style="justify-content:center;grid-gap:30px">
                            <div class="form-group col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" style="margin-bottom:33%;">REQUESTOR</label>
                                <p>--------------------------------------------------------------------------</p>
                                <p>DATE ------------------------------------------------------------------</p>
                            </div>
                            
                            <div class="form-group col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" style="margin-bottom:33%;">APPROVED BY</label>
                                <p>---------------------------------------------------------------------------</p>
                                <p>DATE ------------------------------------------------------------------</p>
                            </div>
                        </div>

                        <br/>

                        <div class="header">
                            <p class="col-md-12" style="text-align:center; font-weight: bold;">SUBMISSION SECTION</p>
                        </div>

                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">Requestor to submit complete form with related documents to Admin</b>
                        </div>

                        <div class="row" style="justify-content:center;grid-gap:30px">                            
                            <div class="form-group col-md-4" style="border:1px solid black;height:250px;">
                                <label for="" style="margin-bottom:30%;">RECEIVED BY (ADMIN)</label>
                                <p>--------------------------------------------------------------------------</p>
                                <p>DATE ------------------------------------------------------------------</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black; font-size:1.5rem;">
                            <ul>
                                <li>
                                    <p>Fill all the required spaces.</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <button type="submit" id="saveBtn" class="col-md-12 btn btn-danger" style="font-size:1.5rem;">Save</button> 
                            </div>

                            <div class="form-group col-md-6">
                                <button type="button" id="printBtn" class="col-md-12 btn btn-info print" href="javascript:;" style="font-size:1.5rem;">Print</button> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('form')}}" class="col-md-12  btn btn-outline-danger" style="font-size:1.5rem;">Back to Homepage</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script>

    $(() => {

        $(document).on('click', "div#textarea-date-work", function(){
            $('div.textarea-date-work').hide()
            $('input[name ="date_work[]"').show()
        });

        $(document).on('click', "div#textarea-start-hour", function(){
            $('div.textarea-start-hour').hide()
            $('input[name ="start_hour[]"').show()
        });

        $(document).on('click', "div#textarea-end-hour", function(){
            $('div.textarea-end-hour').hide()
            $('input[name ="end_hour[]"').show()
        });

        $(document).on('keyup', "[id^='textarea-off-day-desc']", function(){
            var indexOf = $(this).attr('id').split("textarea-off-day-desc");
            var mysave = $(this).text()
            $('input[name ="off_day_desc[]"]').eq(indexOf[1]-1).val(mysave);
        });

        $(document).on('keyup', "[id^='textarea-job-description']", function(){
            var indexOf = $(this).attr('id').split("textarea-job-description");
            var mysave = $(this).text()
            $('input[name ="job_description[]"]').eq(indexOf[1]-1).val(mysave);
        });

        $(document).on('click', "div#textarea-total-hour", function(){
            $('div.textarea-total-hour').hide()
            $('input[name ="total_hour[]"').show()
        });

        $(document).on('change', ".end" ,calcHour);
        

        validateForm();

        $('.select2').select2();

        var count = $('#table-liue tbody tr').length + 1;

        $('.add-liue').click(function () { 
            markup = 	`<tr class="item">
                            <td>
                                <div id="textarea-off-day-desc${count}" name="off_day_desc[]" class="font-13 textarea" contenteditable></div>
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="hidden" name="off_day_desc[]">
                            </td>
                            <td><input class="form-control input-group-lg font-13" autocomplete="off" type="date" name="date_work[]"></td>
                            <td><input class="form-control input-group-lg font-13 start" autocomplete="off" type="time" id="start_hour${count}" name="start_hour[]"></td>
                            <td><input class="form-control input-group-lg font-13 end " autocomplete="off" type="time" id="end_hour${count}" name="end_hour[]"></td>
                            <td><input class="form-control input-group-lg font-13 total" style="text-align:center;" autocomplete="off" type="text" id="total_hour${count}" name="total_hour[]"></td>
                            <td>
                                <div id="textarea-job-description${count}" name="job_description[]" class="font-13 textarea" contenteditable></div>
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="hidden" name="job_description[]">
                            </td>
                        </tr>`; 
            tableBody = $("#table-liue"); 
            tableBody.append(markup);
            count++;
        });

        $(".select2").on('change', () => {
            let val = $(".select2").val();
            $('span.select2').addClass('font-13')
            var url = '{{route("form.gettitle",":id")}}';
            let urlx = url.replace(":id",val);

            $.ajax({
                type: 'GET',
                url:  urlx,
                success: (resp) => {
                    $("#job-title").prop("value", resp['job_title']);
                    $("#textarea").html(resp['job_title']);
                },
                error: (e) => {
                    console.log(e);
                }
            });
        })
    })

    let validateForm = () => {
        $('#in_liue').validate({
            submitHandler: (form, e) => {
                btnLoading(true);
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            $("#saveBtn").prop('disabled',true);
                            $("#printBtn").prop('disabled',false);

                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            }).then((result) => {})
                        } else {
                            swal("Failed!", resp.msg, "error");
                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                        btnLoading(false);
                    }
                })    
            },
            rules: {
                off_day_desc : {
                    required:true
                },
            }
        });
    }

    function calcHour() {
        var start = 0;
        var end = 0;
        $(".item").each(function () {
            var start = $(this).find(".start").val();
            var end = $(this).find(".end").val();
            console.log(diff(start,end));
            $(this).find(".total").val(diff(start,end) == "NaN:NaN" ? "" : diff(start,end));
            $("div#textarea-total-hour").text(diff(start,end) == "NaN:NaN" ? "" : diff(start,end));
        });
    }

    function diff(start, end) {
        start = start.split(":");
        end = end.split(":");
        var startDate = new Date(0, 0, 0, start[0], start[1], 0);
        var endDate = new Date(0, 0, 0, end[0], end[1], 0);
        var diff = endDate.getTime() - startDate.getTime();
        var hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(diff / 1000 / 60);
        
        if(hours < 0) {
            newHours = hours + 24;
        }
        else {
            newHours = hours;
        }

        return Math.abs(newHours) + ":" + (minutes <= 9 ? "0" : "") + minutes;
    }

    let btnLoading = (display) => {
		if (display)
			$('#saveBtn').addClass('spinner spinner-white spinner-right').prop('disabled', true);
		else
			$('#saveBtn').removeClass('spinner spinner-white spinner-right').prop('disabled', false);
    }
    
    $('.print').on('click', function(){

        $('input[name ="date_work[]"]').each(function(key, value){
            var dateObject = $(this).val(); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'display': 'block','text-align': 'center', 'min-height': '0%'}).attr('id','textarea-date').addClass("textarea-date font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        })   

        $('input[name ="start_hour[]"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'text-align': 'center', 'display': 'block' }).attr('id','textarea-start-hour').addClass("textarea-start-hour").text($(this).val());
            $(this).hide();
        })

        $('input[name ="end_hour[]"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'text-align': 'center', 'display': 'block' }).attr('id','textarea-end-hour').addClass("textarea-end-hour").text($(this).val());
            $(this).hide();
        })

        $('input[name ="total_hour[]"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'text-align': 'center', 'display': 'block' }).attr('id','textarea-total-hour').addClass("textarea-total-hour").text($(this).val());
            $(this).hide();
        })

        window.scrollTo(0,0);

        $( "#row-btn" ).hide();
        $( "#printSection" ).removeClass('col-md-9');
        $( "#printSection" ).addClass('col-md-8');

        var count = $('#table-liue tbody tr').length;
        var height_table = 1650+150*count;

        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            scrollX: 0,
            scrollY: 0,
            x: 0,
            y: 0,
            width: 1900,
            height: height_table
        };  

        var chart = document.querySelector("#printSection");
        var width = chart.offsetWidth;
        var height = chart.offsetHeight;
        html2canvas(chart, options).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF();

            pdf.addImage(dataURL, 'JPEG', 5, 5, width/6.5, height/6.5, '', 'SLOW');

            pdf.save("In_Liue");
        });

        $( "#row-btn" ).show();
        $( "#printSection" ).removeClass('col-md-8');
        $( "#printSection" ).addClass('col-md-9');
    });
</script>
@endpush