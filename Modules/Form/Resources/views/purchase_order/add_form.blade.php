@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    .font-13 {
        font-size: 1.3rem;
        color: black;
    }

    
    #textarea {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        border: 1px solid #ced4da;
        min-height: 100%;
        padding: 2px;
        width: 100%;
        background: white;
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:70px;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:4%;font-size:40px;">PURCHASE ORDER REGISTRATION FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:30%;margin-top:30%;font-size:20px;">ID {{$index}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="purchaseOrder" method="POST" action="{{route('purchase_order.post')}}">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <label for="" class="col-md-2 font-13">JOB NUMBER</label>
                                        <div class="form-group col-md-3">
                                            <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="job_number" value="">
                                        </div>
                                        <label for="" class="col-md-2 font-13">CLIENT REF NUMBER</label>
                                        <div class="form-group col-md-5">
                                            <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="client_ref_no" value="">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label for="" class="col-md-2 font-13">CLIENT NAME</label>
                                        <div class="form-group col-md-3">
                                            <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="client_name" value="">
                                        </div>

                                        <label for="" class="col-md-2 font-13">JOB TITLE</label>
                                        <div class="form-group col-md-5" style="min-height:110px;">
                                            <div id="textarea" name="job_title" class="font-13" contenteditable></div>
                                            <input type="hidden" id="job_title" name="job_title">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label for="" class="col-md-2 font-13">PO DATE</label>
                                        <div class="form-group col-md-3">
                                            <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="po_date" value="" id="po_date">
                                        </div>

                                        <label for="" class="col-md-7"></label>
                                    </div>

                                    <div class="row">
                                        <label for="" class="col-md-2 font-13">PO VALUE</label>
                                        <div class="form-group col-md-3">
                                            <input class="form-control input-group-lg font-13" autocomplete="off" onchange="setTwoNumberDecimal(this)"  type="text"  name="po_value" id="po_value">
                                        </div>

                                        <label for="" class="col-md-2 font-13">PROJECT OWNER</label>
                                        <div class="form-group col-md-5">
                                            <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="project_owner" value="{{Auth::user()->staff_id}}" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-row" style="justify-content:flex-end;grid-gap:20px;">
                                        <div class="col-md-8" style="border:1px solid black;padding:15px">
                                            <p style="font-weight:bold;width:250px;margin-top:-30px;margin-left:10px;background:white;text-align:center">TYPE OF PURCHASE ORDER</p>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="type_po" id="gridRadios1" value="1">
                                                        <label class="form-check-label" style="font-size:1.1rem;" for="gridRadios1">SERVICES</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="type_po" id="gridRadios2" value="2">
                                                        <label class="form-check-label" style="font-size:1.1rem;" for="gridRadios2">TRADING</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="type_po" id="gridRadios3" value="3" >
                                                        <label class="form-check-label" style="font-size:1.1rem;" for="gridRadios3">RENTAL</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="type_po" id="gridRadios4" value="4" >
                                                        <label class="form-check-label" style="font-size:1.1rem;" for="gridRadios4">OPEX</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="errors"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <h2>INCOMING CLIENT PAYMENT</h2>
                                    <div class="table-responsive">
                                        <table id="table-client" class="table table-bordered font-13" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th style="vertical-align:middle;text-align:center;width:20%"></th>
                                                    <th style="vertical-align:middle;text-align:center;width:40%">EXPECTED DATE</th>
                                                    <th style="vertical-align:middle;text-align:center;width:40%">AMOUNT (RM)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>STAGE 1</td>
                                                    <td><input class="form-control input-group-lg font-13 date" autocomplete="off" type="text" name="expected_date_in[]" value=""></td>
                                                    <td><input class="form-control input-group-lg font-13" autocomplete="off" type="text" onchange="setTwoNumberDecimal(this)" name="amount_in[]" value=""></td>
                                                </tr>
                                            <tbody>
                                        </table>
                                    </div>

                                    <div class="mb-4 btn btn-primary add-client" style="font-size:16px;float:right"><span><i class="fas fa-plus"></i> Add New Row</div> 
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="card" style="border:2px solid lightgrey;justify-content:flex-end;">
                                        <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                                            <div class="btn" style="color:white;font-size:30px;margin-top:2%">IMPORTANT</div>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group row font-13">
                                                <ul>
                                                    <li>
                                                        <p>Ensure the form has been Saved before leaving this page.</p>
                                                    </li>
                                                <ul>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <button type="submit" class="col-md-12 btn btn-danger" id="saveBtn" style="font-size:1.3rem;">Save</button>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <a href="{{route('form')}}" class="col-md-12 btn btn-outline-danger" style="font-size:1.3rem;">Home</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" style="justify-content:center">
                                <div class="col-md-10">
                                    <h2>OUTGOING SUBCONT PAYMENT</h2>
                                    <div class="table-responsive">
                                        <table id="table-subcont" class="table table-bordered font-13" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th style="vertical-align:middle;text-align:center;width:5%"></th>
                                                    <th style="vertical-align:middle;text-align:center;width:30%">SUBCONT NAME</th>
                                                    <th style="vertical-align:middle;text-align:center;width:45%">DESCRIPTION</th>
                                                    <th style="vertical-align:middle;text-align:center;width:10%">EXPECTED DATE</th>
                                                    <th style="vertical-align:middle;text-align:center;width:10%">AMOUNT (RM)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td><input class="form-control input-group-lg font-13" autocomplete="off" type="text" name="subcont_name[]" value=""></td>
                                                    <td><input class="form-control input-group-lg font-13" autocomplete="off" type="text" name="description[]" value=""></td> 
                                                    <td><input class="form-control input-group-lg date font-13" autocomplete="off" type="text" name="expected_date_out[]" value=""></td>
                                                    <td><input class="form-control input-group-lg amount_out font-13" autocomplete="off" type="text" onchange="setTwoNumberDecimal(this)" name="amount_out[]" value=""></td>
                                                </tr>
                                            <tbody>
                                        </table>
                                        <div class="mb-4 btn btn-primary add-subcont" style="font-size:16px;float:right"><span><i class="fas fa-plus"></i> Add New Row</div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                </div>
                                <label for="" class="col-md-2 font-13">TOTAL</label>
                                <div class="col-md-3">
                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="text" name="total_outgoing">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
   
   $(() => {

        $(document).on('keyup', ".amount_out",calcAmount);

        validateForm();

        $("#po_date, .date ").datepicker(
        {
            format: 'yyyy-mm-dd',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });

        var count = $('#table-client tbody tr').length + 1;

        $('.add-client').click(function () { 
            $(".date").datepicker("destroy");

            markup = 	'<tr>'+
                            '<td>STAGE '+count+'</td>'+
                            '<td><input class="form-control input-group-lg date" autocomplete="off" type="text" name="expected_date_in[]"></td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="text" onchange="setTwoNumberDecimal(this)" name="amount_in[]"></td>'+
                        '</tr>'; 
            tableBody = $("#table-client"); 
            tableBody.append(markup);
            count++;

            $(".date").datepicker({
                format: 'yyyy-mm-dd',
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                clearBtn: true,
                orientation: "bottom right" ,
            });
        });

        var count1 = $('#table-subcont tbody tr').length + 1;

        $('.add-subcont').click(function () { 
            $(".date").datepicker("destroy");
            markup = 	'<tr>'+
                            '<td>'+count1+'</td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="text" name="subcont_name[]"></td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="text" name="description[]"></td>'+
                            '<td><input class="form-control input-group-lg date" autocomplete="off" type="text" name="expected_date_out[]"></td>'+
                            '<td><input class="form-control input-group-lg amount_out" autocomplete="off" type="text" onchange="setTwoNumberDecimal(this)" name="amount_out[]"></td>'+
                        '</tr>'; 
            tableBody = $("#table-subcont"); 
            tableBody.append(markup);
            count1++;

            $(".date").datepicker({
                format: 'yyyy-mm-dd',
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                clearBtn: true,
                orientation: "bottom right" ,
            });
        });

        $('#textarea').on('keyup',function () {
            var mysave = $('#textarea').html();
            $('#job_title').val(mysave);
        });


        })


        let validateForm = () => {
        $('#purchaseOrder').validate({
            submitHandler: (form, e) => {
                btnLoading(true);
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            })
                            .then((result) => {
                                window.location.href = resp.url;
                            });
                        } else {
                            swal("Failed!", resp.msg, "error");
                        }
                    },
                    error: (err) => {
                        swal("Failed!", resp.msg, "error");
                    },
                    complete: () => {
                        btnLoading(false);
                    }
                })    
            },
            rules: {
                job_number : {
                    minlength:5,
                    required:true
                },
                client_ref_no : {
                    minlength:5,
                    required:true
                },
                client_name : {
                    minlength:5,
                    required:true
                },
                job_title : {
                    required:true
                },
                po_value : {
                    required:true
                },
                po_date : {
                    required:true
                },
                type_po : {
                    required:true
                },
                "expected_date_in[]": "required",
                "amount_in[]": "required",
                "subcont_name[]": "required",
                "description[]": "required",
                "expected_date_out[]": "required",
                "amount_out[]": "required",
                
            },
            errorPlacement: function(error, element) {
                if(element.attr("type") == "radio") {
                    error.insertAfter($(".errors"));
                }
                else {
                    error.insertAfter(element);
                }
            }
        });
        }

        let btnLoading = (display) => {
        if (display)
            $('#saveBtn').addClass('spinner spinner-white spinner-right').prop('disabled', true);
        else
            $('#saveBtn').removeClass('spinner spinner-white spinner-right').prop('disabled', false);
        }

        function numberWithCommas(x) {
            return x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function setTwoNumberDecimal(el) {
            el.value = numberWithCommas(parseFloat(el.value).toFixed(2));
        }

        function calcAmount() {
            var total = 0;

            $(".amount_out").each(function () {
                var num = this.value.replace(/,/g, '');
                total += parseFloat(num);
            });

            $("input[name='total_outgoing']").val(numberWithCommas(total.toFixed(2)));
        }


</script>
@endpush