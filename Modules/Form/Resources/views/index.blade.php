@extends('layouts.app')
@push('styles')
<style>
 .card-no-border .card {
    background: grey;
    border-radius: 20px;
    box-shadow: 
    1px 1px 0px #000, 
    2px 2px 0px #000, 
    3px 3px 0px #000, 
    4px 4px 0px #000, 
    5px 5px 0px #000, 
    6px 6px 0px #000;
}
.text-danger {
    color: white !important;
    }
a {
    color: #ffff35;
    }
.text-centre {
    margin: auto;
    }

.card:hover{
   background-color: #5d5d5d;
}
h2:hover {
    color: black !important;
}

</style>
@endpush

@section('content')
<!-- <div class="row" style="padding-bottom:20px;padding-top:20px">
    <div class="col-md-12">
        <h3><strong>Form</strong></h3>
    </div>
</div> -->

<br/>
<br/>
<br/>

<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a href="{{route('purchase_order.add')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger" style="margin-top:7%"><strong>PURCHASE ORDER REGISTRATION FORM</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{route('medical_claim.add')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>MEDICAL CLAIM FORM</strong></h2>
                                    <small><strong>Please fill up PR Form after completing this form and submit both forms</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a href="{{route('purchase_request.add')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>PURCHASE REQUEST FORM</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                @if($inliue == 1)
                <a href="{{route('in_liue.add')}}">
                @else
                <a href="javascript:void(0)">
                @endif
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>IN LIUE FORM</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a href="{{route('outstation_claim.add')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>OUTSTATION FORM</strong></h2>
                                    <small><strong>Please fill up PR Form after completing this form and submit both forms</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{route('leave_application.add')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>LEAVE FORM</strong></h2>
                                    <small><strong>Including Annual, Emergency, Sick, Unpaid and Other</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a href="{{route('site_allowance.add')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>SITE ALLOWANCE FORM</strong></h2>
                                    <small><strong>Please fill up PR Form after completing this form and submit both forms</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{route('mileage_claim.add')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>MILEAGE FORM</strong></h2>
                                    <small><strong>Please fill up PR Form after completing this form and submit both forms</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <a href="{{route('timesheet')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>TIMESHEET CALCULATOR</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush