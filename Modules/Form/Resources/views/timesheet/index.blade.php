@extends('form::layouts.master')
@push('styles')
<style>
    .verticalTableHeader {
    text-align:center;
    white-space:nowrap;
    g-origin:50% 50%;
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
    
}
.verticalTableHeader p {
    margin:0 -100% ;
    display:inline-block;
}
.verticalTableHeader p:before{
    content:'';
    width:0;
    padding-top:150%;/* takes width as reference, + 10% for faking some extra padding */
    display:inline-block;
    vertical-align:middle;
}

.form-check-label p:before{
   margin-left: 25% 
}
table {
    /* text-align:center; */
    table-layout : fixed;
    /* width:150px */
}
.table td {
    padding:0% !important;
}
/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;

}

.disabletab{
    pointer-events: none;
}

@media screen and (max-width: 767px) {
    p{
        text-align:center !important;
        transform:none !important;
        margin:1% !important;
        padding:1% !important;
    }
}

input,label{
    font-weight:bold;
    color:black;
}

.font-13 {
        /* font-size: 1.3rem; */
        color: black;
}

#textarea {
    -moz-appearance: textfield-multiline;
    -webkit-appearance: textarea;
    border: 1px solid #ced4da;
    min-height: 100%;
    padding: 2px;
    width: 100%;
    background: white;
}

.text-danger{
    font-size: 1rem;
}
</style>
@endpush

@section('content')           
<br>
<div class="row" id="printSection">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9">
                <div class="card printtest" style="border: 2px solid lightgrey;" id="printtest1">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2 font-13 font-13" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">TIMESHEET CALCULATOR</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$index}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="leave" method="POST" action="{{route('leave_application.post')}}">
                        @csrf
                        <div class="col-md-12 ">
                            <div class="row">
                                <label for="" class="col-md-2 font-13  mx-auto">REQUESTOR</label>
                                <div class="form-group col-md-3 mx-auto">
                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="staff_id" value="{{$staff->staff_id}}" readonly>
                                </div>
                                <label for="" class="col-md-2 font-13 mx-auto">DATE APPLY</label>
                                <div class="form-group col-md-3 mx-auto">
                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="date"  name="apply" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <label for="" class="col-md-2 font-13 mx-auto">OT(STAFF) NAME</label>
                                <div class="form-group col-md-3 mx-auto">
                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="staff_id" id="staff-id" value="{{$staff->staff_id}}" readonly>
                                </div>
                                <label for="" class="col-md-2 font-13 mx-auto">SUPERIOR NAME</label>
                                <div class="form-group col-md-3 font-13 mx-auto">
                                    {{ Form::select('', $user, null,['class' => 'form-control select2',  'placeholder' => 'Select Superior Name']) }}
                                </div>
                            </div>
                            <div class="row">
                                <label for="" class="col-md-2 font-13 mx-auto">JOB NUMBER</label>
                                <div class="form-group col-md-3 font-13 mx-auto">
                                    {{ Form::select('', $orders, null,['class' => 'form-control select2', 'id' => 'job', 'placeholder' => 'Select Job Number']) }}
                                </div>
                                <label for="" class="col-md-2 font-13 mx-auto">JOB TITLE</label>
                                <div class="form-group col-md-3 mx-auto" style="min-height:110px;">
                                    <div id="textarea" name="job_title" class="font-13 form-control"></div>
                                    <input type="hidden" id="job_title" name="job_title">
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-5 mx-auto" style="border:1px solid black;">
                                <div class="row">
                                    <div class="col-md-6 font-13">
                                        <label for="" style="margin:20px">SELECT TYPE OF STAFF</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type" id="type_staff1" value="1" checked>
                                            <label class="form-check-label" for="type_staff1" style="white-space:nowrap;">  <p class="label-staff">CONTRACT STAFF </p></label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type" id="type_staff2" value="2">
                                            <label class="form-check-label" style="margin-bottom: 20px;" style="white-space:nowrap;" for="type_staff2"> <p class="label-staff"> VTSB STAFF</p></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="" style="margin-top: 20px; color:blue; font-size:1.3rem;">HOURLY RATE FOR<br> CONTRACT STAFF</label>
                                        <div class="form-group col-md-10" style="padding:0px;">
                                            <input class="form-control input-group-lg font-13" autocomplete="off" type="text" id="rate" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br><br><br>
                       
                    </div>
                </div>
            </div>

            <div class="col-md-3" data-html2canvas-ignore="true">
                <div class="card">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black;font-size:1.3rem;">
                            <ul>
                                <li>
                                <p>Fill all the required spaces</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <button type="button" id="printBtn" class="col-md-12 btn btn-info print" href="javascript:;" style="font-size:1.3rem;">Print</button> 
                                {{-- <a href="{{route('pdf.print')}}" class="col-md-12 btn btn-info print" style="font-size:1.3rem;">Print</a> --}}

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('form')}}" class="col-md-12 btn btn-outline-danger" style="font-size:1.3rem;">Back to Homepage</a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <br><br>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12 printtest" id="printtest2">
                <br><br>
                <div class="tab">
                    <button class="tablinks disabletab" onclick="openCity(event, 'Offshore')">Offshore</button>
                    <button class="tablinks disabletab" onclick="openCity(event, 'Onshore')">Onshore</button>
                </div>
            <div class="card">
                <div class="card-body">
                    <div id="Offshore" class="tabcontent">
                        <div class="table-responsive" style="color:black;">
                            <table id="table-offshore" class="table table-bordered">
                                <thead  style="margin-bottom: 20px">
                                    <tr>
                                        <th style="vertical-align:middle;text-align:center;width:10%">WORKING DAYS</th>
                                        <th style="vertical-align:middle;text-align:center;width:10%">TYPE OF DAY</th>
                                        <th class="verticalTableHeader" ><p>RATE RATIO</p></th>
                                        <th class="verticalTableHeader" ><p>06:00-07:00</p></th>
                                        <th class="verticalTableHeader" ><p>07:00-08:00</p></th>
                                        <th class="verticalTableHeader" ><p>08:00-09:00</p></th>
                                        <th class="verticalTableHeader" ><p>09:00-10:00</p></th>
                                        <th class="verticalTableHeader" ><p>10:00-11:00</p></th>
                                        <th class="verticalTableHeader" ><p>11:00-12:00</p></th>
                                        <th class="verticalTableHeader" ><p>12:00-13:00</p></th>
                                        <th class="verticalTableHeader" ><p>13:00-14:00</p></th>
                                        <th class="verticalTableHeader" ><p>14:00-15:00</p></th>
                                        <th class="verticalTableHeader" ><p>15:00-16:00</p></th>
                                        <th class="verticalTableHeader" ><p>16:00-17:00</p></th>
                                        <th class="verticalTableHeader" ><p>17:00-18:00</p></th>
                                        <th class="verticalTableHeader" ><p>RATE RATIO</p></th>
                                        <th class="verticalTableHeader" ><p>18:00-19:00</p></th>
                                        <th class="verticalTableHeader" ><p>19:00-20:00</p></th>
                                        <th class="verticalTableHeader" ><p>20:00-21:00</p></th>
                                        <th class="verticalTableHeader" ><p>21:00-22:00</p></th>
                                        <th class="verticalTableHeader" ><p>22:00-23:00</p></th>
                                        <th class="verticalTableHeader" ><p>23:00-00:00</p></th>
                                        <th class="verticalTableHeader" ><p>00:00-01:00</p></th>
                                        <th class="verticalTableHeader" ><p>01:00-02:00</p></th>
                                        <th class="verticalTableHeader" ><p>02:00-03:00</p></th>
                                        <th class="verticalTableHeader" ><p>03:00-04:00</p></th>
                                        <th class="verticalTableHeader" ><p>04:00-05:00</p></th>
                                        <th class="verticalTableHeader" ><p>05:00-06:00</p></th>
                                        <th class="verticalTableHeader" ><p>06:00-07:00</p></th>
                                        <th style="vertical-align:middle;text-align:center;width:10%" ><p>TOTAL NORMAL HOUR (RM)</p></th>
                                        <th style="vertical-align:middle;text-align:center;width:10%" ><p>TOTAL OT HOUR (RM)</p></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="row1" class="item">
                                        <td><input class="form-control input-group-sm working_days date"  autocomplete="off" type="text" name="day[]"></td>
                                        <td><input class="form-control input-group-sm"  autocomplete="off" type="text" name="type[]"></td>
                                        <td><input class="form-control input-group-sm normal_ratio" style="background-color:#7FAD21; color:white; text-align: center; font-weight: bold;"  autocomplete="off" type="text" name="ratio_normal[]"></td>
                                        <td><input class="form-control input-group-sm jam1" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam1[]" ></td>
                                        <td><input class="form-control input-group-sm jam2" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam2[]" ></td>
                                        <td><input class="form-control input-group-sm jam3" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam3[]" ></td>
                                        <td><input class="form-control input-group-sm jam4" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam4[]" ></td>
                                        <td><input class="form-control input-group-sm jam5" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam5[]" ></td>
                                        <td><input class="form-control input-group-sm jam6" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam6[]" ></td>
                                        <td><input class="form-control input-group-sm jam7" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam7[]" ></td>
                                        <td><input class="form-control input-group-sm jam8" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam8[]" ></td>
                                        <td><input class="form-control input-group-sm jam9" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam9[]" ></td>
                                        <td><input class="form-control input-group-sm jam10" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam10[]" ></td>
                                        <td><input class="form-control input-group-sm jam11" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam11[]" ></td>
                                        <td><input class="form-control input-group-sm jam12" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam12[]" ></td>
                                        <td><input class="form-control input-group-sm ot_ratio" style="background-color: #2491BA; color:white; text-align: center; font-weight: bold;"  autocomplete="off" type="text" name="ratio_ot[]"></td>
                                        <td><input class="form-control input-group-sm jam13" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam13[]"></td>
                                        <td><input class="form-control input-group-sm jam14" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam14[]"></td>
                                        <td><input class="form-control input-group-sm jam15" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam15[]"></td>
                                        <td><input class="form-control input-group-sm jam16" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam16[]"></td>
                                        <td><input class="form-control input-group-sm jam17" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam17[]"></td>
                                        <td><input class="form-control input-group-sm jam18" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam18[]"></td>
                                        <td><input class="form-control input-group-sm jam19" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam19[]"></td>
                                        <td><input class="form-control input-group-sm jam20" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam20[]"></td>
                                        <td><input class="form-control input-group-sm jam21" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam21[]"></td>
                                        <td><input class="form-control input-group-sm jam22" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam22[]"></td>
                                        <td><input class="form-control input-group-sm jam23" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam23[]"></td>
                                        <td><input class="form-control input-group-sm jam24" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam24[]"></td>
                                        <td><input class="form-control input-group-sm jam25" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam25[]"></td>
                                        <td><input class="form-control input-group-sm normal_hour"  autocomplete="off" type="text" name="normal_hour[]"></td>
                                        <td><input class="form-control input-group-sm ot_hour"  autocomplete="off" type="text" name="ot_hour[]"></td>
                                    </tr>
                                <tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9"><div style="float: right"> <label for="">SUB TOTAL</label> </div></div>
                            <div class="col-md-3" style="display: flex; justify-content: flex-end">
                                <div>
                                    <input class="form-control input-group-lg sub_total_normal" autocomplete="off" type="text" placeholder="RM" name="sub_total_normal" value=""> 
                                    
                                </div>
                                <div>
                                    <input  class="form-control input-group-lg sub_total_ot" autocomplete="off" type="text" placeholder="RM" name="sub_total_ot" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10"><div style="float: right"> <label for="">GRAND TOTAL</label> </div></div>
                            <div class="col-md-2 font-13">
                                <div><input class="form-control input-group-lg grand_total" style="background-color: #FE5E5E; color:white; font-size:1.3rem;" autocomplete="off" type="text" name="grand_total" value=""> </div>
                            </div>
                        </div>
                        <div class="mb-4 btn btn-primary add-offshore" style="font-size:14px;float:right" data-html2canvas-ignore="true" ><span><i class="fas fa-plus"></i> Add New Row</div> 
                    </div>

                    <div id="Onshore" class="tabcontent">
                        <div class="table-responsive" style="color:black;">
                            <table id="table-onshore" class="table table-bordered">
                                <thead  style="margin-bottom: 20px">
                                    <tr>
                                        <th style="vertical-align:middle;text-align:center;width:10%">WORKING DAYS</th>
                                        <th style="vertical-align:middle;text-align:center;width:10%">TYPE OF DAY</th>
                                        <th class="verticalTableHeader" ><p>RATE RATIO</p></th>
                                        <th class="verticalTableHeader" ><p>07:30-08:30</p></th>
                                        <th class="verticalTableHeader" ><p>08:30-09:30</p></th>
                                        <th class="verticalTableHeader" ><p>09:30-10:30</p></th>
                                        <th class="verticalTableHeader" ><p>10:30-11:30</p></th>
                                        <th class="verticalTableHeader" ><p>11:30-12:30</p></th>
                                        <th class="verticalTableHeader" ><p>12:30-13:30</p></th>
                                        <th class="verticalTableHeader" ><p>13:30-14:30</p></th>
                                        <th class="verticalTableHeader" ><p>14:30-15:30</p></th>
                                        <th class="verticalTableHeader" ><p>15:30-16:30</p></th>
                                        <th class="verticalTableHeader" ><p>16:30-17:30</p></th>
                                        <th class="verticalTableHeader" ><p>RATE RATIO</p></th>
                                        <th class="verticalTableHeader" ><p>17:30-18:30</p></th>
                                        <th class="verticalTableHeader" ><p>18:30-19:30</p></th>
                                        <th class="verticalTableHeader" ><p>20:30-21:30</p></th>
                                        <th class="verticalTableHeader" ><p>21:30-22:30</p></th>
                                        <th class="verticalTableHeader" ><p>22:30-23:30</p></th>
                                        <th class="verticalTableHeader" ><p>23:30-00:30</p></th>
                                        <th class="verticalTableHeader" ><p>00:30-01:30</p></th>
                                        <th class="verticalTableHeader" ><p>01:30-02:30</p></th>
                                        <th class="verticalTableHeader" ><p>02:30-03:30</p></th>
                                        <th class="verticalTableHeader" ><p>03:30-04:30</p></th>
                                        <th class="verticalTableHeader" ><p>04:30-05:30</p></th>
                                        <th class="verticalTableHeader" ><p>05:30-06:30</p></th>
                                        <th class="verticalTableHeader" ><p>06:30-07:30</p></th>
                                        <th style="vertical-align:middle;text-align:center;width:10%" ><p>TOTAL NORMAL HOUR (RM)</p></th>
                                        <th style="vertical-align:middle;text-align:center;width:10%" ><p>TOTAL OT HOUR (RM)</p></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="row1" class="item-onshore">
                                        <td><input class="form-control input-group-sm working_day_onshore date"  autocomplete="off" type="text" id="working_day"></td>
                                        <td><input class="form-control input-group-sm"  autocomplete="off" type="text" name="type[]"></td>
                                        <td><input class="form-control input-group-sm normal_ratio_onshore" style="background-color:#7FAD21; color:white; text-align: center; font-weight: bold;"  autocomplete="off" type="text" name="ratio_normal[]"></td>
                                        <td><input class="form-control input-group-sm jam30" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam1[]" ></td>
                                        <td><input class="form-control input-group-sm jam31" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam2[]" ></td>
                                        <td><input class="form-control input-group-sm jam32" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam3[]" ></td>
                                        <td><input class="form-control input-group-sm jam33" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam4[]" ></td>
                                        <td><input class="form-control input-group-sm jam34" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam5[]" ></td>
                                        <td><input class="form-control input-group-sm jam35" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam6[]" ></td>
                                        <td><input class="form-control input-group-sm jam36" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam7[]" ></td>
                                        <td><input class="form-control input-group-sm jam37" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam8[]" ></td>
                                        <td><input class="form-control input-group-sm jam38" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam9[]" ></td>
                                        <td><input class="form-control input-group-sm jam39" style="background-color: #C3FF64; text-align: center;color:black;"  autocomplete="off" type="text" name="jam10[]" ></td>
                                        <td><input class="form-control input-group-sm ot_ratio_onshore" style="background-color: #2491BA; color:white; text-align: center; font-weight: bold;"  autocomplete="off" type="text" name="ratio_ot[]"></td>
                                        <td><input class="form-control input-group-sm jam40" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam11[]" ></td>
                                        <td><input class="form-control input-group-sm jam41" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam12[]" ></td>
                                        <td><input class="form-control input-group-sm jam42" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam13[]"></td>
                                        <td><input class="form-control input-group-sm jam43" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam14[]"></td>
                                        <td><input class="form-control input-group-sm jam44" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam15[]"></td>
                                        <td><input class="form-control input-group-sm jam45" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam16[]"></td>
                                        <td><input class="form-control input-group-sm jam46" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam17[]"></td>
                                        <td><input class="form-control input-group-sm jam47" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam18[]"></td>
                                        <td><input class="form-control input-group-sm jam48" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam19[]"></td>
                                        <td><input class="form-control input-group-sm jam49" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam20[]"></td>
                                        <td><input class="form-control input-group-sm jam50" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam21[]"></td>
                                        <td><input class="form-control input-group-sm jam51" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam22[]"></td>
                                        <td><input class="form-control input-group-sm jam52" style="background-color: #BBE3E8; text-align: center; color:black;"  autocomplete="off" type="text" name="jam23[]"></td>
                                        <td><input class="form-control input-group-sm normal_hour_onshore"  autocomplete="off" type="text" name="normal_hour[]"></td>
                                        <td><input class="form-control input-group-sm ot_hour_onshore"  autocomplete="off" type="text" name="ot_hour[]"></td>
                                    </tr>
                                <tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9"><div style="float: right"> <label class="font-13" for="">SUB TOTAL</label> </div></div>
                            <div class="col-md-3" style="display: flex; justify-content: flex-end">
                                <div><input class="form-control input-group-lg sub_total_normal_onshore font-13" autocomplete="off" type="text" placeholder="RM" name="sub_total_normal_onshore" value="">  </div>
                                <div><input class="form-control input-group-lg sub_total_ot_onshore font-13" autocomplete="off" type="text" placeholder="RM" name="sub_total_ot_onshore" value=""> </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10"><div style="float: right"> <label class="font-13" for="">GRAND TOTAL</label> </div></div>
                            <div class="col-md-2 font-13">
                                <div><input class="form-control input-group-lg grand_total_onshore" style="background-color: #FE5E5E; color:white; font-size:1.3rem;" autocomplete="off" type="text" name="grand_total_onshore" value=""> </div>
                            </div>
                        </div>
                        <div class="mb-4 btn btn-primary add-onshore" style="font-size:14px;float:right" data-html2canvas-ignore="true" ><span><i class="fas fa-plus"></i> Add New Row</div> 
                    </div>
                
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    var rate_hour = 0;
    var rate = '{!! $staff->salary !!}';
    var staff = '{!! $staff->staff_id !!}';

    const formatter = new Intl.NumberFormat('ms-MY', {
        style: 'currency',
        currency: 'MYR',
        minimumFractionDigits: 2
    });

    $(() => {
        $(".date").datepicker({
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "auto" ,
        });

        // $("#rate").val() != null {
        //     $(".tablinks").removeClass('disabletab')
        // }

        $("#staff-id").prop('readonly', false);
        $("#staff-id").val('');
        $("#rate").show();

        $("input[name='type']").on('change', function() {
            if(this.value == 2) {
                var string = rate.replace(/[^\d\.\-]/g, ""); 
                var number = parseFloat(string);

                $("#rate").prop('readonly',true);
                $("#rate").val(number/26/8);
                rate_hour = number/26/8;

                $("#rate").hide();

                $("#staff-id").prop('readonly', true);
                $("#staff-id").val(staff);

                $(".tablinks").removeClass('disabletab');

            }
            else {
                $("#rate").prop('readonly',false);
                $("#rate").val('');

                $("#staff-id").prop('readonly', false);
                $("#staff-id").val('');

                $("#rate").show();
            }
        });

        $("#rate").on('keyup', function() {
            rate_hour = this.value;
            if(this.value != null) {
                $(".tablinks").removeClass('disabletab');
            } 
        });

        $("#job").on('change', () => {
                let val = $("#job").val();
                
                var url = '{{route("form.gettitle",":id")}}';
                let urlx = url.replace(":id",val);

                $.ajax({
                    type: 'GET',
                    url:  urlx,
                    success: (resp) => {
                        $("#job_title").prop("value", resp['job_title']);
                        $("#textarea").html(resp['job_title']);
                    },
                    error: (e) => {
                        console.log(e);
                    }
                });
        })


        $(document).on('keyup', ".working_days, .normal_ratio, .ot_ratio, .jam1, .jam2, .jam3, .jam4, .jam5, .jam6, .jam7, .jam8, .jam9, .jam10, .jam11, .jam12, .jam13, .jam14, .jam15, .jam16, .jam17, .jam18, .jam19, .jam20, .jam21, .jam22, .jam23, .jam24",calcAll);
        $(document).on('keyup', ".working_day_onshore, .normal_ratio_onshore, .ot_ratio_onshore, .jam30, .jam31, .jam32, .jam33, .jam34, .jam35, .jam36, .jam37, .jam38, .jam39, .jam40, .jam41, .jam42, .jam43, .jam44, .jam45, .jam46, .jam47, .jam48, .jam49, .jam50, .jam51, .jam52, .jam53, .jam54",calcAllOnshore);

        var count = $('#table-offshore tbody tr').length + 1;
        var count1 = $('#table-onshore tbody tr').length + 1;


        $('.add-offshore').click(function () { 
            $(".date").datepicker("destroy");
            markup = 	`<tr id="row${count}" class="item">
                                    <td><input class="form-control input-group-sm working_days date"  autocomplete="off" type="text" name="day[]"></td>
                                    <td><input class="form-control input-group-sm"  autocomplete="off" type="text" name="type[]"></td>
                                    <td><input class="form-control input-group-sm normal_ratio" style="background-color:#7FAD21; color:white; text-align: center; font-weight: bold;"  autocomplete="off" type="text" name="ratio_normal[]"></td>
                                    <td><input class="form-control input-group-sm jam1" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam1[]" ></td>
                                    <td><input class="form-control input-group-sm jam2" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam2[]" ></td>
                                    <td><input class="form-control input-group-sm jam3" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam3[]" ></td>
                                    <td><input class="form-control input-group-sm jam4" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam4[]" ></td>
                                    <td><input class="form-control input-group-sm jam5" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam5[]" ></td>
                                    <td><input class="form-control input-group-sm jam6" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam6[]" ></td>
                                    <td><input class="form-control input-group-sm jam7" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam7[]" ></td>
                                    <td><input class="form-control input-group-sm jam8" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam8[]" ></td>
                                    <td><input class="form-control input-group-sm jam9" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam9[]" ></td>
                                    <td><input class="form-control input-group-sm jam10" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam10[]" ></td>
                                    <td><input class="form-control input-group-sm jam11" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam11[]" ></td>
                                    <td><input class="form-control input-group-sm jam12" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam12[]" ></td>
                                    <td><input class="form-control input-group-sm ot_ratio" style="background-color: #2491BA; color:white; text-align: center; font-weight: bold;"  autocomplete="off" type="text" name="ratio_ot[]"></td>
                                    <td><input class="form-control input-group-sm jam13" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam13[]"></td>
                                    <td><input class="form-control input-group-sm jam14" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam14[]"></td>
                                    <td><input class="form-control input-group-sm jam15" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam15[]"></td>
                                    <td><input class="form-control input-group-sm jam16" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam16[]"></td>
                                    <td><input class="form-control input-group-sm jam17" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam17[]"></td>
                                    <td><input class="form-control input-group-sm jam18" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam18[]"></td>
                                    <td><input class="form-control input-group-sm jam19" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam19[]"></td>
                                    <td><input class="form-control input-group-sm jam20" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam20[]"></td>
                                    <td><input class="form-control input-group-sm jam21" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam21[]"></td>
                                    <td><input class="form-control input-group-sm jam22" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam22[]"></td>
                                    <td><input class="form-control input-group-sm jam23" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam23[]"></td>
                                    <td><input class="form-control input-group-sm jam24" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam24[]"></td>
                                    <td><input class="form-control input-group-sm jam25" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam25[]"></td>
                                    <td><input class="form-control input-group-sm normal_hour"  autocomplete="off" type="text" name="normal_hour[]"></td>
                                    <td><input class="form-control input-group-sm ot_hour"  autocomplete="off" type="text" name="ot_hour[]"></td>
                                </tr>`; 
            tableBody = $("#table-offshore"); 
            tableBody.append(markup);
            count++;

            $(".date").datepicker({
                format: 'dd/mm/yyyy',
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                clearBtn: true,
                orientation: "auto" ,
            });
        });
        $('.add-onshore').click(function () { 
            $(".date").datepicker("destroy");
            markup = 	`<tr id="row${count}" class="item-onshore">
                                        <td><input class="form-control input-group-sm working_day_onshore date"  autocomplete="off" type="text" name="day[]"></td>
                                        <td><input class="form-control input-group-sm"  autocomplete="off" type="text" name="type[]"></td>
                                        <td><input class="form-control input-group-sm normal_ratio_onshore" style="background-color:#7FAD21; color:white; text-align: center; font-weight: bold;"  autocomplete="off" type="text" name="ratio_normal[]"></td>
                                        <td><input class="form-control input-group-sm jam30" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam1[]" ></td>
                                        <td><input class="form-control input-group-sm jam31" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam2[]" ></td>
                                        <td><input class="form-control input-group-sm jam32" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam3[]" ></td>
                                        <td><input class="form-control input-group-sm jam33" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam4[]" ></td>
                                        <td><input class="form-control input-group-sm jam34" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam5[]" ></td>
                                        <td><input class="form-control input-group-sm jam35" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam6[]" ></td>
                                        <td><input class="form-control input-group-sm jam36" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam7[]" ></td>
                                        <td><input class="form-control input-group-sm jam37" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam8[]" ></td>
                                        <td><input class="form-control input-group-sm jam38" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam9[]" ></td>
                                        <td><input class="form-control input-group-sm jam39" style="background-color: #C3FF64; text-align: center;"  autocomplete="off" type="text" name="jam10[]" ></td>
                                        <td><input class="form-control input-group-sm ot_ratio_onshore" style="background-color: #2491BA; color:white; text-align: center; font-weight: bold;"  autocomplete="off" type="text" name="ratio_ot[]"></td>
                                        <td><input class="form-control input-group-sm jam40" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam11[]" ></td>
                                        <td><input class="form-control input-group-sm jam41" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam12[]" ></td>
                                        <td><input class="form-control input-group-sm jam42" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam13[]"></td>
                                        <td><input class="form-control input-group-sm jam43" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam14[]"></td>
                                        <td><input class="form-control input-group-sm jam44" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam15[]"></td>
                                        <td><input class="form-control input-group-sm jam45" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam16[]"></td>
                                        <td><input class="form-control input-group-sm jam46" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam17[]"></td>
                                        <td><input class="form-control input-group-sm jam47" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam18[]"></td>
                                        <td><input class="form-control input-group-sm jam48" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam19[]"></td>
                                        <td><input class="form-control input-group-sm jam49" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam20[]"></td>
                                        <td><input class="form-control input-group-sm jam50" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam21[]"></td>
                                        <td><input class="form-control input-group-sm jam51" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam22[]"></td>
                                        <td><input class="form-control input-group-sm jam52" style="background-color: #BBE3E8; text-align: center;"  autocomplete="off" type="text" name="jam23[]"></td>
                                        <td><input class="form-control input-group-sm normal_hour_onshore"  autocomplete="off" type="text" name="normal_hour[]"></td>
                                        <td><input class="form-control input-group-sm ot_hour_onshore"  autocomplete="off" type="text" name="ot_hour[]"></td>
                                </tr>`; 
            tableBody = $("#table-onshore"); 
            tableBody.append(markup);
            count++;
            $(".date").datepicker({
                format: 'dd/mm/yyyy',
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                clearBtn: true,
                orientation: "auto" ,
            });
        });
    })

    function calcAll() {
            // calculate total for one row
            
        $(".item").each(function () {
            var amount = 0;
            var jam1 = 0;
            var jam2 = 0;
            var jam3 = 0;
            var jam4 = 0;
            var jam5 = 0;
            var jam6 = 0;
            var jam7 = 0;
            var jam8 = 0;
            var jam9 = 0;
            var jam10 = 0;
            var jam11 = 0;
            var jam12 = 0;
            var jam13 = 0;
            var jam14 = 0;
            var jam15 = 0;
            var jam16 = 0;
            var jam17 = 0;
            var jam18 = 0;
            var jam19 = 0;
            var jam20 = 0;
            var jam21 = 0;
            var jam22 = 0;
            var jam23 = 0;
            var jam24 = 0;
            var jam25 = 0;
            var working_days = 0;
            var normal_ratio = 0;
            var ot_ratio = 0;

            
            if (!isNaN(parseFloat($(this).find(".jam1").val()))) {
                jam1 = parseFloat($(this).find(".jam1").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam2").val()))) {
                jam2 = parseFloat($(this).find(".jam2").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam3").val()))) {
                jam3 = parseFloat($(this).find(".jam3").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam4").val()))) {
                jam4 = parseFloat($(this).find(".jam4").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam5").val()))) {
                jam5 = parseFloat($(this).find(".jam5").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam6").val()))) {
                jam6 = parseFloat($(this).find(".jam6").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam7").val()))) {
                jam7 = parseFloat($(this).find(".jam7").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam8").val()))) {
                jam8 = parseFloat($(this).find(".jam8").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam9").val()))) {
                jam9 = parseFloat($(this).find(".jam9").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam10").val()))) {
                jam10 = parseFloat($(this).find(".jam10").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam11").val()))) {
                jam11 = parseFloat($(this).find(".jam11").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam12").val()))) {
                jam12 = parseFloat($(this).find(".jam12").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam13").val()))) {
                jam13 = parseFloat($(this).find(".jam13").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam14").val()))) {
                jam14 = parseFloat($(this).find(".jam14").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam15").val()))) {
                jam15 = parseFloat($(this).find(".jam15").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam16").val()))) {
                jam16 = parseFloat($(this).find(".jam16").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam17").val()))) {
                jam17 = parseFloat($(this).find(".jam17").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam18").val()))) {
                jam18 = parseFloat($(this).find(".jam18").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam19").val()))) {
                jam19 = parseFloat($(this).find(".jam19").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam20").val()))) {
                jam20 = parseFloat($(this).find(".jam20").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam21").val()))) {
                jam21 = parseFloat($(this).find(".jam21").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam22").val()))) {
                jam22 = parseFloat($(this).find(".jam22").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam23").val()))) {
                jam23 = parseFloat($(this).find(".jam23").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam24").val()))) {
                jam24 = parseFloat($(this).find(".jam24").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam25").val()))) {
                jam25 = parseFloat($(this).find(".jam25").val());
            }
            if (!isNaN(parseFloat($(this).find(".working_days").val()))) {
                working_days = parseFloat($(this).find(".working_days").val());
            }
            if (!isNaN(parseFloat($(this).find(".normal_ratio").val()))) {
                normal_ratio = parseFloat($(this).find(".normal_ratio").val());
            }
            if (!isNaN(parseFloat($(this).find(".ot_ratio").val()))) {
                ot_ratio = parseFloat($(this).find(".ot_ratio").val());
            }

            total_normal = rate_hour  * (jam1 + jam2 + jam3 + jam4 + jam5 + jam6 + jam7 + jam8 + jam9 + jam10 + jam11 + jam12) * normal_ratio;
            total_ot = rate_hour * (jam13 + jam14 + jam15 + jam16 + jam17 + jam18 + jam19 + jam20 + jam21 + jam22 + jam23 + jam24 + jam25) * ot_ratio;

            $(this).find(".normal_hour").val(total_normal.toFixed(2));
            $(this).find(".ot_hour").val(total_ot.toFixed(2));
        });

        var sub_total_normal = 0;
        $(".normal_hour").each(function () {
            if (!isNaN(this.value) && this.value.length != 0) {
                sub_total_normal += parseFloat(this.value);
            }
        });

        var sub_total_ot = 0;
        $(".ot_hour").each(function () {
            if (!isNaN(this.value) && this.value.length != 0) {
                sub_total_ot += parseFloat(this.value);
            }
        });

        var grand_total = sub_total_normal + sub_total_ot;

        $(".sub_total_normal").val(sub_total_normal.toFixed(2));
        $(".sub_total_ot").val(sub_total_ot.toFixed(2));
        $(".grand_total").val(formatter.format(grand_total));
            
    }

    function calcAllOnshore() {
            // calculate total for one row
            
        $(".item-onshore").each(function () {
            var amount = 0;
            var jam1 = 0;
            var jam2 = 0;
            var jam3 = 0;
            var jam4 = 0;
            var jam5 = 0;
            var jam6 = 0;
            var jam7 = 0;
            var jam8 = 0;
            var jam9 = 0;
            var jam10 = 0;
            var jam11 = 0;
            var jam12 = 0;
            var jam13 = 0;
            var jam14 = 0;
            var jam15 = 0;
            var jam16 = 0;
            var jam17 = 0;
            var jam18 = 0;
            var jam19 = 0;
            var jam20 = 0;
            var jam21 = 0;
            var jam22 = 0;
            var jam23 = 0;
            var jam24 = 0;
            var jam25 = 0;
            var working_days = 0;
            var normal_ratio = 0;
            var ot_ratio = 0;

            if (!isNaN(parseFloat($(this).find(".jam30").val()))) {
                jam1 = parseFloat($(this).find(".jam30").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam31").val()))) {
                jam2 = parseFloat($(this).find(".jam31").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam32").val()))) {
                jam3 = parseFloat($(this).find(".jam32").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam33").val()))) {
                jam4 = parseFloat($(this).find(".jam33").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam34").val()))) {
                jam5 = parseFloat($(this).find(".jam34").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam35").val()))) {
                jam6 = parseFloat($(this).find(".jam35").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam36").val()))) {
                jam7 = parseFloat($(this).find(".jam36").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam37").val()))) {
                jam8 = parseFloat($(this).find(".jam37").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam38").val()))) {
                jam9 = parseFloat($(this).find(".jam38").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam39").val()))) {
                jam10 = parseFloat($(this).find(".jam39").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam40").val()))) {
                jam11 = parseFloat($(this).find(".jam40").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam41").val()))) {
                jam12 = parseFloat($(this).find(".jam41").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam42").val()))) {
                jam13 = parseFloat($(this).find(".jam42").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam43").val()))) {
                jam14 = parseFloat($(this).find(".jam43").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam44").val()))) {
                jam15 = parseFloat($(this).find(".jam44").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam45").val()))) {
                jam16 = parseFloat($(this).find(".jam45").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam46").val()))) {
                jam17 = parseFloat($(this).find(".jam46").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam47").val()))) {
                jam18 = parseFloat($(this).find(".jam47").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam48").val()))) {
                jam19 = parseFloat($(this).find(".jam48").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam49").val()))) {
                jam20 = parseFloat($(this).find(".jam49").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam50").val()))) {
                jam21 = parseFloat($(this).find(".jam50").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam51").val()))) {
                jam22 = parseFloat($(this).find(".jam51").val());
            }
            if (!isNaN(parseFloat($(this).find(".jam52").val()))) {
                jam23 = parseFloat($(this).find(".jam52").val());
            }
            if (!isNaN(parseFloat($(this).find(".working_day_onshore").val()))) {
                working_days = parseFloat($(this).find(".working_day_onshore").val());
            }
            if (!isNaN(parseFloat($(this).find(".normal_ratio_onshore").val()))) {
                normal_ratio = parseFloat($(this).find(".normal_ratio_onshore").val());
            }
            if (!isNaN(parseFloat($(this).find(".ot_ratio_onshore").val()))) {
                ot_ratio = parseFloat($(this).find(".ot_ratio_onshore").val());
            }

            total_normal = rate_hour  * (jam1 + jam2 + jam3 + jam4 + jam5 + jam6 + jam7 + jam8 + jam9 + jam10) * normal_ratio;
            total_ot =  rate_hour  * (jam11 + jam12 + jam13 + jam14 + jam15 + jam16 + jam17 + jam18 + jam19 + jam20 + jam21 + jam22 + jam23) * ot_ratio;

            console.log('===============rate' + rate_hour);
            console.log('===============day' + working_days);
            console.log('===============normal' + normal_ratio);
            console.log('===============ot' + ot_ratio);


            $(this).find(".normal_hour_onshore").val(total_normal.toFixed(2));
            $(this).find(".ot_hour_onshore").val(total_ot.toFixed(2));
        });

        var sub_total_normal = 0;
        $(".normal_hour_onshore").each(function () {
            if (!isNaN(this.value) && this.value.length != 0) {
                sub_total_normal += parseFloat(this.value);
            }
        });

        var sub_total_ot = 0;
        $(".ot_hour_onshore").each(function () {
            if (!isNaN(this.value) && this.value.length != 0) {
                sub_total_ot += parseFloat(this.value);
            }
        });

        var grand_total = sub_total_normal + sub_total_ot;

        $(".sub_total_normal_onshore").val(sub_total_normal.toFixed(2));
        $(".sub_total_ot_onshore").val(sub_total_ot.toFixed(2));
        $(".grand_total_onshore").val(formatter.format(grand_total));
            
    }

        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

    
    $('.print').on('click', function(){
        window.scrollTo(0,0);

        var chart = document.querySelector("#printSection");

        var ubah = chart.children[0].firstElementChild.firstElementChild;
        ubah.className = 'col-md-12';

        var tukarStyle = document.querySelectorAll('.select2, .form-control');
        for (var i = 0; i < tukarStyle.length; i++ ) {
            tukarStyle[i].style.fontWeight = "bold";
            tukarStyle[i].style.fontSize = "1.3rem";
        }

        var count = $('#table-offshore tbody tr').length;
        var count1 = $('#table-onshore tbody tr').length;
        var margin = (count>count1) ? 10*count : 10*count1;
        $('p.label-staff').css('margin-left', `${margin}px`)

        html2canvas(chart,{
            windowWidth: window.innerWidth,
            windowHeight: window.innerHeight,
            scale: 2,
        }).then(canvas => {
            var count = $('#table-offshore tbody tr').length;
            var count1 = $('#table-onshore tbody tr').length;
            var margin = (count>count1) ? 10*count : 10*count1;
            $('p.label-staff').css('margin-left', `${margin}px`)
            var dataURL = canvas.toDataURL('image/png',1.0);

            var pdf = new jsPDF({orientation:'landscape', compress:true});

            var pageWidth = pdf.internal.pageSize.width;
            var pageHeight = pdf.internal.pageSize.height;

            var pdf = new jsPDF('landscape');

            var canvasWidth = canvas.width * (ratio + 0.015);
            var canvasHeight = canvas.height * (ratio + 0.015);

            pdf.addImage(dataURL, 'JPEG', 10, 2, canvasWidth, canvasHeight, undefined, 'FAST');

            pdf.save("Timesheet_Calculator");
        });
    });



</script>
@endpush