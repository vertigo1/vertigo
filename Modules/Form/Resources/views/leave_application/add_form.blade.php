@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.5rem;
        color: black;
    }

    #textarea, #textarea-reason, #textarea-start, #textarea-end, #textarea-total-day, #textarea-type-leave, #textarea-balance, #textarea-apply, #textarea-handover {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        border: 1px solid #ced4da;
        min-height: 100%;
        padding: 2px;
        width: 100%;
        background: white;
        font-weight: 400;
        padding: .375rem .75rem;
    }

    .text-danger{
        font-size: 1rem;
    }

    .form-control {
        height: fit-content !important;
    }


</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9" id="printSection">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2 font-13 font-13" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">LEAVE APPLICATION FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$index}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="leave" method="POST" action="{{route('leave_application.post')}}">
                        @csrf
                        <div class="form-group row">
                            <b class="col-md-12 text-danger">* Leave form must be submitted at least 5 DAYS before leave date.</b>
                            <b class="col-md-12 text-danger">* Other than annual leaves, please attach relevant supporting documents for reference.</b>
                            <b class="col-md-12 text-danger">* Approval confirmation only valid through system. Please check your Leave Application Status before taking a leave.</b>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">STAFF ID</label>
                            <div class="form-group col-md-3">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="staff_id" value="{{$staff->staff_id}}" readonly>
                            </div>
                            <label for="" class="col-md-2 font-13">DATE APPLY</label>
                            <div class="form-group col-md-3">
                                <input class="form-control input-group-lg font-13 " autocomplete="off" type="hidden"  name="apply" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">START DATE</label>
                            <div class="form-group col-md-3">
                                <input class="form-control input-group-lg font-13 date" autocomplete="off" type="text"  id="start" name="start" value="">
                            </div>

                            <label for="" class="col-md-2 font-13">TOTAL DAY</label>

                            <label for="" class="col-md-3 font-13">TYPE OF LEAVE</label>

                            <label for="" class="col-md-2 font-13">TOTAL BALANCE LEAVE</label>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">END DATE</label>
                            <div class="form-group col-md-3">
                                <input class="form-control input-group-lg font-13 date" autocomplete="off" type="text" id="end" name="end" value="" readonly>
                            </div>

                            <div class="form-group col-md-2 font-13">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="hidden"  id="total_day" name="total_day" value="" readonly>
                                <div id="textarea-total-day" class="font-13" style="text-align:center;opacity: 0.7;background-color: #e9ecef;"></div>
                            </div>

                            <div class="form-group col-md-3 font-13">
                                {{-- <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="type_leave" value=""> --}}
                                {{ Form::select('type_leave', ['Annual Leave', 'Liue Leave', 'Emergency Leave', 'Sick Leave', 'Unpaid Leave', 'Others'], null,['class' => 'form-control mh-100 font-13', 'id' => 'type_leave']) }} 
                            </div>

                            <div class="form-group col-md-2 font-13">
                                <!-- <input class="form-control input-group-lg font-13" autocomplete="off" type="text" id="balance" name="" value="{{$staff->annual_leave + $staff->liue_leave}}" readonly> -->
                                <input class="form-control input-group-lg font-13" style="text-align:center;" autocomplete="off" type="hidden" id="balance" name="" value="{{$staff->annual_leave}}" readonly>
                                <div id="textarea-balance" class="font-13" style="text-align:center;opacity: 0.7;background-color: #e9ecef;"></div>
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">REASON</label>
                            <div class="form-group col-md-10" style="min-height:110px;">
                                <div id="textarea-reason" name="reason" class="font-13" contenteditable></div>
                                <input type="hidden" id="reason" name="reason">
                            </div>
                        </div>

                        <hr/><br/>

                        <div class="header">
                            <p class="col-md-12" style="text-align:center; font-weight:bold;">WORK HAND OVER SECTION</p>
                        </div>

                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">Requestor to get agreement from peer</b>
                        </div>

                        <div class="form-group row">
                            <p class="col-md-12" style="text-align:center">TimeFrame : 3 working days</p>
                        </div>

                        <div class="form-row" style="justify-content:center;grid-gap:20px;">
                            <div class="col-md-7" style="border:1px solid black; height:230px;">
                                <div class="col-sm-12">
                                <br/>
                                    <label class="font-13" for="" >HAND OVER SCOPE OF WORK</label>
                                    <br/>
                                    <div id="textarea" name="work_scope" style="min-height:110px;" class="font-13" contenteditable></div>
                                    <input type="hidden" id="work_scope" name="work_scope">
                                </div>
                            </div>

                            <div class="col-md-4" style="border:1px solid black; height:230px;">
                            <br/>
                                <label class="font-13" for="" >AGREED BY</label>
                                <br/>
                                <div class="form-group row" style="margin-top:10px;">
                                    <label for="" class="col-md-4 font-13">STAFF ID</label>
                                    <div class="col-md-8">
                                        {{ Form::select('handover', $user, null, ['class' => 'form-control font-13', 'placeholder' => 'Select Replacement']) }}
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group row">
                                    <label for="" class="col-md-4 font-13">DATE</label>
                                    <div class="col-md-8">
                                    <p>-----------------------------------------------------</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr/><br/>

                        <div class="header">
                            <p class="col-md-12" style="text-align:center;font-weight:bold;">SUBMISSION SECTION</p>
                        </div>

                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">Requestor to get approval from dedicated Superior</b>
                        </div>

                        <div class="form-group row">
                            <p class="col-md-12" style="text-align:center">Timeframe : 3 working days</p>
                        </div>

                        <div class="form-row" style="justify-content:center;grid-gap:30px">
                            <div class="col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" style="margin-bottom:33%;">REQUESTOR</label>
                                <p>-----------------------------------------------------------------------------</p>
                                <p>DATE ---------------------------------------------------------------------</p>
                            </div>
                            
                            <div class="col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" style="margin-bottom:33%;">APPROVED BY</label>
                                <p>-----------------------------------------------------------------------------</p>
                                <p>DATE ---------------------------------------------------------------------</p>
                            </div>
                        </div>
                        <br/>
                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">System Admin require 2 DAYS to update status in System</b>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black; font-size:1.5rem;">
                            <ul>
                                <li>
                                <p>Fill all the required spaces</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <button type="submit" id="saveBtn" class="col-md-12 btn btn-danger" style="font-size:1.5rem;">Save</button> 
                            </div>

                            <div class="form-group col-md-6">
                                <button type="button" id="printBtn" class="col-md-12 btn btn-info print" style="font-size:1.5rem;" href="javascript:;" disabled>Print</button> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('form')}}" class="col-md-12 btn btn-outline-danger" style="font-size:1.5rem;">Back to Homepage</a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

function myfunc(){
    var start= $("#start").val();
    var end= $("#end").val();
    var diff = new Date(Date.parse(end) - Date.parse(start));

    var days = (diff/1000/60/60/24) + 1;

    $("#total_day").prop('value', days);
    $("div#textarea-total-day").text(days);
}

let validateForm = () => {

    $(".date ").datepicker(
        {
            format: 'yyyy-mm-dd',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });

        $('#leave').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            $("#saveBtn").attr('disabled','disabled');
                            $("#printBtn").prop('disabled',false);

                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            })
                            .then((result) => {
                            })

                        } else {

                            swal("Failed!", resp.msg, "error");

                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })    
            },
            rules: {
                reason : {
                    required:true
                },
                handover : {
                    required:true
                },
                work_scope : {
                    required:true
                },
                
            }
        });
    }

    $('#textarea-reason').on('keyup',function () {
        var mysave = $('#textarea-reason').html();
        $('#reason').val(mysave);
    });

    $('#textarea').on('keyup',function () {
        var mysave = $('#textarea').html();
        $('#work_scope').val(mysave);
    });

    $('input[name ="apply"]').each(function(key, value){
        var dateObject = $(this).datepicker('getDate'); 
        var date = new Date()
        var div = $("<div>").appendTo($(this).parent());
        var hh = ("0" + date.getDate()).slice(-2)
        var mm = ("0" + (date.getMonth() + 1)).slice(-2)
        div.css({'display': 'block', 'min-height': '0%', 'opacity': '0.7', 'background-color': '#e9ecef'}).attr('id','textarea-apply').addClass("textarea-apply font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
        $(this).hide();
    })   

    $(() => {

        $(document).on('click', "div#textarea-start", function(){
            $('div.textarea-start').hide()
            $('input[name ="start"').show()
        });

        $(document).on('click', "div#textarea-end", function(){
            $('div.textarea-end').hide()
            $('input[name ="end"').show()
        });

        $(document).on('click', "div#textarea-type-leave", function(){
            $('div.textarea-type-leave').hide()
            $('select[name ="type_leave"').show()
            $('.select2').show()
        });

        $(document).on('click', "div#textarea-handover", function(){
            $('div.textarea-handover').hide()
            $('select[name ="handover"').show()
            $('.select2').show()
        });

        $('div#textarea-balance').text({{$staff['annual_leave']}})

        validateForm();
        

        $("#start").on('change', () => {
            $("#end").prop({"readonly": false, "value": ''});
            $("#total_day").prop('value', '');
        })

        $("#end").on('change', () => {
            let edate = $("#end").val();
            myfunc();
        })
    });

    $('#type_leave').on('change', function(){
        var type = $(this).val();
        $('span.select2').addClass('font-13')
        var staff = {!! $staff !!};
        if(type == 0){
            $('#balance').val(staff.annual_leave);
            $("div#textarea-balance").text(staff.annual_leave);
        }else if(type == 1){
            $('#balance').val(staff.liue_leave);
            $("div#textarea-balance").text(staff.liue_leave);
        }else if(type == 3){
            $('#balance').val(staff.medical_leave);
            $("div#textarea-balance").text(staff.medical_leave);
        }else{
            $('#balance').val('');
            $("div#textarea-balance").text('');
        }
    });

    $('.print').on('click', function(){

        $('input[name ="start"]').each(function(key, value){
            var dateObject = $(this).datepicker('getDate'); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'display': 'block', 'min-height': '0%' }).attr('id','textarea-start').addClass("textarea-start font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        })        
        
        $('input[name ="end"]').each(function(key, value){
            var dateObject = $(this).datepicker('getDate'); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'display': 'block', 'min-height': '0%' }).attr('id','textarea-end').addClass("textarea-end font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        })

        $('select[name ="type_leave"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'display': 'block', 'min-height': '0%' }).attr('id','textarea-type-leave').addClass("textarea-type-leave font-13").text($('option:selected',this).text());
            $(this).hide();
        })

        $('select[name ="handover"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'display': 'block', 'min-height': '0%' }).attr('id','textarea-handover').addClass("textarea-handover font-13").text($('option:selected',this).text());
            $(this).hide();
        })

        window.scrollTo(0,0);

        $( "#printSection" ).removeClass('col-md-9');
        $( "#printSection" ).addClass('col-md-8');

        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            scrollX: 0,
            scrollY: 0,
            x: 0,
            y: 0,
            width: 1900,
            height: 1700
        };  

        var chart = document.querySelector("#printSection");
        var width = chart.offsetWidth;
        var height = chart.offsetHeight;
        html2canvas(chart, options).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF();

            pdf.addImage(dataURL, 'JPEG', 5, 5, width/6.5, height/6.5, '', 'SLOW');

            pdf.save("Leave_Application");
        });

        $( "#printSection" ).removeClass('col-md-8');
        $( "#printSection" ).addClass('col-md-9');
    });
    
</script>
@endpush