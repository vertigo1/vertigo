@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.5rem;
        color: black;
    }

    #textarea-job-title, #textarea-total, .textarea, #textarea-amount  {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        min-height:36px;
        border: 1px solid #ced4da;
        padding: 2px;
        width: 100%;
        background: white;
        font-weight: 400;
        padding: .375rem .75rem;
    }

    .text-danger{
        font-size: 1rem;
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9" id="printSection">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2 font-13 font-13" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">SITE ALLOWANCE FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$index}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="allowance" method="POST" action="{{route('site_allowance.post')}}">
                        @csrf
                        <div class="form-group row">
                            <b class="col-md-12 text-danger">* Each form only applicable to 1 Job Number.</b>
                            <b class="col-md-12 text-danger">* Requestor to attach all / any related documents.</b>
                            <b class="col-md-12 text-danger">* Working hour at site should be more than 8 hours.</b>
                            <b class="col-md-12 text-danger">* Site visit Safety Briefing not included.</b>
                            <b class="col-md-12 text-danger">* All application form required to be submitted within 1 month from execution date.</b>
                            <b class="col-md-12 text-danger">* Site Allowance is not applicable during outstation.</b>
                            <b class="col-md-12 text-danger">* Maximum claim limit to planned schedule in costing range.</b>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">STAFF ID</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="staff_id" value="{{$staff->staff_id}}" readonly>
                            </div>
                            <label for="" class="col-md-2 font-13">JOB TITLE</label>
                            <div class="form-group col-md-4" style="min-height:110px;">
                                <div id="textarea-job-title" name="job_title" class="font-13" style="height:100%;"></div>
                                <input type="hidden" id="job_title" name="job_title">
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">JOB NUMBER</label>
                            <div class="form-group col-md-4">
                            {{ Form::select('job_number', $orders, null,['class' => 'form-control select2 font-13', 'id' => 'job-number','placeholder'=> 'Choose Job Number' ] ) }} 
                            </div>

                            <label for="" class="col-md-2 font-13">DATE</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="date"  name="apply" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" readonly>
                            </div>
                        </div>

                        <!-- <div class="mb-4 btn btn-primary add-site" style="font-size:16px;float:right"><span><i class="fas fa-plus"></i> Add New Row</div> -->

                        <div class="table-responsive" style="overflow-y: hidden;overflow-x: hidden;">
                            <table id="table-site" class="table table-bordered font-13" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info">
                                <thead>
                                    <tr>
                                        <th style="vertical-align:middle;text-align:center;width:15%">DATE</th>
                                        <th style="vertical-align:middle;text-align:center;width:70%">SITE WORK DESCRIPTION</th>
                                        <th style="vertical-align:middle;text-align:center;width:15%">AMOUNT (RM)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input class="form-control input-group-lg font-13 date" style="text-align:center;" autocomplete="off" type="text" name="date[]" value=""></td>
                                        <td>
                                            <div id="textarea-site-work1" name="site_work[]" class="font-13 textarea" contenteditable></div>
                                            <input type="hidden" class="form-control input-group-lg font-13" autocomplete="off" name="site_work[]" value="">
                                        </td>
                                        <td>
                                            <input class="form-control input-group-lg font-13 amount" autocomplete="off" type="text" onchange="setTwoNumberDecimal(this)" name="amount[]" value="">
                                        </td>
                                    </tr>
                                <tbody>
                            </table>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-9"></div>

                            <label for="" class="col-md-1">TOTAL</label>
                            <div class="col-md-2 font-13">
                                <input class="form-control input-group-lg font-13 total_amount" style="text-align:center;" autocomplete="off" type="hidden" placeholder="RM" name="total">
                                <div id="textarea-total" class="font-13" style="text-align:center;min-height:36px;"></div>
                            </div>
                        </div>
                        <div class="mb-4 btn btn-primary add-site" style="font-size:16px;float:right"><span><i class="fas fa-plus"></i>Add New Row</div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black; font-size:1.5rem;">
                            <ul>
                                <li>
                                    <p>Fill all the required spaces.</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <button type="submit" id="saveBtn" class="col-md-12 btn btn-danger" style="font-size:1.5rem;">Save</button> 
                            </div>

                            <div class="form-group col-md-6">
                                <button type="button" id="printBtn" style="font-size:1.5rem;" class="col-md-12 btn btn-info print" href="javascript:;" disabled>Print</button> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('form')}}" class="col-md-12 btn btn-outline-danger" style="font-size:1.5rem;">Back to Homepage</a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function calcAmount() {
        var total = 0;

        $(".amount").each(function () {
            var string = this.value;
                string = string.replace(/[^\d\.\-]/g, ""); 
            var number = parseFloat(string);

            total += number;
        });
        
        $(".total_amount").val(numberWithCommas(parseFloat(total).toFixed(2)));
        $("div#textarea-total").text(numberWithCommas(parseFloat(total).toFixed(2)));

    }

    function numberWithCommas(x) {
        return x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function setTwoNumberDecimal(el) {
        el.value = numberWithCommas(parseFloat(el.value).toFixed(2));
    }

    $(() => {

        $(document).on('click', "div#textarea-date", function(){
            $('div.textarea-date').hide()
            $('input[name ="date[]"').show()
        });

        $(document).on('keyup', "[id^='textarea-site-work']", function(){
            var indexOf = $(this).attr('id').split("textarea-site-work");
            var mysave = $(this).text()
            $('input[name ="site_work[]"]').eq(indexOf[1]-1).val(mysave);
        });

        $(document).on('click', "div#textarea-amount", function(){
            $('div.textarea-amount').hide()
            $('input[name ="amount[]"').show()
        });

        $(document).on('keyup', ".amount",calcAmount);

        $("input[name='total']").on('keyup', function() {
            console.log('asds');
        })

        $(".date ").datepicker(
        {
            format: 'yyyy-mm-dd',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });

        validateForm();

        $('.select2').select2();
        var count = $('#table-site tbody tr').length + 1;

        $('.add-site').click(function () { 
            $(".date").datepicker("destroy");
            markup = 	`<tr>
                            <td>
                                <input class="form-control input-group-lg font-13 date" style="text-align:center;" autocomplete="off" type="text" name="date[]">
                            </td>
                            <td>
                                <div id="textarea-site-work${count}" name="site_work[]" class="font-13 textarea" contenteditable></div>
                                <input type="hidden" class="form-control input-group-lg font-13" autocomplete="off" name="site_work[]">
                            </td>
                            <td><input class="form-control input-group-lg font-13 amount" autocomplete="off" type="text" onchange="setTwoNumberDecimal(this)" name="amount[]"></td>
                        </tr>`; 
            tableBody = $("#table-site"); 
            tableBody.append(markup);
            count++;

            $(".date").datepicker({
                format: 'yyyy-mm-dd',
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                clearBtn: true,
                orientation: "auto" ,
            });
        });

        $(".select2").on('change', () => {
                let val = $(".select2").val();
                $('span.select2').addClass('font-13')
                var url = '{{route("form.gettitle",":id")}}';
                let urlx = url.replace(":id",val);

                $.ajax({
                    type: 'GET',
                    url:  urlx,
                    success: (resp) => {
                        $("#job_title").prop("value", resp['job_title']);
                        $("#textarea-job-title").html(resp['job_title']);
                    },
                    error: (e) => {
                        console.log(e);
                    }
                });
        })
    })

    let validateForm = () => {
        $('#allowance').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            $("#saveBtn").attr('disabled','disabled');
                            $("#printBtn").prop('disabled',false);

                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            })
                            .then((result) => {
                            })

                        } else {

                            swal("Failed!", resp.msg, "error");

                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })    
            },
            rules: {
                site_work : {
                    required:true
                },
                total: {
                    required:true
                },
                
            }
        });
    }
    
    $('.print').on('click', function(){
        
        $('input[name ="date[]"]').each(function(key, value){
            var dateObject = $(this).datepicker('getDate'); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'text-align': 'center', 'display': 'block' }).attr('id','textarea-date').addClass("textarea-date").text(`${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`);
            $(this).hide();
        })

        $('input[name ="amount[]"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'text-align': 'center', 'display': 'block' }).attr('id','textarea-amount').addClass("textarea-amount").text($(this).val());
            $(this).hide();
        })

        window.scrollTo(0,0);
        var count = $('#table-site tbody tr').length;
        var height_table = 850+150*count;
        console.log(height_table)
        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            scrollX: 0,
            scrollY: 0,
            x: 0,
            y: 0,
            width: 1180,
            height: height_table
        };  
        var chart = document.querySelector("#printSection");
        var width = chart.offsetWidth;
        var height = chart.offsetHeight;
        html2canvas(chart, options).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF();

            pdf.addImage(dataURL, 'JPEG', 0, 5, width/5.6, height/5.6, '', 'SLOW');

            pdf.save("Site_Allowance");
        });
        $('.add-site').show();
        $( "#printSection" ).removeClass('col-md-8');
        $( "#printSection" ).addClass('col-md-9');
    });
</script>
@endpush