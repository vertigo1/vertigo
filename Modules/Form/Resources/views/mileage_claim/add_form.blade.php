@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 600px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }

        .img-form {
            width: 50%;
            height: auto;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.5rem !important;
        color: black;
    }

    .text-danger{
        font-size: 1rem;
    }

    .img-form {
        width:100%; 
        height:auto;
    }

    #textarea-from, #textarea-to, #textarea-description, #textarea-reason, #textarea-date, #textarea-distance, #textarea-amount, #textarea-fuel-price, #textarea-job-number, #textarea-total, .textarea  {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        border: 1px solid #ced4da;
        padding: 2px;
        width: 100%;
        background: white;
        font-weight: 400;
        padding: .375rem .75rem;
    }

    input[type=radio] {
        border: 0px;
        width: 100%;
        height: 2rem;
    }

    .form-control {
        padding: 0px;
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9" id="printSection">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2 col-s-4 font-13" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img class="img-form" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">MILEAGE CLAIM FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$index}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="outstation" method="POST" action="{{route('mileage_claim.post')}}">
                        @csrf
                        
                        <div class="row">
                            <label for="" class="col-md-3 font-13">STAFF ID</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="text" name="staff_id" value="{{Auth::user()->staff_id}}" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-3 font-13">COMPANY CAR AVAILABILITY</label>
                            <div class="row">
                                <div class="form-group col-sm-6" style="margin-right:20px;">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="availability" value="yes" id="1">
                                        <label class="form-check-label font-13" style="white-space:nowrap;" for='1'>YES</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="availability" value="no" id='2'>
                                        <label class="form-check-label font-13" for='2'>NO</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-3 reason" style="display:none;padding:0px;">
                                <label class="font-13" for="">REASON (IF YES)</label>
                                <div id="textarea-reason" name="reason" class="font-13" contenteditable></div>
                                <input type="hidden" id="reason" autocomplete="off" type="text" name="reason">
                            </div>
                        </div>

                        <div class="table-responsive" style="overflow-y: hidden;overflow-x: hidden;">
                            <table id="table-mileage" class="table table-bordered font-13" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th style="vertical-align:middle;text-align:center;width:14%">DATE</th>
                                        <th style="vertical-align:middle;text-align:center;width:16%">JOB NUMBER</th>
                                        <th style="vertical-align:middle;text-align:center;width:14%">FROM</th>
                                        <th style="vertical-align:middle;text-align:center;width:14%">TO</th>
                                        <th style="vertical-align:middle;text-align:center;width:25%">DESCRIPTION</th>
                                        <th style="vertical-align:middle;text-align:center;width:5%">DISTANCE (KM)</th>
                                        <th style="vertical-align:middle;text-align:center;width:5%">FUEL PRICE</th>
                                        <th style="vertical-align:middle;text-align:center;width:5%">AMOUNT (RM)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="item">
                                        <td>
                                            <input class="form-control input-group-lg font-13 date" style="text-align:center;" autocomplete="off" type="text" name="date[]">
                                        </td>
                                        <td>{{ Form::select('job_number[]', $orders, null,['class' => 'form-control select2 font-13', 'id' => 'job-number','placeholder'=> 'Choose Job Number' ] ) }} </td>
                                        <td>
                                            <div id="textarea-from1" name="from[]" class="font-13 textarea" contenteditable></div>
                                            <input type="hidden" id="from" autocomplete="off" name="from[]">
                                        </td>
                                        <td>
                                            <div id="textarea-to1" name="to[]" class="font-13 textarea" contenteditable></div>
                                            <input type="hidden" id="to" autocomplete="off" name="to[]">
                                        </td>
                                        <td>
                                            <div id="textarea-description1" name="description[]" class="font-13 textarea" contenteditable></div>
                                            <input type="hidden" id="description" autocomplete="off" name="description[]">
                                        </td>
                                        <td>
                                            <input class="form-control input-group-lg font-13 distance" autocomplete="off" type="text" name="distance[]">
                                        </td>
                                        <td>
                                            <input class="form-control input-group-lg font-13 fuel_price" autocomplete="off" type="text" onchange="setTwoNumberDecimal(this)" name="fuel_price[]">
                                        </td>
                                        <td>
                                            <input class="form-control input-group-lg font-13 amount" autocomplete="off" type="text" name="amount[]">
                                        </td>
                                    </tr>
                                <tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8"></div>
                            <div class="col-md-2" style="float:right;"><p class="font-13">TOTAL</p></div>
                            <div class="col-md-2" style="font-size:14px;float:right"><span>
                                <input class="form-control input-group-lg font-13 total_amount" style="text-align:center;" autocomplete="off" type="hidden">
                                <div id="textarea-total" class="font-13" style="text-align:center;height:100%;"></div>
                            </div> 
                        </div>
                        <div id="btn-add" class="mb-4 btn btn-primary add-mileage" style="font-size:16px;float:right"><span><i class="fas fa-plus"></i>Add New Row</div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 font-13">
                <div class="card">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black; font-size:1.5rem;">
                            <ul>
                                <li>
                                    <p>Fill all the required spaces.</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <button type="submit" id="saveBtn" class="col-md-12 btn btn-danger" style="font-size:1.5rem;">Save</button> 
                            </div>

                            <div class="form-group col-md-6">
                                <button type="button" id="printBtn" class="col-md-12 btn btn-info print" style="font-size:1.5rem;" href="javascript:;" disabled>Print</button> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('form')}}" class="col-md-12  btn btn-outline-danger" style="font-size:1.5rem;">Back to Homepage</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    </form>
                    
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

    function setTwoNumberDecimal(el) {
        el.value = parseFloat(el.value).toFixed(2);
    }

    function numberWithCommas(x) {
        return x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function calcMileage() {
        $(".item").each(function () {
            var amount = 0;
            var distance = 0;
            var fuel_price = 0;
            var total_amount = 0;

            if (!isNaN(parseFloat($(this).find(".distance").val()))) {
                distance = parseFloat($(this).find(".distance").val());
            }
            if (!isNaN(parseFloat($(this).find(".fuel_price").val()))) {
                fuel_price = parseFloat($(this).find(".fuel_price").val());
            }

            amount = fuel_price*distance*0.3;
            $(this).find(".amount").val(numberWithCommas(amount.toFixed(2)));

            $(".amount").each(function () {
                var num = this.value.replace(/,/g, '');
                total_amount += parseFloat(num);
            });
            // $(".total_amount").val(total_amount.toFixed(2));
            $(".total_amount").val(numberWithCommas(total_amount.toFixed(2)));
            $("div#textarea-total").text(numberWithCommas(total_amount.toFixed(2)));
        });
    }

    $(() => {

        $(document).on('click', "div#textarea-date", function(){
            $('div.textarea-date').hide()
            $('input[name ="date[]"').show()
        });

        $(document).on('click', "div#textarea-job-number", function(){
            $('div.textarea-job-number').hide()
            $('.select2').show()
        });
        
        $(document).on('click', "div#textarea-distance", function(){
            $('div.textarea-distance').hide()
            $('input[name ="distance[]"').show()
        });

        $(document).on('click', "div#textarea-amount", function(){
            $('div.textarea-amount').hide()
            $('input[name ="amount[]"').show()
        });

        $(document).on('click', "div#textarea-fuel-price", function(){
            $('div.textarea-fuel-price').hide()
            $('input[name ="fuel_price[]"').show()
        });

        $(document).on('keyup', "[id^='textarea-from']", function(){
            var indexOf = $(this).attr('id').split("textarea-from");
            var mysave = $(this).text()
            $('input[name ="from[]"]').eq(indexOf[1]-1).val(mysave);
        });

        $(document).on('keyup', "[id^='textarea-to']", function(){
            var indexOf = $(this).attr('id').split("textarea-to");
            var mysave = $(this).text()
            $('input[name ="to[]"]').eq(indexOf[1]-1).val(mysave);
        });

        $(document).on('keyup', "[id^='textarea-description']", function(){
            var indexOf = $(this).attr('id').split("textarea-description");
            var mysave = $(this).text()
            $('input[name ="description[]"]').eq(indexOf[1]-1).val(mysave);
        });

        $('#textarea-reason').on('keyup',function () {
                var mysave = $('#textarea-reason').html();
                $('#reason').val(mysave);
        });

        $(document).on('keyup', ".distance, .fuel_price", calcMileage);

        $(".date").datepicker(
        {
            format: 'yyyy-mm-dd',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "auto"
        });

        validateForm();

        $('.select2').select2();

        var count = $('#table-mileage tbody tr').length + 1;

        $('.add-mileage').click(function () { 

            $(".date").datepicker("destroy");

            $(".select2.select2-hidden-accessible").select2('destroy');

            markup = 	`<tr class="item">
                            <td>
                                <input class="form-control input-group-lg font-13 date" style="text-align:center;" autocomplete="off" type="text" name="date[]">
                            </td>
                            <td>{{ Form::select('job_number[]', $orders, null,['class' => 'form-control select2 font-13', 'id' => 'job-number','placeholder'=> 'Choose Job Number' ] ) }} </td>
                            <td>
                                <div id="textarea-from${count}" name="from[]" class="font-13 textarea" contenteditable></div>
                                <input type="hidden" id="from" autocomplete="off" name="from[]">
                            </td>
                            <td>
                                <div id="textarea-to${count}" name="to[]" class="font-13 textarea" contenteditable></div>
                                <input type="hidden" id="to" autocomplete="off" name="to[]">
                            </td>
                            <td>
                                <div id="textarea-description${count}" name="description[]" class="font-13 textarea" contenteditable></div>
                                <input type="hidden" id="description" autocomplete="off" name="description[]">
                            </td>
                            <td>
                                <input class="form-control input-group-lg font-13 distance" autocomplete="off" type="text" name="distance[]">
                            </td>
                            <td>
                                <input class="form-control input-group-lg font-13 fuel_price" autocomplete="off" type="text" onchange="setTwoNumberDecimal(this)" name="fuel_price[]">
                            </td>
                            <td>
                                <input class="form-control input-group-lg font-13 amount" autocomplete="off" type="text" name="amount[]">
                            </td>
                        </tr>`; 
            tableBody = $("#table-mileage"); 
            tableBody.append(markup);
            count++;

            $(".date").datepicker(
            {
                format: 'yyyy-mm-dd',
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                clearBtn: true,
                orientation: "auto"
            });

            $('.select2').select2();
        });

        $('input[name=availability]').change(function(){
            var value = $(this).val();

            if(value == 'yes'){
                $('.reason').show();
            }else{
                $('.reason').hide();
            }
        });
    })


    let validateForm = () => {
        $('#outstation').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            $("#saveBtn").attr('disabled','disabled');
                            $("#printBtn").prop('disabled',false);

                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            })
                            .then((result) => {
                            })

                        } else {

                            swal("Failed!", resp.msg, "error");

                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })    
            },
            rules: {
                "distance[]" : {
                    required:true
                },
                "fuel_price[]" : {
                    required:true
                },
                total_amount : {
                    required:true
                },
                "job_number[]" : {
                    required:true
                },
                "description[]" : {
                    required:true
                },
                "from[]" : {
                    required:true
                },
                "to[]" : {
                    required:true
                },
                
            },
            errorPlacement: function(error, element) {
                if(element.attr("id") == "distance") {
                    error.insertAfter($(".errors1"));
                }
                else if(element.attr("id") == "fuel-price") {
                    error.insertAfter($(".errors2"));
                }
                else if(element.attr("id") == "amount") {
                    error.insertAfter($(".errors3"));
                }
                else {
                    error.insertAfter(element);
                }
            }
        });
    } 

    $('.print').on('click', function(){

        $('select').children('option:selected').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent().parent());
            div.css({ 'text-align': 'center', 'display': 'block' }).attr('id','textarea-job-number').addClass("textarea-job-number").text($(this).parent().val());
            $('.select2').hide();
        })

        $('input[name ="date[]"]').each(function(key, value){
            var dateObject = $(this).datepicker('getDate'); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'display': 'block', 'min-height': '0%'}).attr('id','textarea-date').addClass("textarea-date font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        })   

        $('input[name ="distance[]"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'text-align': 'center', 'display': 'block' }).attr('id','textarea-distance').addClass("textarea-distance").text($(this).val());
            $(this).hide();
        })

        $('input[name ="amount[]"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'text-align': 'center', 'display': 'block' }).attr('id','textarea-amount').addClass("textarea-amount").text($(this).val());
            $(this).hide();
        })

        $('input[name ="fuel_price[]"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'text-align': 'center', 'display': 'block' }).attr('id','textarea-fuel-price').addClass("textarea-fuel-price").text($(this).val());
            $(this).hide();
        })

        window.scrollTo(0,0);
        $('#btn-add').hide()
        var count = $('#table-mileage tbody tr').length;
        var height_table = 700+150*count;
        console.log(height_table)
        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            scrollX: 0,
            scrollY: 0,
            x: 0,
            y: 0,
            // width: 2100,
            // height: height_table
        };        
        var chart = document.querySelector("#printSection");
        var width = chart.offsetWidth;
        var height = chart.offsetHeight;
        html2canvas(chart, options).then(canvas => {

            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF({orientation: 'l', compress:true});

            var pageWidth = pdf.internal.pageSize.width;
            var pageHeight = pdf.internal.pageSize.height;
            console.log(canvas.width)
            var widthRatio = pageWidth / canvas.width;
            var heightRatio = pageHeight / canvas.height;
            var ratio = widthRatio > heightRatio ? heightRatio : widthRatio;

            if(canvas.width <= 1740){
                var canvasWidth = canvas.width * (ratio + 0.028);
                var canvasHeight = canvas.height * (ratio);
                pdf.addImage(dataURL, 'JPEG', 0, 2, canvasWidth, canvasHeight, undefined, 'FAST');
            }else if(canvas.width <= 2184){
                var canvasWidth = canvas.width * (ratio);
                var canvasHeight = canvas.height * (ratio);
                pdf.addImage(dataURL, 'JPEG', 0, 2, canvasWidth, canvasHeight, undefined, 'FAST');
            }else{
                var canvasWidth = canvas.width * (ratio);
                var canvasHeight = canvas.height * (ratio);
                pdf.addImage(dataURL, 'JPEG', 0, 2, canvasWidth, canvasHeight, undefined, 'FAST');
            }  

            pdf.save("Mileage_Claim");
        });
        $('#btn-add').show()
        // $('.header-logo').css('width','55%');
    });
</script>
@endpush