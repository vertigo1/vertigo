@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.5rem;
        color: black;
    }

    #textarea, #textarea-date, #textarea-clinic-name, #textarea-balance {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        border: 1px solid #ced4da;
        min-height: 100%;
        padding: 2px;
        width: 100%;
        background: white;
        font-weight: 400;
        padding: .375rem .75rem;
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-6" id="printSection">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">MEDICAL CLAIM FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$index}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="medical" method="POST" action="{{route('medical_claim.post')}}">
                        @csrf
                        <div class="row">
                            <label for="" class="col-md-2 font-13">STAFF ID</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="staff_id" value="{{$staff->staff_id}}" readonly>
                            </div>

                            <label for="" class="col-md-2 font-13">MONTH APPLY</label>
                            <div class="form-group col-md-4">
                                <select class="form-control input-group-lg font-13 select2" autocomplete="off" type="text"  id="month" name="month" value="" >
                                    <option value="">Choose Month</option>
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August"> August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                </select>
                            </div>
                        </div>

                        <br><br>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">AMOUNT RECEIPT</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  onchange="setTwoNumberDecimal(this)" step="0.01" placeholder="RM" name="amount_receipt" value="">
                            </div>

                            <label for="" class="col-md-6 font-13">MONTHLY  APPLICABLE BALANCE AMOUNT</label>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">DATE (RECEIPT)</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg font-13 date" autocomplete="off" type="text"  name="date" value="">
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">RM</div>
                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="hidden"  name="" value="" id="balance" readonly>
                                    <div id="textarea-balance" style="opacity: 0.7;background-color: #e9ecef;" class="font-13"></div>
                                </div>
                            </div>
                        </div>

                        <br><br>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">CLINIC NAME</label>
                            <div class="form-group col-md-10">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="text" name="clinic_name" value="">
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">REASON</label>
                            <div class="form-group col-md-10" style="min-height:110px;">
                                <div id="textarea" name="reason" class="font-13" contenteditable></div>
                                <input type="hidden" id="reason" name="reason">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black;font-size:1.5rem;">
                            <ul>
                                <li>
                                    <p>Fill all the required spaces.</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <button type="submit" id="saveBtn" class="col-md-12 btn btn-danger" style="font-size: 1.5rem;">Save</button> 
                            </div>

                            <div class="form-group col-md-6">
                                <button type="button" id="printBtn" class="col-md-12 btn btn-info print" style="font-size: 1.5rem;" href="javascript:;" disabled>Print</button> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('form')}}" class="col-md-12 btn btn-outline-danger" style="font-size: 1.5rem;">Back to Homepage</a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    var balance1 = '{!! $claim->January !!}';
    var balance2 = '{!! $claim->February !!}';
    var balance3 = '{!! $claim->March !!}';
    var balance4 = '{!! $claim->April !!}';
    var balance5 = '{!! $claim->May !!}';
    var balance6 = '{!! $claim->June !!}';
    var balance7 = '{!! $claim->July !!}';
    var balance8 = '{!! $claim->August !!}';
    var balance9 = '{!! $claim->September !!}';
    var balance10 = '{!! $claim->October !!}';
    var balance11 = '{!! $claim->November !!}';
    var balance12 = '{!! $claim->December !!}';

    function setTwoNumberDecimal(el) {
        el.value = parseFloat(el.value).toFixed(2);
    }

    $(() => {

        $(document).on('click', "div#textarea-date", function(){
            $('div.textarea-date').hide()
            $('input[name ="date"').show()
        });

        $(document).on('click', "div#textarea-clinic-name", function(){
            $('div.textarea-clinic-name').hide()
            $('input[name ="clinic_name"').show()
        });

        $("#month").change(function() {
            $("span.select2").addClass('font-13');
            if(this.value == "January") {
                $("#balance").val(parseFloat(balance1).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance1).toFixed(2));
            } 
            else if(this.value == "February") {
                $("#balance").val(parseFloat(balance2).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance2).toFixed(2));

            }
            else if(this.value == "March") {
                $("#balance").val(parseFloat(balance3).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance3).toFixed(2));
            }
            else if(this.value == "April") {
                $("#balance").val(parseFloat(balance4).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance4).toFixed(2));
            }
            else if(this.value == "May") {
                $("#balance").val(parseFloat(balance5).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance5).toFixed(2));
            }
            else if(this.value == "June") {
                $("#balance").val(parseFloat(balance6).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance6).toFixed(2));
            }
            else if(this.value == "July") {
                $("#balance").val(parseFloat(balance7).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance7).toFixed(2));
            }
            else if(this.value == "August") {
                $("#balance").val(parseFloat(balance8).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance8).toFixed(2));
            }
            else if(this.value == "September") {
                $("#balance").val(parseFloat(balance9).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance9).toFixed(2));
            }
            else if(this.value == "October") {
                $("#balance").val(parseFloat(balance10).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance10).toFixed(2));
            }
            else if(this.value == "November") {
                $("#balance").val(parseFloat(balance11).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance11).toFixed(2));
            }
            else if(this.value == "December") {
                $("#balance").val(parseFloat(balance12).toFixed(2));
                $("#textarea-balance").text(parseFloat(balance12).toFixed(2));
            }
            else if(this.value == "") {
                $("#balance").val("");
                $("#textarea-balance").text("");
            }
        })

        validateForm();

        $(".select2").select2();

        $(".date ").datepicker(
        {
            format: 'yyyy-mm-dd',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });

        $('#textarea').on('keyup',function () {
            var mysave = $('#textarea').html();
            $('#reason').val(mysave);
        });
    })

    let validateForm = () => {
        $('#medical').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            $("#saveBtn").attr('disabled','disabled');
                            $("#printBtn").prop('disabled',false);

                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            })
                            .then((result) => {
                            })

                        } else {

                            swal("Failed!", resp.msg, "error");

                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })    
            },
            rules: {
                clinic_name : {
                    required:true
                },
                reason: {
                    required:true
                },
                date: {
                    required:true
                },
                amount_receipt: {
                    required:true
                },
                month: {
                    required:true
                },
                
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            }
        });
    }

    $('.print').on('click', function(){

        $('input[name ="date"]').each(function(key, value){
            var dateObject = $(this).datepicker('getDate'); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'display': 'block', 'min-height': '0%' }).attr('id','textarea-date').addClass("textarea-date font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        })

        $('input[name ="clinic_name"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'display': 'block', 'min-height': '0%' }).attr('id','textarea-clinic-name').addClass("textarea-clinic-name font-13").text($(this).val());
            $(this).hide();
        })

        window.scrollTo(0,0);
        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            scrollX: 0,
            scrollY: 0,
            x: 0,
            y: 0,
            width: 1200,
            height: 850
        };  
        var chart = document.querySelector("#printSection");
        var width = chart.offsetWidth;
        var height = chart.offsetHeight;
        html2canvas(chart, options).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF();

            pdf.addImage(dataURL, 'JPEG', 0, 5, width/5.6, height/5.6, '', 'SLOW');

            pdf.save("Medical_Form");
        });

        $( "#printSection" ).removeClass('col-md-6');
        $( "#printSection" ).addClass('col-md-9');
    });
</script>
@endpush