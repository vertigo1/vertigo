@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.5rem;
        color: black;
    }

    #textarea, #textarea-desc, #textarea-demob, #textarea-mob, #textarea-start, #textarea-end, #textarea-location, #textarea-total-accom, #textarea-total-allowance, #textarea-total {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        border: 1px solid #ced4da;
        min-height: 100%;
        padding: 2px;
        width: 100%;
        background: white;
        font-weight: 400;
        padding: .375rem .75rem;
    }

    .text-danger{
        font-size: 1rem;
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9" id="printSection">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2 font-13" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">OUTSTATION CLAIM FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$index}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="outstation" method="POST" style="padding-bottom:10px;" action="{{route('outstation_claim.post')}}">
                        @csrf
                        <div class="form-group row">
                            <b class="col-md-12 text-danger">* Each form only applicable to 1 Job Number.</b>
                            <b class="col-md-12 text-danger">* Requestor to attach all / any related documents.</b>
                            <b class="col-md-12 text-danger">* Minimum radius distance is 220 kilometre.</b>
                            <b class="col-md-12 text-danger">* All outstation activities prior to Superior approval.</b>
                            <b class="col-md-12 text-danger">* All application form required to be submitted within 1 month from execution date.</b>
                            <b class="col-md-12 text-danger">* Site Allowance is not applicable during outstation.</b>
                            <b class="col-md-12 text-danger">* Maximum claim limit to planned schedule in costing range.</b>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">STAFF ID</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="staff_id" value="{{$staff->staff_id}}" readonly>
                            </div>

                            <label for="" class="col-md-2 font-13">JOB NUMBER</label>
                            <div class="form-group col-md-4">
                                {{-- <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="job_number" value=""> --}}
                                {{ Form::select('job_number', $orders, null,['class' => 'form-control select2 font-13', 'id' => 'job-number', 'placeholder' => 'Select Job Number']) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6"></div>

                            <label for="" class="col-md-2 font-13">JOB TITLE</label>
                            <div class="form-group col-md-4" style="min-height:110px;">
                                <div id="textarea" name="job_title" class="font-13"></div>
                                <input type="hidden" id="job_title" name="job_title">
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">DESCRIPTION</label>
                            <div class="form-group col-md-10" style="min-height:110px;">
                                <div id="textarea-desc" name="description" class="font-13" contenteditable></div>
                                <input type="hidden" id="description" name="description">
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">LOCATION</label>
                            <div class="form-group col-md-10">
                                <input class="form-control font-13" autocomplete="off"  name="location">
                            </div>
                        </div>
                        <br/>
                        <div class="row" style="justify-content:center;grid-gap:20px;">
                            <div class="col-md-12" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:200px;margin-top:-30px;margin-left:10px;background:white;text-align:center">Out-Station Status</p>
                                <div class="row">
                                    <div class="form-group col-sm-4">
                                        <label style="white-space:nowrap;" for="">STAFF TYPE</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="exec" checked>
                                            <label class="form-check-label" for="gridRadios1">
                                                Executive
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="non_exec">
                                            <label class="form-check-label" for="gridRadios2">
                                                Non Executive
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="">ACCOMMODATION TYPE</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="grid" id="grid1" value="companyarr" checked>
                                            <label class="form-check-label" for="grid1">
                                                COMPANY ARRANGEMENT
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="grid" id="grid2" value="selfarr">
                                            <label class="form-check-label" for="grid2">
                                                SELF ARRANGEMENT
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="" >STAFF ALLOWANCE RATE</label>
                                        <div class="input-group-prepend" style="display: none;">
                                            <div class="input-group-text">RM</div>
                                            <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="rate" value="{{$staff->site_allowance}}" id="rate" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br/>
                        <br/>
                        <div class="row" style="justify-content:center;grid-gap:20px;">
                            <div class="col-md-12" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:150px;margin-top:-30px;margin-left:10px;background:white;text-align:center">Calculation</p>
                                <div class="row">
                                    <label for="" class="col-md-3">TRAVELLING DATE (MOB)</label>
                                    <div class="form-group col-md-3">
                                        <input class="form-control input-group-lg font-13 date" autocomplete="off" type="text"  name="mob" value="" id="mob">
                                    </div>

                                    <label for="" class="col-md-3">TRAVELLING DATE (DE-MOB)</label>
                                    <div class="form-group col-md-3">
                                        <input class="form-control input-group-lg font-13 date" autocomplete="off" type="text"  name="demob" value="" id="demob" readonly>
                                    </div>
                                </div>

                                <div class="row">
                                    <label for="" class="col-md-3">ACCOMODATION DATE (START)<sup style="color:red">&#10033 Start working day</sup></label>
                                    <div class="form-group col-md-3">
                                        <input class="form-control input-group-lg font-13 date" autocomplete="off" type="text"  name="start" value="" id="start">
                                    </div>

                                    <label for="" class="col-md-3">ACCOMODATION DATE (END)<sup style="color:red">&#10033 Work Completion after 9pm will be considered to demob next day</sup></label>
                                    <div class="form-group col-md-3">
                                        <input class="form-control input-group-lg font-13 date" autocomplete="off" type="text"  name="end" value="" id="end" readonly>
                                    </div>
                                </div>
                                <br/>
                                <div class="row" style="justify-content:center;grid-gap:20px;">
                                    <div class="col-md-4">
                                        <div class="row" style="justify-content:center;grid-gap:20px;">
                                            <div class="col-md-12" style="border:1px solid black;padding:15px">
                                                <p style="text-align:center;font-weight:bold;width:250px;margin-top:-30px;margin-left:40px;background:white;">TOTAL TRAVELLING DAY</p>
                                                <div class="row" style="justify-content:center;">
                                                    <label style="text-align:center;" class="col-md-12">TOTAL APPLICABLE ALLOWANCE DAY(S) INCLUDING TRAVELLING DAYS (MOB / DE-MOB)</label>
                                                    <div class="form-group col-md-4">
                                                        <input class="form-control input-group-lg font-13" autocomplete="off" type="hidden"  name="total_allowance" value="" id="total_allowance" readonly>
                                                        <div id="textarea-total-allowance" class="font-13" style="text-align:center;height:38px;opacity: 0.7;background-color: #e9ecef;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="row" style="justify-content:center;grid-gap:20px;height:100%;">
                                            <div class="col-md-12" style="border:1px solid black;padding:15px">
                                                <p style="text-align:center;font-weight:bold;width:250px;margin-top:-30px;margin-left:40px;background:white;">TOTAL ACCOMODATION DAY</p>
                                                <div class="row" style="justify-content:center;">
                                                    <label style="text-align:center;" class="col-md-12">ONLY APPLICABLE ON SELF-ARRANGEMENT STATUS</label>
                                                    <div class="form-group col-md-4">
                                                        <input class="form-control input-group-lg font-13 total_accom" autocomplete="off" type="hidden" name="total_accom" value="" id="total_accom">
                                                        <div id="textarea-total-accom" class="font-13" style="text-align:center;height:38px;opacity: 0.7;background-color: #e9ecef;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="form-group col-md-6"></div>

                                    <label for="" class="col-md-1">TOTAL</label>
                                    <div class="form-group col-md-3">
                                        <input class="form-control input-group-lg font-13" autocomplete="off" type="hidden" placeholder="RM" name="total_amount" value="" id="total">
                                        <div id="textarea-total" class="font-13" style="text-align:center;height:100%;opacity: 0.7;background-color: #e9ecef;"></div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <a class="col-md-12 btn btn-info" style="color: aliceblue; font-size: 16px;" onclick="calculate()">Calculate</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black;font-size:1.5rem;">
                            <ul>
                                <li>
                                    <p>Fill all the required spaces.</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                {{-- <a class="col-md-12 btn btn-danger">Save</a> --}}
                                <button type="submit" id='saveBtn' class="col-md-12 btn btn-danger" style="font-size:1.5rem;">Save</button>
                            </div>

                            <div class="form-group col-md-6">
                                <button type="button" id='printBtn' class="col-md-12 btn btn-info print" href="javascript:;" style="font-size:1.5rem;" disabled>Print</button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('form')}}" class="col-md-12 btn btn-outline-danger" style="font-size:1.5rem;">Back to Homepage</a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(() => {

        $(document).on('click', "div#textarea-mob", function(){
            $('div.textarea-mob').hide()
            $('input[name ="mob"').show()
        });

        $(document).on('click', "div#textarea-demob", function(){
            $('div.textarea-demob').hide()
            $('input[name ="demob"').show()
        });

        $(document).on('click', "div#textarea-start", function(){
            $('div.textarea-start').hide()
            $('input[name ="start"').show()
        });

        $(document).on('click', "div#textarea-end", function(){
            $('div.textarea-end').hide()
            $('input[name ="end"').show()
        });

        $(document).on('click', "div#textarea-location", function(){
            $('div.textarea-location').hide()
            $('input[name ="location"').show()
        });

        validateForm();

        $(".date ").datepicker(
        {
            format: 'yyyy-mm-dd',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });

        $(".select2").select2();
        $('.total_accom').attr('readonly',true);

        $(".select2").on('change', () => {
            let val = $(".select2").val();
            $('span.select2').addClass('font-13')
            let url = '{{route("form.gettitle",":id")}}';
            let urlx = url.replace(":id",val);

            $.ajax({
                type: 'GET',
                url:  urlx,
                success: (resp) => {
                    $("#job_title").prop("value", resp['job_title']);
                    $("#textarea").html(resp['job_title']);
                },
                error: (e) => {
                    console.log(e);
                }
            });
        })

        $("input[name='grid']").change(function(){
            let value = $(this).val();

            if(value == 'companyarr'){
                $('.total_accom').attr('readonly',true)
            }else{
                $('.total_accom').attr('readonly',false)
            }
        });

        $("#start").on('change', () => {
            $("#end").prop({"readonly": false, "value": ''});
            $("#total_day").prop('value', '');
        })

        $("#end").on('change', () => {
            myfunc();
        })

        $("#mob").on('change', () => {
            $("#demob").prop({"readonly": false, "value": ''});
            $("#total_day").prop('value', '');
        })

        $("#demob").on('change', () => {
            myfunc1();
        })

        $('#textarea-desc').on('keyup',function () {
            var mysave = $('#textarea-desc').html();
            $('#description').val(mysave);
        });
  
    })

function numberWithCommas(x) {
    return x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function setTwoNumberDecimal(el) {
    el.value = numberWithCommas(parseFloat(el.value).toFixed(2));
}

function myfunc1(){
    let start= $("#mob").val();
    let end= $("#demob").val();
    let diff = new Date(Date.parse(end) - Date.parse(start));

    let days = (diff/1000/60/60/24) + 1;

    $("#total_allowance").prop('value', days);
    $("div#textarea-total-allowance").text(days);
}

function myfunc(){
    let start= $("#start").val();
    let end= $("#end").val();
    let diff = new Date(Date.parse(end) - Date.parse(start));

    let days = (diff/1000/60/60/24) + 1;

    $("#total_accom").prop('value', days);
    $("div#textarea-total-accom").text(days);
}

function calculate() {
    let staff_type = $("input[name='gridRadios']:checked").val();
    let accom_type = $("input[name='grid']:checked").val();

    let accom = accom_type == 'companyarr' ? 0 : 1;
    let staff = staff_type == 'non_exec' ? 120 : 150;
    let rate = parseInt($("#rate").val());
    let total_accom = $("#total_accom").val() == "" ? 0 : parseInt($("#total_accom").val());
    let total_allowance = parseInt($("#total_allowance").val());

    let total = (staff * accom * total_accom) + (total_allowance * rate);

    $("#total").prop({"readonly":true, "value": numberWithCommas(parseFloat(total).toFixed(2))});
    $("div#textarea-total").text(numberWithCommas(parseFloat(total).toFixed(2)));
}

let validateForm = () => {
        $('#outstation').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            $("#saveBtn").attr('disabled','disabled');
                            $("#printBtn").prop('disabled',false);

                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            })
                            .then((result) => {
                            })

                        } else {

                            swal("Failed!", resp.msg, "error");

                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })    
            },
            rules: {
                job_number : {
                    required:true
                },
                description : {
                    required:true
                },
                location: {
                    required:true
                },
                rate: {
                    required:true
                },
                mob: {
                    required:true
                },
                demob: {
                    required:true
                },
                total_amount: {
                    required:true
                },
            }
        });
    }

    $('.print').on('click', function(){

        $('input[name ="mob"]').each(function(key, value){
            var dateObject = $(this).datepicker('getDate'); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'display': 'block', 'text-align': 'center', 'min-height': '0%' }).attr('id','textarea-mob').addClass("textarea-mob font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        })        
        
        $('input[name ="demob"]').each(function(key, value){
            var dateObject = $(this).datepicker('getDate'); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'display': 'block', 'text-align': 'center', 'min-height': '0%' }).attr('id','textarea-demob').addClass("textarea-demob font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        })

        $('input[name ="start"]').each(function(key, value){
            var dateObject = $(this).datepicker('getDate'); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'display': 'block', 'text-align': 'center', 'min-height': '0%' }).attr('id','textarea-start').addClass("textarea-start font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        })        
        
        $('input[name ="end"]').each(function(key, value){
            var dateObject = $(this).datepicker('getDate'); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'display': 'block', 'text-align': 'center', 'min-height': '0%' }).attr('id','textarea-end').addClass("textarea-end font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        })

        $('input[name ="location"]').each(function(key, value){
            var div = $("<div>").appendTo($(this).parent());
            div.css({ 'display': 'block', 'min-height': '0%' }).attr('id','textarea-location').addClass("textarea-location font-13").text($(this).val());
            $(this).hide();
        })

        window.scrollTo(0,0);

        $( "#printSection" ).removeClass('col-md-9');
        $( "#printSection" ).addClass('col-md-8');

        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            scrollX: 0,
            scrollY: 0,
            x: 0,
            y: 0,
            width: 1900,
            height: 1700
        };  

        var chart = document.querySelector("#printSection");
        var width = chart.offsetWidth;
        var height = chart.offsetHeight;
        html2canvas(chart, options).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF();

            pdf.addImage(dataURL, 'JPEG', 5, 5, width/6.5, height/6.5, '', 'SLOW');

            pdf.save("Outstation_Claim");
        });

        $( "#printSection" ).removeClass('col-md-8');
        $( "#printSection" ).addClass('col-md-9');
    });
</script>
@endpush