<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class PurchaseRequest extends Model
{
    protected $table = 'purchase_request';
    protected $guarded = 'id';
}
