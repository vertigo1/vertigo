<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class SiteWork extends Model
{
    protected $table = 'site_work';
    protected $guarded = 'id';
}
