<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class MedicalClaim extends Model
{
    protected $table = 'medical_claim';
    protected $guarded = 'id';
}
