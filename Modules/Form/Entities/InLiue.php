<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class InLiue extends Model
{
    protected $table = 'off_in_liue';
    protected $guarded = 'id';

    public function work() {
        return $this->hasMany(Work::class, 'form_id','form_id');
    }
}