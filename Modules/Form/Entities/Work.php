<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $table = 'work';
    protected $guarded = 'id';

    public function inlieu() {
        return $this->belongsto(InLiue::class, 'form_id','form_id');
    }

}