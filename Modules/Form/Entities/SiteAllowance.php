<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class SiteAllowance extends Model
{
    protected $table = 'site_allowance';
    protected $guarded = 'id';
}
