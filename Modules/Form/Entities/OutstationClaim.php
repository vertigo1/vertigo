<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class OutstationClaim extends Model
{
    protected $table = 'outstation_claim';
    protected $guarded = 'id';
}
