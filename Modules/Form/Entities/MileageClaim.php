<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class MileageClaim extends Model
{
    protected $table = 'mileage_claim';
    protected $guarded = 'id';
}
