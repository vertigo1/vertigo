<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class OutgoingPayment extends Model
{
    protected $table = 'outgoing_payment';
    protected $guarded = 'id';
}
