<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class LeaveApplication extends Model
{
    protected $table = 'leave_application';
    protected $guarded = 'id';
}
