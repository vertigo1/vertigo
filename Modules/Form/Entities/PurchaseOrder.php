<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $table = 'purchase_order';
    protected $guarded = 'id';

    public function outgoing() {
        return $this->hasMany(OutgoingPayment::class, 'form_id','form_id');
    }

    public function incoming() {
        return $this->hasMany(IncomingPayment::class, 'form_id','form_id');
    }
}
