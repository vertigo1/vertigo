<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class IncomingPayment extends Model
{
    protected $table = 'incoming_payment';
    protected $guarded = 'id';
}
