<?php

namespace Modules\Form\Entities;
use Illuminate\Database\Eloquent\Model;

class MedicalAmount extends Model
{
    protected $table = 'medical_amount';
    protected $guarded = 'id';

    public function medical() {
        return $this->belongsTo(User::class, 'staff_id');
    }

}
