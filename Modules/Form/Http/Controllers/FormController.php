<?php

namespace Modules\Form\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use Auth;
use Modules\Form\Entities\PurchaseOrder;
use Modules\Form\Entities\IncomingPayment;
use Modules\Form\Entities\OutgoingPayment;
use Modules\Form\Entities\PurchaseRequest;
use Modules\Form\Entities\OutstationClaim;
use Modules\Form\Entities\MedicalClaim;
use Modules\Form\Entities\InLiue;
use Modules\Form\Entities\Work;
use Modules\Form\Entities\LeaveApplication;
use Modules\Form\Entities\MileageClaim;
use Modules\Form\Entities\SiteAllowance;
use Modules\Form\Entities\SiteWork;
use Modules\Form\Entities\MedicalAmount;
use Carbon\Carbon;

class FormController extends Controller
{
    public function index(){
        return view('form::index');
    }

    public function add_purchase_order(){

        $id = Auth::id();
        $staff = User::find($id);

        $index = intval(json_encode(PurchaseOrder::orderBy('id','desc')->pluck('id')->first())) + 1; 
        
        return view('form::purchase_order.add_form',compact('staff','index'));
    }

    public function post_purchase_order(Request $request){
        try {

            $month = date('m');
            $year = date('y');

            $index = intval(json_encode(PurchaseOrder::orderBy('id','desc')->pluck('id')->first())) + 1;            

            $order = new PurchaseOrder();

            $order->form_id = strval($index);
            // $order->job_number = Auth::user()->region.$year.$month.$request->job_number;
            $order->job_number = Auth::user()->region.$request->job_number;
            $order->client_ref_no = $request->client_ref_no;
            $order->client_name = $request->client_name;
            $order->job_title = $request->job_title;
            $order->po_date = $request->po_date;
            $order->po_value = $request->po_value;
            $order->po_status = 1;
            $order->staff_id = $request->project_owner;
            $order->type_po = $request->type_po;

            if(isset($request->total_outgoing)) {
                $order->total_outgoing = $request->total_outgoing;
            }

            $saved = $order->save();

            if(isset($request->expected_date_in)) {
                for($i = 0; $i<count($request->expected_date_in); $i++){
                    $incoming = new IncomingPayment();
                    $incoming->form_id = $order->form_id;
                    $incoming->expected_date = $request->expected_date_in[$i];
                    $incoming->amount = $request->amount_in[$i];
                    $incoming->save();
                }
               
            }
    
            if(isset($request->subcont_name)) {
                for($j = 0; $j<count($request->subcont_name); $j++){
                    $outgoing = new OutgoingPayment();
                    $outgoing->form_id = $order->form_id;
                    $outgoing->subcont_name = $request->subcont_name[$j];
                    $outgoing->description = $request->description[$j];
                    $outgoing->expected_date = $request->expected_date_out[$j];
                    $outgoing->amount = $request->amount_out[$j];
                    $outgoing->save();
                }
            }

            if($saved) {
                $status = true;
                $msg = 'Success! Order Create created.';
                $url = route('form');
            }
        }
        catch(\Exception $e) {
            if ($e->getCode() == 23000) {
                // Deal with duplicate key error  
                $status = false;
                $msg = 'Job Number Duplicate!';
                $url = null;
            }
            else {
                $status = false;
                $msg = $e->getMessage();
                $url = null;
            }
        }
        
        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function add_purchase_request(){

        $id = Auth::id();
        $staff = User::find($id);
        $excludeJob = ['KMM2013000', 'KMM2014000', 'KMM2015000', 'KMM2016000', 'KMM2017000','SBH2013000', 'SBH2014000', 'SBH2015000', 'SBH2016000', 'SBH2017000','MLK2013000', 'MLK2014000', 'MLK2015000', 'MLK2016000', 'MLK2017000'];
        $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->whereNotIn('job_number',$excludeJob)->get()->pluck('job_number', 'job_number');
        $index = intval(json_encode(PurchaseRequest::orderBy('id','desc')->pluck('id')->first())) + 1; 

        return view('form::purchase_request.add_form', compact('staff','index','orders'));
    }
   

    public function post_purchase_request(Request $request) {
        $job = $request->job_number;
        $region = Auth::user()->region;
        $month = date('m');
        $year = date('y');  
        if($request->job_number == 0 || $request->job_number == null) {
            if($request->job_title == "Workshop Expenses") $job = $region.$year.'13000';
            else if($request->job_title == "Office Expenses") $job = $region.$year.'14000';
            else if($request->job_title == "Investment") $job = $region.$year.'15000';
            else if($request->job_title == "Transport") $job = $region.$year.'16000';
            else if($request->job_title == "Monthly Commitment") $job = $region.$year.'17000';
        }
       
           
        
        
        try {
            $index = intval(json_encode(PurchaseRequest::orderBy('id','desc')->pluck('id')->first())) + 1; 

            $purchasereq = new PurchaseRequest();

            $purchasereq->form_id = strval($index);
            $purchasereq->staff_id = $request->staff_id;
            $purchasereq->job_number = $job;
            $purchasereq->date = $request->date;
            $purchasereq->amount = $request->amount;
            $purchasereq->description = $request->purpose;
            $purchasereq->supplier = $request->supplier;
            $purchasereq->category = $request->gridRadios;
            $purchasereq->status = 0;
            $purchasereq->bank_slip = 0;

            $saved = $purchasereq->save();

            if($saved) {
                $status = true;
                $msg = 'Success! Purchase request created.';
                $url = route('form');
            }
        }
        catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }

        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function add_medical_claim(){
        
        $staff = User::find(Auth::id());
        $claim = MedicalAmount::where('staff_id',Auth::user()->staff_id)->first();
        $index = intval(json_encode(MedicalClaim::orderBy('id','desc')->pluck('id')->first())) + 1;
        return view('form::medical_claim.add_form',compact('staff','index', 'claim'));
    }

    public function post_medical_claim(Request $request){
        try {
            $index = intval(json_encode(MedicalClaim::orderBy('id','desc')->pluck('id')->first())) + 1; 

            $month = $request->month;
            $user = User::find(Auth::id());
            $claim = MedicalAmount::where('staff_id',Auth::user()->staff_id)->get()->first();
            if($claim->$month <  intval($request->amount_receipt)) {
                $status = false;
                $msg = "Exceed amount Medical Claim";
                $url = null; 
            }
            else {
            $claim->$month = $claim->$month - $request->amount_receipt;
            $claim->save();

            $medical = new MedicalClaim();

            $medical->form_id = strval($index);
            $medical->staff_id = $request->staff_id;
            $medical->month_apply = $request->month;
            $medical->amount = $request->amount_receipt;
            $medical->clinic_name = $request->clinic_name;
            $medical->reason = $request->reason;
            $medical->date_receipt = $request->date;

            
            
            $saved = $medical->save();

            if($saved) {
                $status = true;
                $msg = 'Success! Medical request created.';
                $url = route('form');
            }

            }
            
            
        }
        catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }

        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function add_in_liue(){
        $id = Auth::id();
        $staff = User::find($id);
        // $excludeJob = ['KMM2013000', 'KMM2014000', 'KMM2015000', 'KMM2016000', 'KMM2017000','SBH2013000', 'SBH2014000', 'SBH2015000', 'SBH2016000', 'SBH2017000','MLK2013000', 'MLK2014000', 'MLK2015000', 'MLK2016000', 'MLK2017000'];
        // $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->whereNotIn('job_number', $excludeJob)->get()->pluck('job_number', 'job_number');
        $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->get()->pluck('job_number', 'job_number');
        $index = intval(json_encode(InLiue::orderBy('id','desc')->pluck('id')->first())) + 1; 

        return view('form::in_liue.add_form',compact('staff','orders','index'));
    }

    public function post_in_liue(Request $request) {


        try {
            $index = intval(json_encode(InLiue::orderBy('id','desc')->pluck('id')->first())) + 1; 

            $inliue = new InLiue();

            $inliue->form_id = strval($index);
            $inliue->staff_id = $request->staff_id;
            $inliue->job_number = $request->job_number;
            $inliue->date_apply = $request->date_apply;

            $saved = $inliue->save();

            for($i = 0; $i<count($request->off_day_desc); $i++ ){
                $work = new Work();

                $work->form_id = $inliue->form_id;
                $work->off_day_desc = $request->off_day_desc[$i];
                $work->date_work = $request->date_work[$i];
                $work->start_hour = $request->start_hour[$i];
                $work->end_hour = $request->end_hour[$i];
                $work->total_hour = $request->total_hour[$i];
                $work->job_description = $request->job_description[$i];
                $work->save();
            }

            if($saved) {
                $status = true;
                $msg = 'Success! In Liue created.';
                $url = route('form');
            }
        }
        catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }
        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function add_outstation_claim(){
        $id = Auth::id();
        $staff = User::find($id);
        // $excludeJob = ['KMM2013000', 'KMM2014000', 'KMM2015000', 'KMM2016000', 'KMM2017000','SBH2013000', 'SBH2014000', 'SBH2015000', 'SBH2016000', 'SBH2017000','MLK2013000', 'MLK2014000', 'MLK2015000', 'MLK2016000', 'MLK2017000'];
        // $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->whereNotIn('job_number', $excludeJob)->get()->pluck('job_number', 'job_number');
        $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->get()->pluck('job_number', 'job_number');
        $index = intval(json_encode(OutstationClaim::orderBy('id','desc')->pluck('id')->first())) + 1; 

        return view('form::outstation_claim.add_form',compact('staff','orders','index'));
    }

    public function post_outstation_claim(Request $request){
        // dd($request->all());

        // $order = PurchaseOrder::find($request->job_number);
        // $job = $order->job_number;

        try {
            $index = intval(json_encode(OutstationClaim::orderBy('id','desc')->pluck('id')->first())) + 1; 

            $claim = new OutstationClaim();
            
            $claim->form_id = strval($index);
            $claim->staff_id = $request->staff_id;
            $claim->job_number = $request->job_number;
            $claim->description = $request->description;
            $claim->location = $request->location;
            $claim->staff_type = $request->gridRadios;
            $claim->accom_type = $request->grid;
            $claim->allowance_rate = $request->rate;
            $claim->travel_date_mob = $request->mob;
            $claim->travel_date_demob = $request->demob;
            $claim->outstation_date_start = $request->start;
            $claim->outstation_date_end = $request->end;
            $claim->total_allowance_day = $request->total_allowance;
            $claim->total_accommodation_day = $request->total_accom == null ? 0 : $request->total_accom;
            $claim->total = $request->total_amount;

            $saved = $claim->save();

            if($saved) {
                $status = true;
                $msg = 'Success! Claim created.';
                $url = route('form');
            }
        }
        catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }

        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);

    }

    public function add_leave_application(){
        $id = Auth::id();
        $staff = User::find($id);
        $user = User::get()->pluck('staff_id', 'id');
        $index = intval(json_encode(LeaveApplication::orderBy('id','desc')->pluck('id')->first())) + 1; 

        return view('form::leave_application.add_form',compact('staff','user','index'));
    }

    public function post_leave_application(Request $request){

        $user_handover = User::find($request->handover);

        try {
            $index = intval(json_encode(LeaveApplication::orderBy('id','desc')->pluck('id')->first())) + 1;

            $leave = new LeaveApplication();

            $leave->form_id = strval($index);
            $leave->staff_id = $request->staff_id;
            $leave->date_apply = $request->apply;
            $leave->start_date = $request->start;
            $leave->end_date = $request->end;
            $leave->total_day = $request->total_day;
            $leave->type_leave = $request->type_leave;
            $leave->reason = $request->reason;
            $leave->staff_handover = $user_handover->staff_id;
            $leave->work_scope = $request->work_scope;
            $leave->status = 0;

            $saved = $leave->save();

            if($saved) {
                $status = true;
                $msg = 'Success! Leave created.';
                $url = route('form');
            }

        }
        catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }

        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);

    }

    public function add_site_allowance(){
        $id = Auth::id();
        $staff = User::find($id);
        // $excludeJob = ['KMM2013000', 'KMM2014000', 'KMM2015000', 'KMM2016000', 'KMM2017000','SBH2013000', 'SBH2014000', 'SBH2015000', 'SBH2016000', 'SBH2017000','MLK2013000', 'MLK2014000', 'MLK2015000', 'MLK2016000', 'MLK2017000'];
        // $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->whereNotIn('job_number', $excludeJob)->get()->pluck('job_number', 'job_number');
        $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->get()->pluck('job_number', 'job_number');
        $index = intval(json_encode(SiteAllowance::orderBy('id','desc')->pluck('id')->first())) + 1; 

        return view('form::site_allowance.add_form',compact('staff','orders','index'));
    }

    public function post_site_allowance(Request $request){

        try {
            $index = intval(json_encode(SiteAllowance::orderBy('id','desc')->pluck('id')->first())) + 1;

            $site = new SiteAllowance();

            $site->form_id = strval($index);
            $site->staff_id = $request->staff_id;
            $site->job_number = $request->job_number;
            $site->date_apply = $request->apply;
            $site->total_amount = $request->total;

            $saved = $site->save();

            for($i = 0; $i<count($request->site_work); $i++){
                $work = new SiteWork();
                $work->form_id = $site->form_id;
                $work->date_work = $request->date[$i];
                $work->description = $request->site_work[$i];
                $work->amount = $request->amount[$i];
                $work->save();
            }

            if($saved) {
                $status = true;
                $msg = 'Success! Site allowance created.';
                $url = route('form');
            }
        }
        catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }

        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
       
    }

    public function add_mileage_claim(){
        $id = Auth::id();
        $staff = User::find($id);
        // $excludeJob = ['KMM2013000', 'KMM2014000', 'KMM2015000', 'KMM2016000', 'KMM2017000','SBH2013000', 'SBH2014000', 'SBH2015000', 'SBH2016000', 'SBH2017000','MLK2013000', 'MLK2014000', 'MLK2015000', 'MLK2016000', 'MLK2017000'];
        // $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->whereNotIn('job_number', $excludeJob)->get()->pluck('job_number', 'job_number');
        $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->get()->pluck('job_number', 'job_number');
        $index = intval(json_encode(MileageClaim::orderBy('id','desc')->pluck('id')->first())) + 1; 

        return view('form::mileage_claim.add_form',compact('staff','orders','index'));
    }

    public function post_mileage_claim(Request $request){

        // dd($request->all());
        try {
            $index = intval(json_encode(MileageClaim::orderBy('id','desc')->pluck('id')->first())) + 1;

            for($i = 0; $i<count($request->job_number); $i++){

                $mileage = new MileageClaim();
                $mileage->form_id = strval($index);
                $mileage->staff_id = $request->staff_id;
                $mileage->job_number = $request->job_number[$i];
                $mileage->car_availability = $request->availability;
                $mileage->date_travel = $request->date[$i];
                $mileage->from = $request->from[$i];
                $mileage->to = $request->to[$i];
                $mileage->description = $request->description[$i];
                $mileage->distant = $request->distance[$i];
                $mileage->fuel_price = $request->fuel_price[$i];
                $mileage->amount = $request->amount[$i];
                $saved = $mileage->save();

            }

            if($saved) {
                $status = true;
                $msg = 'Success! Mileage created.';
                $url = route('form');
            }
        }
        catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }

        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function getTitle($id) {

        $title = PurchaseOrder::where('job_number', $id)->first();
        return response()->json($title);
    }

    public function timesheet() {

        $id = Auth::id();
        $staff = User::find($id);
        // $excludeJob = ['KMM2013000', 'KMM2014000', 'KMM2015000', 'KMM2016000', 'KMM2017000','SBH2013000', 'SBH2014000', 'SBH2015000', 'SBH2016000', 'SBH2017000','MLK2013000', 'MLK2014000', 'MLK2015000', 'MLK2016000', 'MLK2017000'];
        // $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->whereNotIn('job_number', $excludeJob)->get()->pluck('job_number', 'job_number');
        $orders = PurchaseOrder::where('job_number','like', '%' . Auth::user()->region . '%')->where('po_status',1)->get()->pluck('job_number', 'job_number');
        $user = User::get()->pluck('staff_id', 'id');

        $index = intval(json_encode(PurchaseOrder::orderBy('id','desc')->pluck('id')->first())) + 1; 

        return view('form::timesheet.index',compact('staff','orders','user','index'));
    }


}
