<?php

namespace Modules\Report\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Form\Entities\PurchaseRequest;
use Modules\Form\Entities\MedicalClaim;
use Modules\Form\Entities\PurchaseOrder;
use Illuminate\Routing\Controller;
use DataTables;
use App\User;
use DB;
use Auth;



class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(){
        if(Auth::user()->region == 'KMM'){
            $region = [
                [
                    'code' => 'KMM',
                    'region' => 'Kemaman'
                ],
                [
                    'code' => 'SBH',
                    'region' => 'Sabah'
                ],
                [
                    'code' => 'MLK',
                    'region' => 'Melaka'
                ],
            ];
        }else if(Auth::user()->region == 'SBH'){
            $region = [
                [
                    'code' => 'SBH',
                    'region' => 'Sabah'
                ],
            ];
        }
        else if(Auth::user()->region == 'MLK'){
            $region = [
                [
                    'code' => 'MLK',
                    'region' => 'Melaka'
                ],
            ];
        }
        return view('report::index',compact('region'));
    }

    public function detail_financial($id = null) {

        $orders = PurchaseOrder::where('job_number','like', '%' . $id . '%')->get()->pluck('job_number', 'job_number')->prepend('Select Job Number');

        if(request()->ajax()) {
            $data = PurchaseRequest::all();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->make(true);
         }

        return view('report::DetailFinancial.index',compact('orders'));
    }

    public function request_date($id = null, Request $request) {

        $user = User::where('region', $id)->get()->pluck('staff_id', 'staff_id')->prepend('Select Staff');

        if(request()->ajax()) {
            $url = $request->segments();
            $data = PurchaseRequest::where('job_number','like', '%' . end($url) . '%')->get();
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->make(true);
        }
        return view('report::PurchaseRequestDate.index',compact('user'));
    }

    public function medical_claim($id = null, Request $request) {

        $user = User::where('region', $id)->get()->pluck('staff_id', 'staff_id')->prepend('Select Staff');

        $arrUser = [];
        $allUser = User::where('region', $id)->get();

        foreach($allUser as $item ) {
            array_push( $arrUser, $item->staff_id);
        }

        if(request()->ajax()) {

            $data = MedicalClaim::whereIn('staff_id', $arrUser)->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->make(true);
        }

        return view('report::MedicalClaim.index',compact('user'));
    }



    public function order_type($id,Request $request) {
        return view('report::purchase_order_type.index');
    }

    public function service($id,Request $request){
        $url = $request->segments();
        $data = PurchaseOrder::where('type_po','=','1')->where('job_number','like', '%' . end($url) . '%')->get();
        $po = PurchaseOrder::whereIn('type_po',['1','2','3'])->where('job_number','like', '%' . end($url) . '%')->get();

        $total_po = 0;
        $total_cost = 0;
        foreach($po as $item){
            $total_po = $total_po + floatval(str_replace(",", "", $item->po_value));
            $total_cost = $total_cost + floatval(str_replace(",", "", $item->total_outgoing));
        }

        return Datatables::of($data)->addIndexColumn()
                                    ->addColumn('total_po',$total_po)
                                    ->addColumn('total_cost',$total_cost)
                                    ->make(true);
    }

    public function trading($id,Request $request){
        $url = $request->segments();
        $data = PurchaseOrder::where('type_po','=','2')->where('job_number','like', '%' . end($url) . '%')->get();
        $po = PurchaseOrder::whereIn('type_po',['1','2','3'])->where('job_number','like', '%' . end($url) . '%')->get();

        $total_po = 0;
        $total_cost = 0;
        foreach($po as $item){
            $total_po = $total_po + floatval(str_replace(",", "", $item->po_value));
            $total_cost = $total_cost + floatval(str_replace(",", "", $item->total_outgoing));
        }

        return Datatables::of($data)->addIndexColumn()
                                    ->addColumn('total_po',$total_po)
                                    ->addColumn('total_cost',$total_cost)
                                    ->make(true);
    }

    public function rental($id,Request $request){
        $url = $request->segments();
        $data = PurchaseOrder::where('type_po','=','3')->where('job_number','like', '%' . end($url) . '%')->get();
        $po = PurchaseOrder::whereIn('type_po',['1','2','3'])->where('job_number','like', '%' . end($url) . '%')->get();

        $total_po = 0;
        $total_cost = 0;
        foreach($po as $item){
            $total_po = $total_po + floatval(str_replace(",", "", $item->po_value));
            $total_cost = $total_cost + floatval(str_replace(",", "", $item->total_outgoing));
        }

        return Datatables::of($data)->addIndexColumn()
                                    ->addColumn('total_po',$total_po)
                                    ->addColumn('total_cost',$total_cost)
                                    ->make(true);
    }

    public function overall($id,Request $request){
        $url = $request->segments();
        $data = PurchaseOrder::whereIn('type_po',['1','2','3'])->where('job_number','like', '%' . end($url) . '%')->get();

        return Datatables::of($data)->addIndexColumn()->make(true);
    }
    
    public function view_overall_purchase_by_date($id = null, Request $request){

        $user = User::where('region', $id)->get()->pluck('staff_id', 'staff_id')->prepend('Select Staff');

        if(request()->ajax()) {
            $url = $request->segments();
            $data = PurchaseOrder::select('staff_id','job_number','client_name', 'po_date',DB::raw('CONCAT(MONTHNAME(po_date)," ",YEAR(po_date)) as dates'), 'po_value','client_ref_no', 'job_title','type_po', 'total_outgoing')
                    ->where('job_number','like', '%' . end($url) . '%')
                    ->groupBy('job_number','dates')
                    ->get();

            return Datatables::of($data)
                    ->addColumn('expected_profit', function($row){
                        return floatval(str_replace(",","",$row->po_value)) - floatval(str_replace(",","",$row->total_outgoing));
                    })
                    ->make(true);
        }
        return view('report::purchase_order.by_date', compact('user'));
    }

     // view outstanding payment by date rage
     public function view_outstanding_payment_by_date($id = null){

        return view('report::payment.outstanding_payment');

    }

       // view outstanding payment by date rage
    public function get_incoming_payment_by_date($id = null, Request $request){

        if(request()->ajax()) {
            $url = $request->segments();
            $data = DB::table('incoming_payment')->join('purchase_order as po', 'incoming_payment.form_id', '=', 'po.form_id')
                    ->where('po.job_number', 'like', '%' . end($url) . '%')
                    ->where('incoming_payment.status',0)
                    ->select('incoming_payment.*','po.*')
                    ->get();

            return Datatables::of($data)
                    ->make(true);
        }

        return view('report::payment.outstanding_payment');
    }

    public function get_outgoing_payment_by_date($id = null, Request $request){

        if(request()->ajax()) {
            $url = $request->segments();
            $data = DB::table('outgoing_payment')->join('purchase_order as po', 'outgoing_payment.form_id', '=', 'po.form_id')
                                                ->where('po.job_number', 'like', '%' . end($url) . '%')
                                                ->where('outgoing_payment.status',0)
                                                ->select('outgoing_payment.*','po.*')
                                                ->get();

            return Datatables::of($data)
                    ->make(true);
        }

        return view('report::payment.outstanding_payment');
    }

    public function getjob($id) {       
        $details = PurchaseOrder::with('incoming','outgoing')->where('job_number',$id)->first();

        return response()->json($details);
    }

    
    public function leave_application_report($id){
        if(request()->ajax()){
            $data = DB::table('leave_application')
                    ->join('profile', 'profile.staff_id', '=', 'leave_application.staff_id')
                    ->where('region','=',$id)
                    ->get();

            $data = $data->where('status',1);

            return Datatables::of($data)
                ->addColumn('balance_day',function($row){
                    $total = intval($row->annual_leave) + intval($row->medical_leave) + intval($row->liue_leave);
                    return $total;
                })
                ->addColumn('action', function($row){
                    $list = 'data-id="'.$row->id.'"
                            data-staff_name="'.$row->staff_id.'"
                            data-date_apply="'.$row->date_apply.'"
                            data-start_date="'.$row->start_date.'"
                            data-end_date="'.$row->end_date.'"
                            data-total_day="'.$row->total_day.'"
                            data-type_leave="'.$row->type_leave.'"
                            data-reason="'.$row->reason.'"
                            data-approved_date="'.$row->approved_date.'"
                            data-update_date="'.$row->update_date.'"
                            data-status="'.$row->status.'"
                            data-remarks="'.$row->remarks.'"';
                    $btn = '<a href="javascript:void(0)" '.$list.' data-toggle="modal" data-target="#leaveModal" class="btn btn-primary btn-sm btn-icon mr-2 openModal"><i class="fas fa-pencil-alt"></i></a>';
                    return $btn;
                })->rawColumns(['action'])->make(true);
        }
        return view('report::leave_application.index');
    }

    public function site_allowance_report($id,Request $request){
        $user = User::where('region','=',$id)->get()->pluck('staff_id', 'staff_id')->prepend('Select Staff');
        if(request()->ajax()){
            $url = $request->segments();
            $data = DB::table('site_allowance')
                        ->join('site_work', 'site_work.form_id', '=', 'site_allowance.form_id')
                        ->where('site_allowance.job_number', 'like', '%' . end($url) . '%')
                        ->get();

            return Datatables::of($data)->make(true);
        }
        return view('report::site_allowance.index',compact('user'));
    }

    public function in_liue_report($id,Request $request){
        if(request()->ajax()){
            $url = $request->segments();
            $data = DB::table('off_in_liue')
                    ->join('work', 'work.form_id', '=', 'off_in_liue.form_id')
                    ->where('off_in_liue.job_number', 'like', '%' . end($url) . '%')
                    ->get();

            $data = $data->where('status',1);

            return Datatables::of($data)->make(true);
        }
        return view('report::in_liue.index');
    }
}
