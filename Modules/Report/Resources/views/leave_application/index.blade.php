@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">LEAVE SUMMARY</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="form-group row">
                            <div class="form-group col-md-3" style="font-size: 15pt;color:black; font-weight:bold;">
                                <div class="col-md-12" style="margin-bottom:30px">
                                    <label style="font-weight:bold">1) Start Date</label>
                                    <input type="text" class="form-control" autocomplete="off" name="start" id="min">
                                </div>
                                <div class="col-md-12" style="margin-bottom:30px">
                                    <label style="font-weight:bold">2) End Date</label>
                                    <input type="text" class="form-control" autocomplete="off" name="end" id="max">
                                </div>
                            </div>

                            <div class="form-group col-md-7">
                                <div class="table-responsive">
                                    <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                        <thead class="thead-theme">
                                            <tr>
                                                <th>STAFF NAME</th>
                                                <th>TOTAL APPROVED LEAVE</th>
                                                <th>TOTAL BALANCE LEAVE</th>
                                                <th>DATE APPLY</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group col-md-2">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <a href="{{ route('report') }}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="leaveModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editing Selected Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <div class="form-group row">
                <label class="col-md-2">STAFF NAME</label>
                <div class="col-md-3">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="staff_name" name="staff_name" readonly>
                </div>
                <label for="" class="col-md-2">DATE APPLY</label>
                <div class="col-md-5">
                    <input class="form-control input-group-lg" autocomplete="off" type="date" id="date_apply" name="date_apply" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2">START DATE</label>
                <div class="col-md-3">
                    <input class="form-control input-group-lg" autocomplete="off" type="date" id="start_date" name="start_date" readonly>
                </div>
                <label class="col-md-2">TOTAL DAYS</label>
                <label class="col-md-5">TYPE OF LEAVE (ANNUAL/EMERGENCY/SICK/UNPAID/OTHERS)</label>
            </div>

            <div class="form-group row">
                <label class="col-md-2">END DATE</label>
                <div class="col-md-3">
                    <input class="form-control input-group-lg" autocomplete="off" type="date" id="end_date" name="end_date" readonly>
                </div>
                <div class="col-md-2">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="total_day" name="total_day" readonly>
                </div>
                <div class="col-md-5">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="type_leave" name="type_leave" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2">REASON</label>
                <div class="col-md-10">
                    <textarea class="form-control" autocomplete="off" rows="5" id="reason" name="reason" readonly></textarea>
                </div>
            </div>

            <div class="col-md-12 form-group row"></div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label class="col-md-12">APPROVED DATE</label>
                    <div class="col-md-12">
                        <input class="form-control input-group-lg" autocomplete="off" type="date" id="approved_date" name="approved_date" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="col-md-12">UPDATE DATE</label>
                    <div class="col-md-12">
                        <input class="form-control input-group-lg" autocomplete="off" type="date" id="update_date" name="update_date" readonly>
                    </div>
                </div>
                <div class="col-md-1">
                    <label class="col-md-12">STATUS</label>
                    <div class="col-md-12">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="status" name="status" value="1" disabled>
                            <label class="form-check-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <label class="col-md-12">REMARKS</label>
                    <div class="col-md-12">
                        <input class="form-control input-group-lg" autocomplete="off" type="text" id="remarks" name="remarks" readonly>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>

<script>
     $(() => {

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = $("#min").datepicker("getDate")

                var max = $("#max").datepicker("getDate")
                
                var date = data[5].split("/");
                var date1 =`${date[2]}-${date[1]}-${date[0]}`;
                var startDate = new Date(date1);
                console.log(startDate);
                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }
                return false;
            }
        );

        $("#min").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            orientation: "bottom right" ,
            onClose: function (date, inst) {
                //set the other dateTo the min date
                var selectedDate = $("#min").datepicker("getDate");
                var followingDate = new Date(selectedDate.getTime() + 86400000);
                followingDate.toLocaleDateString();
                $("#max").datepicker("option", "minDate", followingDate);
            }
        });

        $("#max").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });


        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        let query = window.location.href;
        var hash = query.split('/');
        var lastSegment = hash.pop() || hash.pop();
        let url = "{{ route('leave.report', ':id') }}";
        let urlx = url.replace(':id', lastSegment);

        let table = $('#table').DataTable({
            processing: true,
            serverSide: false,
            responsive: true,
            filter:true,
            "dom" : '<"wrapper"fltip>',
            ajax: {
                url: urlx,
                type: 'GET',
            },
            columns: [
                { data: 'staff_id'},
                { data: 'total_day'},
                { data: 'balance_day'},
                {
                    data: 'date_apply',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                { data: 'action'},
                { data: 'date_apply'},
            ],
            columnDefs: [
                {className: "text-center", "targets":[5]},
                {visible:false,targets:[5]}
            ]
        });  

        $('#min, #max').change(function () {
            table.draw();
        });

    });

    $(document).on('click', '.openModal', function(e) {
        var link = $(this);
        var id  = link.data("id");
        var staff_name = link.data("staff_name");
        var date_apply = link.data("date_apply");
        var start_date = link.data("start_date");
        var end_date = link.data("end_date");
        var total_day = link.data("total_day");
        var type_leave = link.data("type_leave");
        var reason = link.data("reason");
        var approved_date = link.data("approved_date");
        var update_date = link.data("update_date");
        var status = link.data("status");
        var remarks = link.data("remarks");

        var url = '{{route("save.leave_application", ":id")}}';
        var route = url.replace(":id",id);
        $('#leaveReq').attr('action', route);

        $("#staff_name").val(staff_name);
        $("#date_apply").val(date_apply);
        $("#start_date").val(start_date);
        $("#end_date").val(end_date);
        $("#total_day").val(total_day);
        $("#reason").text(reason);
        $("#approved_date").val(approved_date);
        $("#update_date").val(update_date);
        $("#remarks").val(remarks);

        if(type_leave == 0){
            $("#type_leave").val('Annual Leave');   
        }else if(type_leave == 1){
            $("#type_leave").val('In Liue Leave');   
        }

        if(status == 1){
            $("#status").prop('checked',true);
        }else{
            $("#status").vprop('checked',true);
        }
    });
</script>
@endpush