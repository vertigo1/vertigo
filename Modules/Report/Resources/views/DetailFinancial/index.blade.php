@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .dt-buttons .dt-button {
        width:100%;
        background: #745af2;
        border: 1px solid #745af2;
        cursor: pointer;
        text-align: center;
    }

    .dt-buttons {
        text-align: right;
    }
</style>
@endpush

@section('content')           
<br>
<div class="row" id="printSection">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">DETAIL FINANCIAL BY JOB NUMBER</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold">
                            <div class="form-group col-md-6" style="text-align:left" id="printing">
                                <label for="homepage"></label>
                                <a href="{{ route('report') }}" class="col-md-3 btn btn-outline-danger">Homepage</a>
                            </div>
                            <!-- <div class="form-group col-md-6" style="text-align:right">
                                <label for="print"></label>
                                <a href="javascript:;" class="col-md-3 btn btn-primary print">
                                    <span class="icon-printer"></span> Print
                                </a>
                            </div> -->
                        </div>

                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold;justify-content:center;margin-top:2%">
                            <div class="form-group col-md-3 block">
                            {{-- {{ Form::select('staff_id', $user, null,['class' => 'form-control select2', 'id' => 'staff-id']) }s} --}}
                            {{-- {{ Form::select('staff_id', $user, null,['class' => 'form-control select2', 'id' => 'staff-id']) }} --}}
                            {{ Form::select('job_number', $orders, null,['class' => 'form-control select2', 'id' => 'job-number']) }}
                            </div>
                            <div class="form-group col-md-2" style="text-align: center">
                                <label> CLIENT NAME</label>
                            </div>
                            <div class="form-group col-md-7">
                                <input type="text" class="form-control" autocomplete="off" id="client-name" readonly>
                            </div>
                        </div>
                        <br>
                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold">
                            <div class="form-group col-md-1" style="text-align: center">
                                <label> PO DATE</label>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="text" class="form-control" autocomplete="off" id="po-date" readonly>
                            </div>
                            <div class="form-group col-md-2" style="text-align: center">
                                <label> CLIENT REF NO</label>
                            </div>
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" autocomplete="off" id="client-ref-no" readonly>
                            </div>
                            <div class="form-group col-md-2" style="text-align: center">
                                <label> PIC</label>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="text" class="form-control" autocomplete="off" id="pic" readonly>
                            </div>
                        </div>
                        <br>
                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold">
                            <div class="form-group col-md-1" style="text-align: center">
                                <label> PO TYPE</label>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="text" class="form-control" autocomplete="off" id="po-type" readonly>
                            </div>
                            <div class="form-group col-md-2" style="text-align: center">
                                <label> JOB TITLE</label>
                            </div>
                            <div class="form-group col-md-7">
                                <textarea type="text" class="form-control" autocomplete="off" rows="3" id="job-title" readonly></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold">
                            <div class="form-group col-md-1" style="text-align: center">
                                <label> PO VALUE</label>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="text" class="form-control" autocomplete="off" id="po-value" readonly>
                            </div>
                            
                        </div>
                        <div class="col-md-2"></div>
                        <br><br>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <h2>INCOMING CLIENT PAYMENT</h2>
                                <div class="table-responsive">
                                    <table id="table-client" class="table table-bordered" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th style="vertical-align:middle;text-align:center;width:20%"></th>
                                                <th style="vertical-align:middle;text-align:center;width:40%">EXPECTED DATE</th>
                                                <th style="vertical-align:middle;text-align:center;width:40%">AMOUNT (RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <h2>OUTGOING SUBCONT PAYMENT</h2>
                                <div class="table-responsive">
                                    <table id="table-subcont" class="table table-bordered" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th style="vertical-align:middle;text-align:center;width:5%"></th>
                                                <th style="vertical-align:middle;text-align:center;width:30%">SUBCONT NAME</th>
                                                <th style="vertical-align:middle;text-align:center;width:45%">DESCRIPTION</th>
                                                <th style="vertical-align:middle;text-align:center;width:10%">EXPECTED DATE</th>
                                                <th style="vertical-align:middle;text-align:center;width:10%">AMOUNT (RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4"></div>
                            <label for="" class="col-md-2">TOTAL</label>
                            <div class="col-md-4">
                                <input class="form-control input-group-lg" autocomplete="off" type="text" id="total" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <fieldset style="border: 1px black solid">

                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>PLAN COSTING PROFIT</b></legend>
                                    <h3 id="sum_expected1" style="color:steelblue;font-weight:bold;text-align:center"></h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                            <div class="col-md-3">
                                <fieldset style="border: 1px black solid">

                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>ACTUAL CURRENT PROFIT</b></legend>
                                    <h3 id="sum_expected2" style="color:steelblue;font-weight:bold;text-align:center"></h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                            <div class="col-md-3"></div>
                            
                        </div>

                        <br><br>
                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>STAFF ID</th>
                                        <th>DATE</th>
                                        <th>SUPPLIER</th>
                                        <th>PAID AMOUNT</th>
                                        <th>DESCRIPTION</th>
                                        <th>PAID</th>
                                        <th>UNPAID</th>
                                        <th>JOB NUMBER</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" crossorigin="anonymous"></script>

<script>

     $(document).ready(function(){

        const formatter = new Intl.NumberFormat('ms-MY', {
            style: 'currency',
            currency: 'MYR',
            minimumFractionDigits: 2
        });

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };
       
        $("#job-number").on('change', function() {

            let val = $("#job-number option:selected").text();
            
            var url = '{{route("getjob",":id")}}';
            $("#total").prop("value",'');
            $("#sum_expected1").html("");
            $('#table').DataTable().destroy();
            let urlx = url.replace(":id",val);

            $.ajax({
                type: 'GET',
                url:  urlx,
                success: (resp) => {
                    let type;
                    if(resp['type_po'] ==1) type = 'SERVICES';
                    if(resp['type_po'] ==2) type = 'TRADING';
                    if(resp['type_po'] ==3) type = 'RENTAL';
                    if(resp['type_po'] ==4) type = 'OPEX';

                    let expected1 = 0;
                    let expected2 = 0;

                    if(val == "Select Job Number") {
                        expected1 = expected1;
                    }
                    else {
                        var po_value = resp['po_value'].replace(/[^0-9\.]/g,'');
                        var total_outgoing = resp['total_outgoing'].replace(/[^0-9\.]/g,'');
                        expected1 = parseFloat(po_value) - parseFloat(total_outgoing);
                    }
                    var job = $.parseHTML(resp['job_title']);
                    
                    $("#job-title").prop("value", $(job).text());
                    $("#po-date").prop("value", resp['po_date']);
                    $("#po-value").prop("value", resp['po_value']);
                    $("#client-name").prop("value", resp['client_name']);
                    $("#client-ref-no").prop("value", resp['client_ref_no']);
                    $("#pic").prop("value", resp['staff_id']);
                    $("#po-type").prop("value", type );
                    $("#total").prop("value", resp['total_outgoing']);
                    $("#sum_expected1").html(formatter.format(expected1));

                    $("#table-subcont tbody").html('');
                    $("#table-client tbody").html('');
                   
                    resp['incoming'].forEach( (element, index) => {
                        
                       let b = `
                                <tr>
                                    <td class="stage">STAGE ${index+1} </td>
                                    <td><input name="expected_date[]" class="form-control input-group-lg date" autocomplete="off" type="text"  value="${element['expected_date']}" readonly></td>
                                    <td><input name="amount[]" class="form-control input-group-lg amount" autocomplete="off" type="text"  value="${element['amount']}" readonly></td>
                                </tr>
                                `;
                        $("#table-client tbody").append(b);
                    });

                    resp['outgoing'].forEach( (element, index) => {
                        let a = `
                                <tr>
                                    <td class="num">${index+1}</td>
                                    <td><input class="form-control input-group-lg subcont_name" autocomplete="off" type="text"  value="${element['subcont_name']}" readonly></td>
                                    <td><input class="form-control input-group-lg description" autocomplete="off" type="text"  value="${element['description']}" readonly></td>
                                    <td><input class="form-control input-group-lg expected_date" autocomplete="off" type="text"  value="${element['expected_date']}" readonly></td>
                                    <td><input class="form-control input-group-lg amount_subcont" autocomplete="off" type="text"  value="${element['amount']}" readonly></td>
                                </tr>
                               `;
                        $("#table-subcont tbody").append(a);
                    });


                },
                error: (e) => {
                    console.log(e);
                }
            });
            document.getElementById("table").deleteTFoot();
            $("#table").append("<tfoot><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tfoot>");

            var urla = '{{route("detail_financial")}}';
            let table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                responsive: true,
                "dom" : '<"wrapper"Bfltip>',
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
        
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    // Total over all pages
                    // totalpaid = api
                    //     .column( 3 )
                    //     .data()
                    //     .reduce( function (a, b) {
                    //         return intVal(a) + intVal(b);
                    //     }, 0 );
                    totalpaid = 0;
                    var data = table.cells('.paid').render('testing');
                    if(data.length > 0) {
                        totalpaid = data.reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }), 0;

                    }
        
                    // Total over this page
                    totalunpaid = 0;
                    var data = table.cells('.unpaid').render('testing');
                    if(data.length > 0) {
                        totalunpaid = data.reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }), 0;

                    }
                    
                    // Update footer
                    $( api.column( 3 ).footer() ).html(
                        `<span class="text-success" id='paid'> <strong> Total Paid : ${formatter.format(totalpaid)} </strong> </span> `
                    );

                    $( api.column( 6 ).footer() ).html(
                        `<span class="text-danger" id='unpaid'> <strong> Total Unpaid : ${formatter.format(totalunpaid)} </strong> </span> `
                    );

                    let actual = $("#po-value").val();
                    var new_actual = actual.replace(/[^0-9\.]/g,'');
                    $("#sum_expected2").html(formatter.format(new_actual - (totalpaid + totalunpaid)));
                },
                buttons: [
                {
                    extend: 'print', text: '<i class="icon-printer"></i> Print', className: 'btn-primary',
                    messageTop: 'The information in this table is copyright to Vertigo Tech Sdn Bhd.',
                    exportOptions: {
                            columns: [ 0, 1, 2,3, 4,5,6]
                    },
                    customize: function ( win ) {
                        var job_number = $("#job-number option:selected").text();
                        var start = $("#min").val();
                        var end = $("#max").val();
                        start = (start) ? start : 'All Time'
                        end = (end) ? end : 'All Time'
                        
                        $(win.document.body).find('h1').css('text-align', 'center').html(`Detail Financial Report`);

                        var po_date = $('input#po-date').val()
                        var po_type = $('input#po-type').val()
                        var po_value = $('input#po-value').val()
                        var client_name = $('input#client-name').val()
                        var client_ref_no = $('input#client-ref-no').val()
                        var pic = $('input#pic').val()
                        var job_title = $('textarea#job-title').val()
                        var plan_profit = $('h3#sum_expected1').html()
                        var actual_profit = $('h3#sum_expected2').html()

                        var count_client = $('#table-client tbody tr').length;
                        var count_subcont = $('#table-subcont tbody tr').length;

                        var quantity1 = [], quantity2 = [], quantity3 = [];
                        var col1 = [], col2 = [], col3 = [], col4 = [], col5=[];
                        var tr_client = '', tr_subcont = '';

                        $("#table-client tr").each(function() {
                            quantity1.push($(this).find(".stage").text());
                            quantity2.push($(this).find("input.date").val());
                            quantity3.push($(this).find("input.amount").val());
                        });

                        $("#table-subcont tr").each(function() {
                            col1.push($(this).find(".num").text());
                            col2.push($(this).find("input.subcont_name").val());
                            col3.push($(this).find("input.description").val());
                            col4.push($(this).find("input.expected_date").val());
                            col5.push($(this).find("input.amount_subcont").val());
                        });

                        var total_subcont = $('#total').val()

                        for(let i=0;i<quantity1.length;i++){
                            if(i!=0){
                                tr_client += 
                                `<div class="row">
                                    <div class="col-2" style="text-align:center;">
                                        <p>${quantity1[i]}</p>
                                    </div>
                                    <div class="col-5" style="text-align:center;">
                                        <input class="form-control" value="${quantity2[i]}"></input>
                                    </div>
                                    <div class="col-5" style="text-align:center;">
                                        <input class="form-control" value="${quantity3[i]}"></input>
                                    </div>
                                </div>`
                            }
                        }

                        for(let j=0;j<col1.length;j++){
                            if(j!=0){
                                tr_subcont +=   `<div class="row">
                                                    <div class="col-1" style="text-align:center;">
                                                        <p>${col1[j]}</p>
                                                    </div>
                                                    <div class="col-3" style="text-align:center;">
                                                        <input class="form-control" value="${col2[j]}"></input>
                                                    </div>
                                                    <div class="col-4" style="text-align:center;">
                                                        <input class="form-control" value="${col3[j]}"></input>
                                                    </div>
                                                    <div class="col-2" style="text-align:center;">
                                                        <input class="form-control" value="${col4[j]}"></input>
                                                    </div>
                                                    <div class="col-2" style="text-align:center;">
                                                        <input class="form-control" value="${col5[j]}"></input>
                                                    </div>
                                                </div>`

                                
                            }
                        }


                        $(win.document.body).find('h1').after(`
                        <div class="row">
                            <div class="col-12">
                                <h5 style="text-align:center;">Job Number: ${job_number}</h5>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:14px;text-align:left;">PO DATE</label>
                                <input style="font-size:14px;text-align:left;" class="form-control" value="${po_date}"></input>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:14px;text-align:left;">PO TYPE</label>
                                <input style="font-size:14px;text-align:left;" class="form-control" value="${po_type}"></input>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:14px;text-align:left;">PO VALUE</label>
                                <input style="font-size:14px;text-align:left;" class="form-control" value="${po_value}"></input>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:14px;text-align:left;" >CLIENT NAME</label>
                                <input style="font-size:14px;text-align:left;" class="form-control" value="${client_name}"></input>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:14px;text-align:left;" >CLIENT REF NO</label>
                                <input style="font-size:14px;text-align:left;" class="form-control" value="${client_ref_no}"></input>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:14px;text-align:left;" >PIC</label>
                                <input style="font-size:14px;text-align:left;" class="form-control" value="${pic}"></input>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label style="font-size:14px;text-align:left;" >JOB TITLE</label>
                                <input style="font-size:14px;text-align:left;" class="form-control" value="${job_title}"></input>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>INCOMING CLIENT PAYMENT</h4>
                                <div class="row">
                                    <div class="col-2" style="text-align:center;">
                                        <p></p>
                                    </div>
                                    <div class="col-5" style="text-align:center;">
                                        <p>EXPECTED DATE</p>
                                    </div>
                                    <div class="col-5" style="text-align:center;">
                                        <p>AMOUNT (RM)</p>
                                    </div>
                                </div>
                                ${tr_client} 
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>OUTGOING SUBCONT PAYMENT</h4>
                                <div class="row">
                                    <div class="col-1" style="text-align:center;">
                                        <p></p>
                                    </div>
                                    <div class="col-3" style="text-align:center;">
                                        <p>SUBCONT NAME</p>
                                    </div>
                                    <div class="col-4" style="text-align:center;">
                                        <p>DESCRIPTION</p>
                                    </div>
                                    <div class="col-2" style="text-align:center;">
                                        <p>EXPECTED DATE</p>
                                    </div>
                                    <div class="col-2" style="text-align:center;">
                                        <p>AMOUNT (RM)</p>
                                    </div>
                                </div>
                                ${tr_subcont} 
                                <div class="row" style="margin-top:10px;">
                                    <div class="col-8" style="text-align:end;">
                                        <p>TOTAL</p>
                                    </div>
                                    <div class="col-4">
                                        <input class="form-control" value="${total_subcont}"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>                   
                        <div class="row" style="justify-content:center;">
                            <div class="col-md-6">
                                <fieldset style="border: 1px black solid">
                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>PLAN COSTING PROFIT</b></legend>
                                    <h3 style="color:steelblue;font-weight:bold;text-align:center">${plan_profit}</h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                            <div class="col-md-6">
                                <fieldset style="border: 1px black solid">
                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>ACTUAL CURRENT PROFIT</b></legend>
                                    <h3 style="color:steelblue;font-weight:bold;text-align:center">${actual_profit}</h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                        </div>
                        `);

                        var paid = $('table#table').find('tfoot').find('tr').find('td').eq(3).find('span#paid').html()
                        paid=paid.split('Total Paid : ')
                        var unpaid = $('table#table').find('tfoot').find('tr').find('td').eq(6).find('span#unpaid').html()
                        unpaid=unpaid.split('Total Unpaid : ')
                        $(win.document.body).find('table').after(` 
                        <br>                   
                        <div class="row" style="justify-content:center;">
                            <div class="col-md-6">
                                <fieldset style="border: 1px black solid">
                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>TOTAL PAID</b></legend>
                                    <h3 style="color:steelblue;font-weight:bold;text-align:center">${paid[1]}</h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                            <div class="col-md-6">
                                <fieldset style="border: 1px black solid">
                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>TOTAL UNPAID</b></legend>
                                    <h3 style="color:red;font-weight:bold;text-align:center">${unpaid[1]}</h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                        </div>
                        `);
                        // $(win.document.body).find('td:nth-child(1), td:nth-child(2), td:nth-child(3), td:nth-child(5), th:nth-child(1), th:nth-child(2), th:nth-child(3), th:nth-child(5)').css('text-align', 'center');
                        $(win.document.body).css( 'font-size', '14px');
                    }
                
                }],
                ajax: {
                    url: urla,
                    type: 'GET',
                },
                columns: [
                    {data: 'staff_id'},
                    {
                    data: 'date',
                    name: 'date',
                        render: function(data, type, full){
                            var date = new Date(data);
                            var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                            return htmlRender;
                        }
                    },
                    {data: 'supplier'},
                    {data: 'amount',class:'paid'},
                    {data: 'description'},
                    {data: 'payment_status', name: 'payment_status'},
                    {data: 'payment_status', name: 'payment_status', class: 'unpaid'},
                    {data: 'job_number'},
                    
                ],
                columnDefs: [
                    {
                    "targets":[3],
                    "render": function (data, type, row, meta) {
                            if (type == 'display') {
                                var val = row.amount.replace(/[^0-9\.]/g,'');
                                if(row.payment_status == 0){
                                    return '<span>' + formatter.format(0) + '<span>';
                                }else if(row.payment_status == 1){
                                    return '<span>' + formatter.format(val) + '<span>';
                                }else if(row.payment_status == 2){
                                    return '<span>' + formatter.format(0) + '<span>';
                                }
                            }
                            if(type == 'testing') {
                                var val = row.amount.replace(/[^0-9\.]/g,'');
                                return row.payment_status == 0 || row.payment_status == 2? 0 : parseInt(val);
                            }
                        }
                    },
                    {
                    "data":'payment_status',
                    "targets":[5],
                    "render": function (data, type, row, meta) {
                            if (type == 'display') {
                                if(data == 0){
                                    return '<span class="text-danger">No</span>';
                                }else if(data == 1){
                                    return '<span class="text-success">Yes</span>';
                                }else if(data == 2){
                                    return '<span class="text-danger">Reject</span>';
                                }
                            }
                            return data;
                        }
                    },
                    {
                    "data":'payment_status',
                    "targets":[6],
                    "render": function (data, type, row, meta) {
                            if (type == 'display') {
                                var val = row.amount.replace(/[^0-9\.]/g,'');
                                if(row.payment_status == 0){
                                    return '<span class="text-danger">'+ formatter.format(val) + '</span>';
                                }else if(row.payment_status == 1){
                                    return '<span class="text-success">'+ formatter.format(0) + '</span>';
                                }else if(row.payment_status == 2){
                                    return '<span class="text-danger">'+ formatter.format(0) + '</span>';
                                }
                            }
                            if(type == 'testing') {
                                var val = row.amount.replace(/[^0-9\.]/g,'');
                                return row.payment_status == 1 || row.payment_status == 2? 0 : parseInt(val);
                            }
                        }
                    },
                ]
            });

            table.buttons().container().insertAfter($('#printing'));
            $('.dt-buttons .dt-button').addClass('col-md-3');
            $('.dt-buttons').addClass('col-md-6');
            
            regex = '\\b' + val + '\\b';
            table.column(7).search(regex, true, false).draw();

                if(val == "Select Job Number") {
                    $("#job-title").prop("value", '');
                    $("#po-date").prop("value", '');
                    $("#po-value").prop("value", '');
                    $("#client-name").prop("value", '');
                    $("#client-ref-no").prop("value", '');
                    $("#pic").prop("value", '');
                    $("#po-type").prop("value", ''); 
                    $("#total").prop("value",'');
                    $("#sum_expected1").html("");
                }
            });

        })

        $('.print').on('click', function(){
            window.scrollTo(0,0);

            $('.header-logo').css('width','80%');

            var chart = document.querySelector("#printSection");
            html2canvas(chart,{
                windowWidth: window.innerWidth,
                windowHeight: window.innerHeight,
                scale: 2
            }).then(canvas => {
                var dataURL = canvas.toDataURL('image/jpeg',1.0);

                var pdf = new jsPDF();

                pdf.addImage(dataURL, 'JPEG', 5, 5, 250, 250);

                pdf.save("Detail_Financial_By_Job_Number");
            });

            $('.header-logo').css('width','50%');
        });
  
</script>

@endpush