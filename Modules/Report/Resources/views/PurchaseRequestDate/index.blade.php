@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .dt-buttons .dt-button {
        width:100%;
        background: #745af2;
        border: 1px solid #745af2;
        padding: 7px 12px;
    }

    .dt-buttons {
        display: grid;
        align-items: center;
        margin-bottom: 0px;
        cursor: pointer;
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12" id="printSection">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">PURCHASE REQUEST BY DATE & STAFF (OR ALL)</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="card-body" style="margin-top:2%">
                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold;text-align:start">
                            <div class="form-group col-md-3 block">
                                <label for="name">1) Staff Name</label>
                                {{ Form::select('staff_id', $user, null,['class' => 'form-control select2', 'id' => 'staff-id']) }}
                            </div>
                            <div class="form-group col-md-3">
                                <label> 2) Start Date</label>
                                <input type="text" class="form-control" autocomplete="off" name="min" id="min">
                            </div>
                            <div class="form-group col-md-3">
                                <label> 3) End Date</label>
                                <input type="text" class="form-control" autocomplete="off" name="max" id="max">
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-1" style="align-items:baseline;display:grid;">
                                <label for="print"></label>
                                <a href="javascript:;" class="btn btn-primary print">
                                    <span class="icon-printer"></span> Print 
                                </a>
                            </div>
                            <div class="form-group col-md-1" style="align-items:end;display:grid;">
                                <a href="{{ route('report') }}" class="btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>DATE</th>
                                        <th>STAFF NAME</th>
                                        <th>JOB NUMBER</th>
                                        <th>DESCRIPTION</th>
                                        <th>SUPPLIER</th>
                                        <th>AMOUNT</th>
                                        <th>STATUS</th>
                                        <th>PAID</th>
                                        <th>PAYMENT DATE</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" crossorigin="anonymous"></script>

<script>
    $(document).ready(function(){
        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = $("#min").datepicker("getDate")

                var max = $("#max").datepicker("getDate")
                
                var date = data[9].split("/");
                var date1 =`${date[2]}-${date[1]}-${date[0]}`;
                var startDate = new Date(date1);

                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }
                return false;
                }
        );

        $("#min").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            orientation: "bottom right" ,
            onClose: function (date, inst) {
                //set the other dateTo the min date
                var selectedDate = $("#min").datepicker("getDate");
                var followingDate = new Date(selectedDate.getTime() + 86400000);
                followingDate.toLocaleDateString();
                $("#max").datepicker("option", "minDate", followingDate);
            }
        });

        $("#max").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });

        const formatter = new Intl.NumberFormat('ms-MY', {
            style: 'currency',
            currency: 'MYR',
            minimumFractionDigits: 2
        });

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        let query = window.location.href;
        var hash = query.split('/');
        var lastSegment = hash.pop() || hash.pop();
        let url = "{{ route('request_date', ':id') }}";
        let urlx = url.replace(':id', lastSegment);

        let table = $('#table').DataTable({
            processing: true,
            serverSide: false,
            destroy: true,
            responsive: true,
            filter:true,
            "dom" : '<"wrapper"Bfltip>',
            buttons: [
                {
                    extend: 'print', text: '<i class="icon-printer"></i> Print', className: 'btn-primary',
                    messageTop: 'The information in this table is copyright to Vertigo Tech Sdn Bhd.',
                    exportOptions: {
                            columns: [ 0, 1, 2,3, 4,5,6,7,8 ]
                    },
                    customize: function ( win ) {
                        var staff = $("#staff-id option:selected").text();
                        (staff === "Select Staff") ? staff = "All Staff" : ''
                        var start = $("#min").val();
                        var end = $("#max").val();
                        start = (start) ? start : 'All Time'
                        end = (end) ? end : 'All Time'
                        
                        $(win.document.body).find('h1').css('text-align', 'center').html(`Purchase Order Report`);
                        $(win.document.body).find('h1').append(`
                        <div class="row m-5">
                            <div class="col-12 text-center">
                                    <p style="font-size: 18px;">Staff Name: <strong style="padding-right:4em">${staff}</strong>Start Date: <strong style="padding-right:4em">${start}</strong>End Date: <strong>${end}</strong></p>
                            </div>
                        </div>
                        `);

                        $(win.document.body).find('td:nth-child(1), td:nth-child(2), td:nth-child(3), td:nth-child(5), th:nth-child(1), th:nth-child(2), th:nth-child(3), th:nth-child(5)').css('text-align', 'center');
                        $(win.document.body).css( 'font-size', '14px');
                }
            }],
            ajax: {
                url: urlx,
                type: 'GET',
            },
            columns: [
                {
                    data: 'date',   
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {data: 'staff_id'},
                {data: 'job_number'},
                {data: 'description'},
                {data: 'supplier'},
                {
                    data: 'amount', 
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var amount = parseFloat(val);
                        var html = '<p>'+formatter.format(amount)+'</p>';
                        return html;
                    }
                },
                {data: 'payment_status'},
                {data: 'payment_status'},
                {
                    data: 'payment_date',
                    render: function(data, type, full){
                        if(data == '0000-00-00'){
                            var htmlRender = '<p> - </p>';
                            return htmlRender;
                        }else{
                            var payment_date = new Date(data);
                            var htmlRender = '<p>'+payment_date.toLocaleDateString('ms-MY',option)+'</p>';
                            return htmlRender;
                        }
                    }
                },
                {data: 'date'},
                
            ],
            columnDefs: [
                {
                    "data":'payment_status',
                    "targets":[6],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == 1 ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>';
                        }
                        return data;
                    }
                },
                {
                    "data": "name['amount',payment_status]",
                    "targets":[7],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            var val = row.amount.replace(/[^0-9\.]/g,'');
                            var amount = parseFloat(val);
                            return row.payment_status ==  1 ? '<span class="text-success">RM0.00<span>' : '<span class="text-danger">'+formatter.format(amount) + '</span>';
                        }
                        return data;
                    }
                },
                {   "className": "text-center", "targets":[0,1,2,5,6,7,8,9]},
                {   visible:false,targets:[9] },
                {
                    "targets": [0,1,2,3,4,5,7,8,9],
                    "orderable": false
                }
            ]
        });

        table.buttons().container().insertAfter($('#printing'));
        $('.dt-buttons .dt-button').addClass('btn');
        $('.dt-buttons').addClass('col-md-1');

        $("#staff-id").on('change',function(){
            console.log($(this).val());
         if($(this).val()!=0)
            table.column(1).search($(this).val()).draw();
         else
            table.column(1).search("").draw();
        })

        $('#min, #max').change(function () {
            table.draw();
        });
        
    })
</script>

@endpush