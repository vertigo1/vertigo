@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center;
        }
    }
    
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">SITE ALLOWANCE REPORT</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold;text-align:start">
                            <div class="form-group col-md-3 ">
                                <label for="name">1) Staff Name</label>
                                {{ Form::select('staff_id', $user, null,['class' => 'form-control select2', 'id' => 'staff-id']) }}
                            </div>
                            <div class="form-group col-md-3 ">
                                <label> 2) Start Date</label>
                                <input type="text" class="form-control" autocomplete="off" name="min" id="min">
                            </div>
                            <div class="form-group col-md-3 ">
                                <label> 3) End Date</label>
                                <input type="text" class="form-control" autocomplete="off" name="max" id="max">
                            </div>
                            <div class="col-md-2"></div>
                            <div class="form-group col-md-1" style="align-items:end;display:grid;">
                                <a href="{{ route('report') }}" class="btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <br><br><br>
                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>DATE</th>
                                        <th>STAFF NAME</th>
                                        <th>SITE WORK DESCRIPTION</th>
                                        <th>AMOUNT (RM)</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>

<script>
    $(() => {

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                
                var min = $("#min").datepicker("getDate")

                var max = $("#max").datepicker("getDate")
                
                var date = data[4].split("/");
                var date1 =`${date[2]}-${date[1]}-${date[0]}`;
                var startDate = new Date(date1);

                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }
                return false;
                }
        );

        $("#min").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            orientation: "bottom right" ,
            onClose: function (date, inst) {
                //set the other dateTo the min date
                var selectedDate = $("#min").datepicker("getDate");
                var followingDate = new Date(selectedDate.getTime() + 86400000);
                followingDate.toLocaleDateString();
                $("#max").datepicker("option", "minDate", followingDate);
            }
        });

        $("#max").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });

        const formatter = new Intl.NumberFormat('ms-MY', {
            style: 'currency',
            currency: 'MYR',
            minimumFractionDigits: 2
        });

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        let query = window.location.href;
        var hash = query.split('/');
        var lastSegment = hash.pop() || hash.pop();
        let url = "{{ route('site_allowance.report', ':id') }}";
        let urlx = url.replace(':id', lastSegment);

        let table = $('#table').DataTable({
            processing: true,
            serverSide: false,
            responsive: true,
            filter:true,
            "dom" : '<"wrapper"fltip>',
            ajax: {
                url: urlx,
                type: 'GET',
            },
            columns: [
                {
                    data: 'date_work',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                { data: 'staff_id'},
                { data: 'description'},
                {
                    data: 'amount', 
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var amount = parseFloat(val);
                        var html = '<p>'+formatter.format(amount)+'</p>';
                        return html;
                    }
                },
                { data: 'date_work'},
            ],
            columnDefs: [
                {className: "text-center", "targets":[4]},
                {visible:false,targets:[4]}
            ]
        });

        $("#staff-id").on('change', function () {
            if(this.value == 0) {
                table.column(1).search('').ajax.reload();
            }
            if (table.column(1).search() != this.value) {
                table.column(1).search(this.value).ajax.reload();
            }
        });

        $('#min, #max').change(function () {
            table.draw();
        });

    })
</script>

@endpush