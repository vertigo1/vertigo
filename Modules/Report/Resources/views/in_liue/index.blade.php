@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">IN LIUE SUMMARY</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="form-group row">
                            <div class="form-group col-md-3" style="font-size: 15pt;color:black; font-weight:bold;">
                                <div class="col-md-12" style="margin-bottom:30px">
                                    <label> 1) Start Date</label>
                                    <input type="text" class="form-control" autocomplete="off" name="min" id="min">
                                </div>
                                <div class="col-md-12" style="margin-bottom:30px">
                                    <label> 2) End Date</label>
                                    <input type="text" class="form-control" autocomplete="off" name="max" id="max">
                                </div>
                            </div>

                            <div class="form-group col-md-7">
                                <div class="table-responsive">
                                    <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                        <thead class="thead-theme">
                                            <tr>
                                                <th>STAFF NAME</th>
                                                <th>TOTAL APPROVED</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group col-md-2">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <a href="{{ route('report') }}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>

<script>
    $(document).ready(function(){

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {                
                var min = $("#min").datepicker("getDate")

                var max = $("#max").datepicker("getDate")
                
                var date = data[2].split("/");
                var date1 =`${date[2]}-${date[1]}-${date[0]}`;
                var startDate = new Date(date1);
                console.log(startDate);
                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }
                return false;
            }
        );

        $("#min").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            orientation: "bottom right" ,
            onClose: function (date, inst) {
                //set the other dateTo the min date
                var selectedDate = $("#min").datepicker("getDate");
                var followingDate = new Date(selectedDate.getTime() + 86400000);
                followingDate.toLocaleDateString();
                $("#max").datepicker("option", "minDate", followingDate);
            }
        });

        $("#max").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        let query = window.location.href;
        var hash = query.split('/');
        var lastSegment = hash.pop() || hash.pop();
        let url = "{{ route('in_liue.report', ':id') }}";
        let urlx = url.replace(':id', lastSegment);

        let table = $('#table').DataTable({
            processing: true,
            serverSide: false,
            responsive: true,
            filter:true,
            "dom" : '<"wrapper"fltip>',
            ajax: {
                url: urlx,
                type: 'GET',
            },
            columns: [
                { data: 'staff_id'},
                { data: 'total_hour'},
                { data: 'date_apply'},
            ],
            columnDefs: [
                {className: "text-center", "targets":[2]},
                {visible:false,targets:[2]}
            ]
        });

        $("#table_filter").hide(); 
        
        $('#min, #max').change(function () {
           table.draw();
        });
    });
</script>
@endpush