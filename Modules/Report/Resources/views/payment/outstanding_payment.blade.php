@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .dt-buttons .dt-button {
        width:100%;
        background: #745af2;
        border: 1px solid #745af2;
        cursor: pointer;
    }

</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.22/b-1.6.5/sp-1.2.1/datatables.min.css"/>

@endpush

@section('content')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12" id="printSection">
                <div id="upper" class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">OUTSTANDING PAYMENT REPORT</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold;text-align:start">
                            <div class="form-group col-md-3">
                                <label for="startdate">1) Start Date</label>
                                <input type="text" class="form-control" name="startdate" id="min">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="enddate">2) End Date</label>
                                <input type="text" class="form-control" name="enddate" id="max">
                            </div>
                            <div class="col-md-5"></div>
                            <!-- <div class="col-md-1" style="align-items:baseline;display:grid;">
                                <label for="print"></label>
                                <a href="javascript:;" class="btn btn-primary print">
                                    <span class="icon-printer"></span> Print 
                                </a>
                            </div> -->
                            <div class="form-group col-md-1" style="align-items:end;display:grid;">
                                <a href="{{ route('report') }}" class="btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>
                    
                        
                        <div class="table-responsive">
                           
                            <table id="table1" class="table table-bordered table-striped" width="100%" cellspacing="0"
                                role="grid" aria-describedby="dataTable_info" style="width: 100%;"> 
                              
                              <thead class="thead-theme">
                                    <tr>
                                        <TD colspan='7' style="text-align: center;color:steelblue; font-weight:bold"><legend>INCOMING PAYMENT</legend></TD></tr>
                                    <tr>
                                        <th>JOB NUMBER</th>
                                        <th>CLIENT</th>
                                        <th>JOB TITLE</th>
                                        <th>DATE</th>
                                        <th>VALUE</th>
                                        <th>STATUS</th>
                                        <th>REMARKS</th>
                                    </tr>
                                </thead>
                                <tfoot><th></th></tfoot>
                            </table>
                        </div>
                         <div class="table-responsive">
                            <table id="table2" class="table table-bordered table-striped" width="100%" cellspacing="0"
                                role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                              <thead class="thead-theme">
                                   <tr>
                                        <TD colspan='8' style="text-align: center;color:steelblue; font-weight:bold"><legend>OUTGOING PAYMENT</legend></TD></tr>
                                    <tr>
                                        <th>JOB NUMBER</th>
                                        <th>JOB TITLE</th>
                                        <th>SUBCONT/ SUPPLIER</th>
                                        <th>DATE</th>
                                        <th>DESCRIPTION</th>
                                        <th>VALUE</th>
                                        <th>STATUS</th>
                                        <th>REMARKS</th>
                                    </tr>
                                </thead>
                                
                                <tfoot><th></th></tfoot>                          
                            </table>
                            
                        </div>
                    </div>
                                   
                    
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js"crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.21/dataRender/datetime.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" crossorigin="anonymous"></script>
    
<script>
    const formatter = new Intl.NumberFormat('ms-MY', {
        style: 'currency',
        currency: 'MYR',
        minimumFractionDigits: 2
    });

    $(function(){

        jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
            return this.flatten().reduce( function ( a, b ) {
                if ( typeof a === 'string' ) {
                    a = a.replace(/[^\d.-]/g, '') * 1;
                }
                if ( typeof b === 'string' ) {
                    b = b.replace(/[^\d.-]/g, '') * 1;
                }

                return a + b;
            }, 0 );
        } );

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                
                var min = $("#min").datepicker("getDate")

                var max = $("#max").datepicker("getDate")
                
                var date = data[9].split("/");
                var date1 =`${date[2]}-${date[1]}-${date[0]}`;
                var startDate = new Date(date1);

                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }
                return false;
                }
        );

        $("#min").datepicker({
            onSelect: function () {
                oTable1.draw();
                oTable2.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            orientation: "bottom right" ,
            onClose: function (date, inst) {
                //set the other dateTo the min date
                var selectedDate = $("#min").datepicker("getDate");
                var followingDate = new Date(selectedDate.getTime() + 86400000);
                followingDate.toLocaleDateString();
                $("#max").datepicker("option", "minDate", followingDate);
            }
        });

        $("#max").datepicker({
            onSelect: function () {
                oTable1.draw();
            oTable2.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });


        // let table1 = $('#table1');
        // let table2 = $('#table2');

        let query = window.location.href;
        var hash = query.split('/');
        var lastSegment = hash.pop() || hash.pop();
        let url1 = "{{ route('incoming_payment_by_date.view', ':id') }}";
        let urlx1 = url1.replace(':id', lastSegment);

        let url2 = "{{ route('outgoing_payment_by_date.view', ':id') }}";
        let urlx2 = url2.replace(':id', lastSegment);

        let tbl1 = $('#table1').DataTable({
            processing: true,
            serverSide: false,
            filter:true,
            "dom" : '<"wrapper"flBtip>',
            buttons: [
            {
                extend: 'print', text: '<i class="icon-printer"></i> Print', className: 'btn btn-primary',
                messageTop: 'The information in this table is copyright to Vertigo Tech Sdn Bhd.',
                exportOptions: {
                        columns: [0,1,2,3,4,5,6]
                },
                customize: function ( win ) {
                    var start = $("#min").val();
                    var end = $("#max").val();
                    start = (start) ? start : 'All Time'
                    end = (end) ? end : 'All Time'
                    
                    $(win.document.body).find('h1').css('text-align', 'center').html(`Outstanding Payment Report`);
                    $(win.document.body).find('h1').append(`
                    <h1 style="text-align:center;">Incoming Payment</h1>
                    <div class="row m-5">
                        <div class="col-12 text-center">
                                <p style="font-size: 18px;">Start Date: <strong style="padding-right:4em">${start}</strong>End Date: <strong>${end}</strong></p>
                        </div>
                    </div>`);

                    var total_value = $('table#table1').find('tfoot').find('td').eq(0).text();
                    total_value=total_value.split('TOTAL VALUE : ')
                    $(win.document.body).find('table').after(` 
                    <br>                   
                        <div class="row" style="justify-content:center;">
                            <div class="col-md-6">
                                <fieldset style="border: 1px black solid">
                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>TOTAL VALUE</b></legend>
                                    <h3 style="color:steelblue;font-weight:bold;text-align:center">${total_value[1]}</h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                        </div>`
                    );
                    $(win.document.body).find('td:nth-child(1), td:nth-child(2), td:nth-child(4), td:nth-child(5), td:nth-child(6), th:nth-child(1), th:nth-child(2), th:nth-child(4), th:nth-child(5), th:nth-child(6)').css('text-align', 'center');
                    $(win.document.body).css( 'font-size', '14px');
                }
            
            }],
            ajax: {
                    url: urlx1,
                    type: 'GET',
            },
            columns: [
                    {data: 'job_number'},
                    {data: 'client_name'},
                    {data: 'job_title'},
                    {data: 'expected_date'},
                    {
                        data: 'amount',
                        render: function(data, type, full){
                            var val = data.replace(/[^0-9\.]/g,'');
                            var amount = parseFloat(val);
                            var html = '<p>'+formatter.format(amount)+'</p>';
                            return html;
                        }
                    },
                    {data: 'status'},
                    {data: 'remarks'},
                    {data: 'id'},
                    {data: 'expected_date'},
                    {data: 'expected_date'},
            ],
            columnDefs: [
                 {
                    targets : [4], 
                    render: $.fn.dataTable.render.number( ',', '.', 2, 'RM ' ) 
                           
                },
                
                { "width": "40%", "targets": 2 },
                {
                "className": "text-center",
                "data":'status',
                "targets":[5],
                "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == 1 ? '<span class="text-success"></span>' : '<span class="text-danger">No</span>';
                        }
                        return data;
                    }
                },
                {
                "className": "text-center",
                "data":'remarks',
                "targets":[6],
                "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data =='' ? '<span class="text-secondary"> - </span>' : '<span class="text-secondary">'+data+'</span>';
                        }
                        return data;
                    }
                },
                {
                    targets : [3], 
                    type:'date',
                    render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' )
                },
                {className: "text-center", "targets":[7,8,9]},
                {visible:false,targets:[7,8,9]}
               
            ],
            drawCallback: function() {
                   //some click events initilized here
             },
            "footerCallback": function ( tfoot, data, start, end, display ) {
                var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;
                    
                    var api = this.api();
                    $( api.table().footer() ).html( "<td colspan='7' style='font-size:15pt;font-weight:bold;color:steelblue;text-align:center;padding-left: 48%'>TOTAL VALUE : RM "+
                        api.column( 4, {page:'current'} ).data().sum().toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    +"</td>");
            }
         });

         tbl1.buttons().container().appendTo('#table1_wrapper .col-md-6:eq(1)');
         $('div.dt-buttons').addClass('col-md-1');

         let tbl2 = $('#table2').DataTable( {
            processing: true,
            serverSide: false,
            filter:true,
            "dom" : '<"wrapper"flBtip>',
            buttons: [
            {
                extend: 'print', text: '<i class="icon-printer"></i> Print', className: 'btn-primary',
                messageTop: 'The information in this table is copyright to Vertigo Tech Sdn Bhd.',
                exportOptions: {
                        columns: [0,1,2,3,4,5,6,7]
                },
                customize: function ( win ) {
                    var start = $("#min").val();
                    var end = $("#max").val();
                    start = (start) ? start : 'All Time'
                    end = (end) ? end : 'All Time'
                    
                    $(win.document.body).find('h1').css('text-align', 'center').html(`Outstanding Payment Report`);
                    $(win.document.body).find('h1').append(`
                    <h1 style="text-align:center;">Incoming Payment</h1>
                    <div class="row m-5">
                        <div class="col-12 text-center">
                                <p style="font-size: 18px;">Start Date: <strong style="padding-right:4em">${start}</strong>End Date: <strong>${end}</strong></p>
                        </div>
                    </div>`);
                    var total_value = $('table#table2').find('tfoot').find('td').eq(0).text();
                    total_value=total_value.split('TOTAL VALUE : ')
                    $(win.document.body).find('table').after(` 
                    <br>                   
                        <div class="row" style="justify-content:center;">
                            <div class="col-md-6">
                                <fieldset style="border: 1px black solid">
                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>TOTAL VALUE</b></legend>
                                    <h3 style="color:steelblue;font-weight:bold;text-align:center">${total_value[1]}</h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                        </div>`
                    );
                    $(win.document.body).find('td:nth-child(1), td:nth-child(4), td:nth-child(6), td:nth-child(7), th:nth-child(1), th:nth-child(4), th:nth-child(6), th:nth-child(7)').css('text-align', 'center');
                    $(win.document.body).css( 'font-size', '14px');
                }
            }],
            ajax: {
                    url: urlx2,
                    type: 'GET',
            },
            columns: [
                    {data: 'job_number'},
                    {data: 'job_title'},
                    {data: 'subcont_name'},
                    {data: 'expected_date'},
                    {data: 'description'},
                    {
                        data: 'amount',
                        render: function(data, type, full){
                            var val = data.replace(/[^0-9\.]/g,'');
                            var amount = parseFloat(val);
                            var html = '<p>'+formatter.format(amount)+'</p>';
                            return html;
                        }
                    },
                    {data: 'status'},
                    {data: 'remarks'},
                    {data: 'id'},
                    {data: 'expected_date'},
            ],
            columnDefs: [
                {
                    targets : [5], 
                    render: $.fn.dataTable.render.number( ',', '.', 2, 'RM ' ) 
                           
                },
                {
                    targets : [3], 
                    type:'date',
                    render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' )
                },
                {
                "className": "text-center",
                "data":'status',
                "targets":[6],
                "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == 1 ? '<span class="text-success"></span>' : '<span class="text-danger">No</span>';
                        }
                        return data;
                    }
                },
                {
                "className": "text-center",
                "data":'remarks',
                "targets":[7],
                "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data =='' ? '<span class="text-secondary"> - </span>' : '<span class="text-secondary">'+data+'</span>';
                        }
                        return data;
                    }
                },
                {className: "text-center", "targets":[8,9]},
                {visible:false,targets:[8,9]}
            ],
            drawCallback: function() {
                   //some click events initilized here
             
            },
               "footerCallback": function ( tfoot, data, start, end, display ) {
                var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;
                    
                    var api = this.api();
                    $( api.table().footer() ).html( "<td colspan='8' style='font-size:15pt;font-weight:bold;color:steelblue;text-align:center;padding-left: 48%'>TOTAL VALUE : RM "+
                        api.column( 5, {page:'current'} ).data().sum().toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    +"</td>");
            }
        });

        tbl2.buttons().container().appendTo('#table2_wrapper .col-md-6:eq(1)');
        $('div.dt-buttons').addClass('col-md-1');
                    
        $('#min, #max').change(function () {
            tbl1.draw();
            tbl2.draw();
        });

        $('.print').on('click', function(){
            window.scrollTo(0,0);

            $('.header-logo').css('width','80%');

            var chart = document.querySelector("#printSection");
            html2canvas(chart,{
                windowWidth: window.innerWidth,
                windowHeight: window.innerHeight,
                scale: 2
            }).then(canvas => {
                var dataURL = canvas.toDataURL('image/jpeg',1.0);

                var pdf = new jsPDF();

                pdf.addImage(dataURL, 'JPEG', 5, 5, 250, 200);

                pdf.save("Outstanding_Payment");
            });

            $('.header-logo').css('width','50%');
        });
       
});    

        
    </script>
@endpush