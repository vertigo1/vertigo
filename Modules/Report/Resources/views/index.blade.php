@extends('layouts.app')
@push('styles')
<style>
 .card-no-border .card {
    background: grey;
    border-radius: 20px;
    box-shadow: 
    1px 1px 0px #000, 
    2px 2px 0px #000, 
    3px 3px 0px #000, 
    4px 4px 0px #000, 
    5px 5px 0px #000, 
    6px 6px 0px #000;
}
.text-danger {
    color: white !important;
    }
a {
    color: #ffff35;
    }
.text-centre {
    margin: auto;
    }
.card:hover{
   background-color: #5d5d5d;
}
h2:hover {
    color: black !important;
}
</style>
@endpush

@section('content')
<!-- <div class="row" style="padding-bottom:20px;padding-top:20px">
    <div class="col-md-12">
        <h3><strong>Report</strong></h3>
    </div>
</div> -->

<br>
<br>
<br>

<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <label><b>1) Select Location</b></label>
                <select class="form-control" id="location" name="location">
                    @foreach($region as $regions)
                        <option value="{{$regions['code']}}">{{$regions['region']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>

<br>

@if(Auth::user()->position == 'Director' || Auth::user()->position == 'Finance' || Auth::user()->position == 'Operation (Service)')
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a id="detail_financial" data-value="{{route('detail_financial', ':id')}}" href="" >
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>DETAIL FINANCIAL BY JOB NUMBER </strong></h2>
                                    <small><strong>Detail PO information, Expected Incoming, Outgoing Payment, All Purchase Request On Job Number C/W Status, Planned Costing Profit, Actual Current Profit, List All Status PR.</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a id="purchase_request_date" data-value="{{route('request_date', ':id')}}" href="" >
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>PURCHASE REQUEST BY DATE RANGE</strong></h2>
                                    <small><strong>All Purchase Requestion within timeframe by all requestor, all type category, complete with Total Amount within the selected timeframe. To analyse overall expenses.</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endif

@if(Auth::user()->position == 'Director' || Auth::user()->position == 'Finance' || Auth::user()->position == 'Operation (Service)')
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a id="overall_purchase" data-value="{{route('overall_purchase_by_date.view', ':id')}}" href="">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>PURCHASE ORDER BY PIC & DATE RANGE</strong></h2>
                                    <small><strong>List Purchase Order gained by specific Person in Charge within timeframe complete with I/O Value Expected Costing. To analyse performance at each Person - In - Charge.</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a id="purchase_type" data-value="{{route('order_type', ':id')}}" href="">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>OVERALL PURCHASE ORDER TYPE BY DATE</strong></h2>
                                    <small><strong>All Awarded Purchase Order by type within selected date. To analyse performance at each Section / Department.</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            @if(Auth::user()->position != 'HR' && Auth::user()->position != 'Operation (Service)')
            <div class="col-md-4">
                <a id="outstanding" data-value="{{route('outstanding_payment_by_date.view', ':id')}}" href="">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>OUTSTANDING PAYMENT REPORT</strong></h2>
                                    <small><strong>Details of Oustanding Payment (incoming outgoing) with detail information of expected date. To use information for future financial planning.</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @else
            <div class="col-md-4"></div>
            @endif

            @if(Auth::user()->position == 'Director' || Auth::user()->position == 'Finance' || Auth::user()->position == 'HR')
            <div class="col-md-4">
                <a id="medical" data-value="{{route('medical_claim', ':id')}}" href="">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger" style="margin-top:10%"><strong>TOTAL MEDICAL CLAIM BY STAFF NAME</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @else
            <div class="col-md-4"></div>
            @endif
        </div>
    </div>
</div>

@if(Auth::user()->position == 'Director' || Auth::user()->position == 'Finance' || Auth::user()->position == 'HR')
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a id="leave" data-value="{{route('leave.report', ':id')}}" href="">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>LEAVE SUMMARY</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a id="site_allowance" data-value="{{route('site_allowance.report', ':id')}}" href="">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>SITE ALLOWANCE SUMMARY</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endif

@if(Auth::user()->position == 'Director' || Auth::user()->position == 'Finance' || Auth::user()->position == 'HR')
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a id="in_liue" data-value="{{route('in_liue.report', ':id')}}" href="">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>IN LIUE SUMMARY</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>
@endif

@endsection

@push('scripts')
<script>
    $(() => {
        $("#detail_financial, #purchase_request_date, #purchase_type, #overall_purchase, #outstanding, #medical, #leave, #site_allowance, #in_liue").on('click',function(event){
            let currentId = $(this).attr('id');
            let id = $("#location").val();
            let url = $(`#${currentId}`).data('value');
            let urlx = url.replace(':id', id);
            $(`#${currentId}`).attr('href', urlx);
        })
    })
</script>
@endpush