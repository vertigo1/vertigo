@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .dt-buttons .dt-button {
        width:100%;
        background: #745af2;
        border: 1px solid #745af2;
        cursor: pointer;
    }
    
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.22/b-1.6.5/sp-1.2.1/datatables.min.css"/>
@endpush

@section('content')
<br>
<div class="row" id="printSection">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div id="upper" class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">PURCHASE ORDER BY TYPE REPORT</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold;text-align:start">
                            <div class="form-group col-md-3">
                                    <label> 1) Start Date</label>
                                    <input type="text" class="form-control" autocomplete="off" name="min" id="min">
                                </div>
                                <div class="form-group col-md-3">
                                    <label> 2) End Date</label>
                                    <input type="text" class="form-control" autocomplete="off" name="max" id="max">
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-1" style="align-items:baseline;display:grid;">
                                    <label for="print"></label>
                                    <a href="javascript:;" class="btn btn-primary print">
                                        <span class="icon-printer"></span> Print 
                                    </a>
                                </div>
                                <div class="form-group col-md-1" style="align-items:end;display:grid;">
                                    <a href="{{ route('report') }}" class="btn btn-outline-danger">Homepage</a>
                                </div>
                            </div>

                        <div class="card">
                            <div class="card-header" style="text-align:center;background-color: #745af2;">
                                <h3 style="font-weight:bold;color:white">1) SERVICE</h3>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="service" class="table table-bordered table-striped" width="100%" cellspacing="0"role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                        <thead class="thead-theme">
                                            <tr>
                                                <th>JOB NUMBER</th>
                                                <th>CLIENT</th>
                                                <th>CLIENT REF NUM</th>
                                                <th>PIC</th>
                                                <th>JOB TITLE</th>
                                                <th>PO DATE</th>
                                                <th>PO VALUE</th>
                                                <th>COST</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3" style="text-align:right">TOTAL PO VALUE</th>
                                                <th><p id="po_value_service"></p></th>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right;background-color:grey;color:white">% VALUE FROM OVERALL COMPANY PERFORMANCE</th>
                                                <th style="background-color:grey;color:white"><p id="overall_service"></p></th>
                                            </tr>
                                            <tr>
                                                <th colspan="3" style="text-align:right">TOTAL COST</th>
                                                <th><p id="cost_service"></p></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <th colspan="3" style="text-align:right;background-color:grey;color:white">TOTAL EXPECTED COSTING PROFIT</th>
                                                <th style="background-color:grey;color:white"><p id="total_service"></p></th>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right;background-color:grey;color:white">% EXPECTED PROFIT FROM OVERALL COMPANY PERFORMANCE</th>
                                                <th style="background-color:grey;color:white"><p id="profit_service"></p></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card">
                            <div class="card-header" style="text-align:center;background-color: #745af2;">
                                <h3 style="font-weight:bold;color:white">2) TRADING</h3>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="trading" class="table table-bordered table-striped" width="100%" cellspacing="0"role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                        <thead class="thead-theme">
                                            <tr>
                                                <th>JOB NUMBER</th>
                                                <th>CLIENT</th>
                                                <th>CLIENT REF NUM</th>
                                                <th>PIC</th>
                                                <th>JOB TITLE</th>
                                                <th>PO DATE</th>
                                                <th>PO VALUE</th>
                                                <th>COST</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3" style="text-align:right">TOTAL PO VALUE</th>
                                                <th><p id="po_value_trading"></p></th>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right;background-color:grey;color:white">% VALUE FROM OVERALL COMPANY PERFORMANCE</th>
                                                <th style="background-color:grey;color:white"><p id="overall_trading"></p></th>
                                            </tr>
                                            <tr>
                                                <th colspan="3" style="text-align:right">TOTAL COST</th>
                                                <th><p id="cost_trading"></p></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <th colspan="3" style="text-align:right;background-color:grey;color:white">TOTAL EXPECTED COSTING PROFIT</th>
                                                <th style="background-color:grey;color:white"><p id="total_trading"></p></th>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right;background-color:grey;color:white">% EXPECTED PROFIT FROM OVERALL COMPANY PERFORMANCE</th>
                                                <th style="background-color:grey;color:white"><p id="profit_trading"></p></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card">
                            <div class="card-header" style="text-align:center;background-color: #745af2;">
                                <h3 style="font-weight:bold;color:white">3) RENTAL</h3>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="rental" class="table table-bordered table-striped" width="100%" cellspacing="0"role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                        <thead class="thead-theme">
                                            <tr>
                                                <th>JOB NUMBER</th>
                                                <th>CLIENT</th>
                                                <th>CLIENT REF NUM</th>
                                                <th>PIC</th>
                                                <th>JOB TITLE</th>
                                                <th>PO DATE</th>
                                                <th>PO VALUE</th>
                                                <th>COST</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3" style="text-align:right">TOTAL PO VALUE</th>
                                                <th><p id="po_value_rental"></p></th>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right;background-color:grey;color:white">% VALUE FROM OVERALL COMPANY PERFORMANCE</th>
                                                <th style="background-color:grey;color:white"><p id="overall_rental"></p></th>
                                            </tr>
                                            <tr>
                                                <th colspan="3" style="text-align:right">TOTAL COST</th>
                                                <th><p id="cost_rental"></p></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <th colspan="3" style="text-align:right;background-color:grey;color:white">TOTAL EXPECTED COSTING PROFIT</th>
                                                <th style="background-color:grey;color:white"><p id="total_rental"></p></th>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right;background-color:grey;color:white">% EXPECTED PROFIT FROM OVERALL COMPANY PERFORMANCE</th>
                                                <th style="background-color:grey;color:white"><p id="profit_rental"></p></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="overall" class="table table-bordered table-striped" width="100%" cellspacing="0"role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                            <thead class="thead-theme">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr style="background-color: grey;color: white;font-weight: bold;">
                                    <th></th>
                                    <th></th>
                                    <th style="text-align:right">GRAND TOTAL PO VALUE</th>
                                    <th><p id="overall_po_value"></p></th>
                                    <th style="text-align:right">GRAND TOTAL PROFIT</th>
                                    <th><p id="overall_profit"></p></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr style="background-color: grey;color: white;font-weight: bold;">
                                    <th></th>
                                    <th></th>
                                    <th style="text-align:right">GRAND TOTAL COST</th>
                                    <th><p id="overall_cost"></p></th>
                                    <th style="text-align:right">PROFIT %</th>
                                    <th><p id="percent_profit"></p></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js"crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.21/dataRender/datetime.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" crossorigin="anonymous"></script>
<script>

    $(function(){
        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = $("#min").datepicker("getDate")

                var max = $("#max").datepicker("getDate")
                
                var date = data[8].split("/");
                var date1 =`${date[2]}-${date[1]}-${date[0]}`;
                var startDate = new Date(date1);

                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }
                return false;
            }
        );

        $("#min").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            orientation: "bottom right" ,
            onClose: function (date, inst) {
                var selectedDate = $("#min").datepicker("getDate");
                var followingDate = new Date(selectedDate.getTime() + 86400000);
                followingDate.toLocaleDateString();
                $("#max").datepicker("option", "minDate", followingDate);
            }
        });

        $("#max").datepicker({
            onSelect: function () {
                table.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });

        const formatter = new Intl.NumberFormat('ms-MY', {
            style: 'currency',
            currency: 'MYR',
            minimumFractionDigits: 2
        });

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        let query = window.location.href;
        var hash = query.split('/');
        var lastSegment = hash.pop() || hash.pop();
        let url = "{{ route('table_service', ':id') }}";
        let urlx = url.replace(':id', lastSegment);

        let service = $('#service').DataTable({
            processing: true,
            serverSide: false,
            filter:true,
            "dom" : '<"wrapper"flBtip>',
            buttons: [
            {
                extend: 'print', text: '<i class="icon-printer"></i> Print', className: 'btn btn-primary',
                messageTop: 'The information in this table is copyright to Vertigo Tech Sdn Bhd.',
                exportOptions: {
                        columns: [0,1,2,3,4,5,6,7 ]
                },
                customize: function ( win ) {
                    var start = $("#min").val();
                    var end = $("#max").val();
                    start = (start) ? start : 'All Time'
                    end = (end) ? end : 'All Time'
                    
                    $(win.document.body).find('h1').css('text-align', 'center').html(`Purchase Order Report`);
                    $(win.document.body).find('h1').append(`
                    <h1 style="text-align:center;">Type Service</h1>
                    <div class="row m-5">
                        <div class="col-12 text-center">
                                <p style="font-size: 18px;">Start Date: <strong style="padding-right:4em">${start}</strong>End Date: <strong>${end}</strong></p>
                        </div>
                    </div>`);

                    var total_po_value = $('table#service').find('tfoot').find('tr').eq(0).find('th').eq(1).find('p').html()
                    var overall_company_performance = $('table#service').find('tfoot').find('tr').eq(0).find('th').eq(5).find('p').html()
                    var total_cost = $('table#service').find('tfoot').find('tr').eq(1).find('th').eq(1).find('p').html()
                    var total_expected_costing_profit = $('table#service').find('tfoot').find('tr').eq(2).find('th').eq(1).find('p').html()
                    var expected_profit_in_percent = $('table#service').find('tfoot').find('tr').eq(2).find('th').eq(5).find('p').html()

                    $(win.document.body).find('table').append(` 
                    <tfoot>
                        <tr>
                            <th colspan="2" style="text-align:right" rowspan="1">TOTAL PO VALUE</th>
                            <th rowspan="1" colspan="2">${total_po_value}</th>
                            <th rowspan="1" colspan="1"></th>
                            <th style="text-align:right;color:black" rowspan="1" colspan="2">% VALUE FROM OVERALL COMPANY PERFORMANCE</th>
                            <th rowspan="1" colspan="1" style="color:black">${overall_company_performance}</th>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:right" rowspan="1">TOTAL COST</th>
                            <th rowspan="1" colspan="2">${total_po_value}</th>
                            <th rowspan="1" colspan="1"></th>
                            <th rowspan="1" colspan="2"></th>
                            <th rowspan="1" colspan="1"></th>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:right;color:black" rowspan="1">TOTAL EXPECTED COSTING PROFIT</th>
                            <th rowspan="1" colspan="2">${total_expected_costing_profit}</th>
                            <th rowspan="1" colspan="1"></th>
                            <th style="text-align:right;color:black" rowspan="1" colspan="2">% EXPECTED PROFIT FROM OVERALL COMPANY PERFORMANCE</th>
                            <th rowspan="1" colspan="1" style="color:black">${expected_profit_in_percent}</th>
                        </tr>
                    </tfoot>`);

                    var grand_total_po_value = $('table#overall').find('tfoot').find('tr').eq(0).find('th').eq(3).find('p').html()
                    var grand_total_profit = $('table#overall').find('tfoot').find('tr').eq(0).find('th').eq(5).find('p').html()
                    var grand_total_cost = $('table#overall').find('tfoot').find('tr').eq(1).find('th').eq(3).find('p').html()
                    var profit = $('table#overall').find('tfoot').find('tr').eq(1).find('th').eq(5).find('p').html()

                    $(win.document.body).find('table').after(`
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">GRAND TOTAL PO VALUE: ${grand_total_po_value}</h4>
                            </div>
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">GRAND TOTAL PROFIT: ${grand_total_profit}</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">GRAND TOTAL COST: ${grand_total_cost}</h4>
                            </div>
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">PROFIT : ${profit}</h4>
                            </div>
                        </div>
                    `)

                    $(win.document.body).find('td:nth-child(1), td:nth-child(2), td:nth-child(4), td:nth-child(5), td:nth-child(6), th:nth-child(1), th:nth-child(2), th:nth-child(4), th:nth-child(5), th:nth-child(6)').css('text-align', 'center');
                    $(win.document.body).css( 'font-size', '14px');
                }
            
            }],
            ajax: {
                url: urlx,
                type: 'GET',
            },
            columns: [
                {data: 'job_number'},
                {data: 'client_name'},
                {data: 'client_ref_no'},
                {data: 'staff_id'},
                {data: 'job_title'},
                {
                    data: 'po_date',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {
                    data: 'po_value',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var po_value = parseFloat(val);
                        var html = '<p>'+formatter.format(po_value)+'</p>';
                        return html;
                    }
                },
                {
                    data: 'total_outgoing', 
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var total_outgoing = parseFloat(val);
                        var html = '<p>'+formatter.format(total_outgoing)+'</p>';
                        return html;
                    }
                },
                {data: 'po_date'},
                {data: 'total_po'},
                {data: 'total_cost'},
            ],
            columnDefs: [
                {visible:false,targets:[8,9,10]}
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                po_service = api.column(6,{ page: 'current'}).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                cost_service = api.column(7,{ page: 'current'}).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                total_po = api.column(9).data().reduce( function (a, b) {
                    return b;
                }, 0 );

                total_cost = api.column(10).data().reduce( function (a, b) {
                    return b;
                }, 0 );

                var expected_service = 0;
                expected_service = po_service - cost_service;

                var service = total_po;
                var service_profit = total_po - total_cost;
                var overall_service = po_service/service*100;

                var profit_service = expected_service/service_profit*100;

                $('#po_value_service').text(formatter.format(po_service));
                $('#cost_service').text(formatter.format(cost_service));
                $('#total_service').text(formatter.format(expected_service));
                $('#overall_service').text(overall_service.toFixed(2)+' %');
                $('#profit_service').text(profit_service.toFixed(2)+' %');
            }
        });

        service.buttons().container().appendTo('#service_wrapper .col-md-6:eq(1)');

        let query1 = window.location.href;
        var hash1 = query1.split('/');
        var lastSegment1 = hash1.pop() || hash1.pop();
        let url1 = "{{ route('table_trading', ':id') }}";
        let urlx1 = url1.replace(':id', lastSegment1);

        let trading = $('#trading').DataTable({
            processing: true,
            serverSide: false,
            filter:true,
            "dom" : '<"wrapper"flBtip>',
            buttons: [
            {
                extend: 'print', text: '<i class="icon-printer"></i> Print', className: 'btn btn-primary',
                messageTop: 'The information in this table is copyright to Vertigo Tech Sdn Bhd.',
                exportOptions: {
                        columns: [0,1,2,3,4,5,6,7 ]
                },
                customize: function ( win ) {
                    var start = $("#min").val();
                    var end = $("#max").val();
                    start = (start) ? start : 'All Time'
                    end = (end) ? end : 'All Time'
                    
                    $(win.document.body).find('h1').css('text-align', 'center').html(`Purchase Order Report`);
                    $(win.document.body).find('h1').append(`
                    <h1 style="text-align:center;">Type Trading</h1>
                    <div class="row m-5">
                        <div class="col-12 text-center">
                                <p style="font-size: 18px;">Start Date: <strong style="padding-right:4em">${start}</strong>End Date: <strong>${end}</strong></p>
                        </div>
                    </div>`);

                    var total_po_value = $('table#trading').find('tfoot').find('tr').eq(0).find('th').eq(1).find('p').html()
                    var overall_company_performance = $('table#trading').find('tfoot').find('tr').eq(0).find('th').eq(5).find('p').html()
                    var total_cost = $('table#trading').find('tfoot').find('tr').eq(1).find('th').eq(1).find('p').html()
                    var total_expected_costing_profit = $('table#trading').find('tfoot').find('tr').eq(2).find('th').eq(1).find('p').html()
                    var expected_profit_in_percent = $('table#trading').find('tfoot').find('tr').eq(2).find('th').eq(5).find('p').html()

                    $(win.document.body).find('table').append(` 
                    <tfoot>
                        <tr>
                            <th colspan="2" style="text-align:right" rowspan="1">TOTAL PO VALUE</th>
                            <th rowspan="1" colspan="2">${total_po_value}</th>
                            <th rowspan="1" colspan="1"></th>
                            <th style="text-align:right;color:black" rowspan="1" colspan="2">% VALUE FROM OVERALL COMPANY PERFORMANCE</th>
                            <th rowspan="1" colspan="1" style="color:black">${overall_company_performance}</th>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:right" rowspan="1">TOTAL COST</th>
                            <th rowspan="1" colspan="2">${total_po_value}</th>
                            <th rowspan="1" colspan="1"></th>
                            <th rowspan="1" colspan="2"></th>
                            <th rowspan="1" colspan="1"></th>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:right;color:black" rowspan="1">TOTAL EXPECTED COSTING PROFIT</th>
                            <th rowspan="1" colspan="2">${total_expected_costing_profit}</th>
                            <th rowspan="1" colspan="1"></th>
                            <th style="text-align:right;color:black" rowspan="1" colspan="2">% EXPECTED PROFIT FROM OVERALL COMPANY PERFORMANCE</th>
                            <th rowspan="1" colspan="1" style="color:black">${expected_profit_in_percent}</th>
                        </tr>
                    </tfoot>`);

                    var grand_total_po_value = $('table#overall').find('tfoot').find('tr').eq(0).find('th').eq(3).find('p').html()
                    var grand_total_profit = $('table#overall').find('tfoot').find('tr').eq(0).find('th').eq(5).find('p').html()
                    var grand_total_cost = $('table#overall').find('tfoot').find('tr').eq(1).find('th').eq(3).find('p').html()
                    var profit = $('table#overall').find('tfoot').find('tr').eq(1).find('th').eq(5).find('p').html()

                    $(win.document.body).find('table').after(`
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">GRAND TOTAL PO VALUE: ${grand_total_po_value}</h4>
                            </div>
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">GRAND TOTAL PROFIT: ${grand_total_profit}</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">GRAND TOTAL COST: ${grand_total_cost}</h4>
                            </div>
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">PROFIT : ${profit}</h4>
                            </div>
                        </div>
                    `)

                    $(win.document.body).find('td:nth-child(1), td:nth-child(2), td:nth-child(4), td:nth-child(5), td:nth-child(6), th:nth-child(1), th:nth-child(2), th:nth-child(4), th:nth-child(5), th:nth-child(6)').css('text-align', 'center');
                    $(win.document.body).css( 'font-size', '14px');
                    
                }
            
            }],
            ajax: {
                url: urlx1,
                type: 'GET',
            },
            columns: [
                {data: 'job_number'},
                {data: 'client_name'},
                {data: 'client_ref_no'},
                {data: 'staff_id'},
                {data: 'job_title'},
                {
                    data: 'po_date',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {
                    data: 'po_value',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var po_value = parseFloat(val);
                        var html = '<p>'+formatter.format(po_value)+'</p>';
                        return html;
                    }
                },
                {
                    data: 'total_outgoing',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var total_outgoing = parseFloat(val);
                        var html = '<p>'+formatter.format(total_outgoing)+'</p>';
                        return html;
                    }
                },
                {data: 'po_date'},
                {data: 'total_po'},
                {data: 'total_cost'},
            ],
            columnDefs: [
                {visible:false,targets:[8,9,10]}
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                po_trading = api.column(6,{ page: 'current'}).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                cost_trading = api.column(7,{ page: 'current'}).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                total_po = api.column(9).data().reduce( function (a, b) {
                    return b;
                }, 0 );

                total_cost = api.column(10).data().reduce( function (a, b) {
                    return b;
                }, 0 );

                var expected_trading = 0;
                expected_trading = po_trading - cost_trading;

                var trading = total_po;
                var trading_profit = total_po - total_cost;
                var overall_profit = po_trading/trading*100;

                var profit_trading = expected_trading/trading_profit*100;

                $('#po_value_trading').text(formatter.format(po_trading));
                $('#cost_trading').text(formatter.format(cost_trading));
                $('#total_trading').text(formatter.format(expected_trading));
                $('#overall_trading').text(overall_profit.toFixed(2)+' %');
                $('#profit_trading').text(profit_trading.toFixed(2)+' %');
            }
        });
        trading.buttons().container().appendTo('#trading_wrapper .col-md-6:eq(1)');

        let query2 = window.location.href;
        var hash2 = query2.split('/');
        var lastSegment2 = hash2.pop() || hash2.pop();
        let url2 = "{{ route('table_rental', ':id') }}";
        let urlx2 = url2.replace(':id', lastSegment2);

        let rental = $('#rental').DataTable({
            processing: true,
            serverSide: false,
            filter:true,
            "dom" : '<"wrapper"flBtip>',
            buttons: [
            {
                extend: 'print', text: '<i class="icon-printer"></i> Print', className: 'btn btn-primary',
                messageTop: 'The information in this table is copyright to Vertigo Tech Sdn Bhd.',
                exportOptions: {
                        columns: [0,1,2,3,4,5,6,7 ]
                },
                customize: function ( win ) {
                    var start = $("#min").val();
                    var end = $("#max").val();
                    start = (start) ? start : 'All Time'
                    end = (end) ? end : 'All Time'
                    
                    $(win.document.body).find('h1').css('text-align', 'center').html(`Purchase Order Report`);
                    $(win.document.body).find('h1').append(`
                    <h1 style="text-align:center;">Type Rental</h1>
                    <div class="row m-5">
                        <div class="col-12 text-center">
                                <p style="font-size: 18px;">Start Date: <strong style="padding-right:4em">${start}</strong>End Date: <strong>${end}</strong></p>
                        </div>
                    </div>`);

                    var total_po_value = $('table#rental').find('tfoot').find('tr').eq(0).find('th').eq(1).find('p').html()
                    var overall_company_performance = $('table#rental').find('tfoot').find('tr').eq(0).find('th').eq(5).find('p').html()
                    var total_cost = $('table#rental').find('tfoot').find('tr').eq(1).find('th').eq(1).find('p').html()
                    var total_expected_costing_profit = $('table#rental').find('tfoot').find('tr').eq(2).find('th').eq(1).find('p').html()
                    var expected_profit_in_percent = $('table#rental').find('tfoot').find('tr').eq(2).find('th').eq(5).find('p').html()

                    $(win.document.body).find('table').append(` 
                    <tfoot>
                        <tr>
                            <th colspan="2" style="text-align:right" rowspan="1">TOTAL PO VALUE</th>
                            <th rowspan="1" colspan="2">${total_po_value}</th>
                            <th rowspan="1" colspan="1"></th>
                            <th style="text-align:right;color:black" rowspan="1" colspan="2">% VALUE FROM OVERALL COMPANY PERFORMANCE</th>
                            <th rowspan="1" colspan="1" style="color:black">${overall_company_performance}</th>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:right" rowspan="1">TOTAL COST</th>
                            <th rowspan="1" colspan="2">${total_po_value}</th>
                            <th rowspan="1" colspan="1"></th>
                            <th rowspan="1" colspan="2"></th>
                            <th rowspan="1" colspan="1"></th>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:right;color:black" rowspan="1">TOTAL EXPECTED COSTING PROFIT</th>
                            <th rowspan="1" colspan="2">${total_expected_costing_profit}</th>
                            <th rowspan="1" colspan="1"></th>
                            <th style="text-align:right;color:black" rowspan="1" colspan="2">% EXPECTED PROFIT FROM OVERALL COMPANY PERFORMANCE</th>
                            <th rowspan="1" colspan="1" style="color:black">${expected_profit_in_percent}</th>
                        </tr>
                    </tfoot>`);

                    var grand_total_po_value = $('table#overall').find('tfoot').find('tr').eq(0).find('th').eq(3).find('p').html()
                    var grand_total_profit = $('table#overall').find('tfoot').find('tr').eq(0).find('th').eq(5).find('p').html()
                    var grand_total_cost = $('table#overall').find('tfoot').find('tr').eq(1).find('th').eq(3).find('p').html()
                    var profit = $('table#overall').find('tfoot').find('tr').eq(1).find('th').eq(5).find('p').html()

                    $(win.document.body).find('table').after(`
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">GRAND TOTAL PO VALUE: ${grand_total_po_value}</h4>
                            </div>
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">GRAND TOTAL PROFIT: ${grand_total_profit}</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">GRAND TOTAL COST: ${grand_total_cost}</h4>
                            </div>
                            <div class="col-md-6">
                                <h4 style="text-align:center;color:steelblue;">PROFIT : ${profit}</h4>
                            </div>
                        </div>
                    `)

                    $(win.document.body).find('td:nth-child(1), td:nth-child(2), td:nth-child(4), td:nth-child(5), td:nth-child(6), th:nth-child(1), th:nth-child(2), th:nth-child(4), th:nth-child(5), th:nth-child(6)').css('text-align', 'center');
                    $(win.document.body).css( 'font-size', '14px');
                    
                }
            
            }],
            ajax: {
                url: urlx2,
                type: 'GET',
            },
            columns: [
                {data: 'job_number'},
                {data: 'client_name'},
                {data: 'client_ref_no'},
                {data: 'staff_id'},
                {data: 'job_title'},
                {
                    data: 'po_date',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {
                    data: 'po_value',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var po_value = parseFloat(val);
                        var html = '<p>'+formatter.format(po_value)+'</p>';
                        return html;
                    }
                },
                {
                    data: 'total_outgoing',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var total_outgoing = parseFloat(val);
                        var html = '<p>'+formatter.format(total_outgoing)+'</p>';
                        return html;
                    }
                },
                {data: 'po_date'},
                {data: 'total_po'},
                {data: 'total_cost'},
            ],
            columnDefs: [
                {visible:false,targets:[8,9,10]}
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                po_rental = api.column(6,{ page: 'current'}).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                cost_rental = api.column(7,{ page: 'current'}).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                total_po = api.column(9).data().reduce( function (a, b) {
                    return b;
                }, 0 );

                total_cost = api.column(10).data().reduce( function (a, b) {
                    return b;
                }, 0 );

                var expected_rental = 0;
                expected_rental = po_rental - cost_rental;

                var rental = total_po;
                var rental_profit = total_po - total_cost;
                var overall_profit = po_rental/rental*100;

                var profit_rental = expected_rental/rental_profit*100;

                $('#po_value_rental').text(formatter.format(po_rental));
                $('#cost_rental').text(formatter.format(cost_rental));
                $('#total_rental').text(formatter.format(expected_rental));
                $('#overall_rental').text(overall_profit.toFixed(2)+' %');
                $('#profit_rental').text(profit_rental.toFixed(2)+' %');
            }
        });

        rental.buttons().container().appendTo('#rental_wrapper .col-md-6:eq(1)');

        let query3 = window.location.href;
        var hash3 = query3.split('/');
        var lastSegment3 = hash3.pop() || hash3.pop();
        let url3 = "{{ route('table_overall', ':id') }}";
        let urlx3 = url3.replace(':id', lastSegment3);

        let overall_table = $('#overall').DataTable({
            processing: true,
            serverSide: false,
            paging: false,
            info: false,
            bPaginate: false,
            bLengthChange: false,
            bFilter: true,
            bInfo: false,
            bAutoWidth: false,
            filter:true,
            "dom" : '<"wrapper"ltip>',
            ajax: {
                url: urlx3,
                type: 'GET',
            },
            columns: [
                {
                    data: 'po_value',
                    render: function(data, type, full){
                        return '';
                    }
                },
                {
                    data: 'total_outgoing',
                    render: function(data, type, full){
                        return '';
                    }
                },
                {
                    data: 'total_outgoing',
                    render: function(data, type, full){
                        return '';
                    }
                },
                {
                    data: 'total_outgoing',
                    render: function(data, type, full){
                        return '';
                    }
                },
                {
                    data: 'total_outgoing',
                    render: function(data, type, full){
                        return '';
                    }
                },
                {
                    data: 'total_outgoing',
                    render: function(data, type, full){
                        return '';
                    }
                },
                {
                    data: 'total_outgoing',
                    render: function(data, type, full){
                        return '';
                    }
                },
                {
                    data: 'total_outgoing',
                    render: function(data, type, full){
                        return '';
                    }
                },
                {data: 'po_date'},
            ],
            columnDefs: [
                {className: "text-center", "targets":[8]},
                {visible:false,targets:[8]}
            ],
            fnInitComplete : function() {
                if ($(this).find('tbody tr').length > 0) {
                    $(this).find('tbody tr').hide();
                    $(this).find('thead tr').hide();
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                total_po = api.column(0,{ page: 'current'}).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                total_cost = api.column(1,{ page: 'current'}).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                var total_expected = 0;
                total_expected = total_po - total_cost;

                $('#overall_po_value').html(formatter.format(total_po));
                $('#overall_cost').html(formatter.format(total_cost));
                $('#overall_profit').html(formatter.format(total_expected));
                $('#percent_profit').html((total_expected/total_po*100).toFixed(2)+' %');
            }
        });

        $('#min, #max').change(function () {
            trading.draw();
            service.draw();
            overall_table.draw();
        });

        $('div.dt-buttons').addClass('col-md-1');
        
    });

    $('.print').on('click', function(){
        window.scrollTo(0,0);

        $('.header-logo').css('width','80%');

        var chart = document.querySelector("#printSection");
        html2canvas(chart,{
            windowWidth: window.innerWidth,
            windowHeight: window.innerHeight,
            scale: 2
        }).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF();

            pdf.addImage(dataURL, 'JPEG', 5, 5, 250, 250);

            pdf.save("Purchase_Order_Type_by_Date");
        });

        $('.header-logo').css('width','50%');
    });
</script>
@endpush