@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .dt-buttons .dt-button {
        width:100%;
        background: #745af2;
        border: 1px solid #745af2;
    }

    .dt-buttons {
        display: grid;
        align-items: end;
        margin-bottom: 0px;
        cursor: pointer;
    }
</style>
@endpush

@section('content')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12" id="printSection">
                <div id="upper" class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">OVERALL PURCHASE ORDER BY STAFF AND DATE</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="row" style="font-size: 15pt;color:black; font-weight:bold;text-align:start">
                            <div class="form-group col-md-3 block">
                                <label for="name">1) Staff Name</label>
                                    {{ Form::select('staff_id', $user, null,['class' => 'form-control select2', 'id' => 'staff-id']) }}
                            </div>
                            <div class="form-group col-md-3">
                                <label> 2) Start Date</label>
                                <input type="text" class="form-control" autocomplete="off" name="min" id="min">
                            </div>
                            <div class="form-group col-md-3">
                                <label> 3) End Date</label>
                                <input type="text" class="form-control" autocomplete="off" name="max" id="max">
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-1" style="align-items:baseline;display:grid;">
                                <label for="print"></label>
                                <a href="javascript:;" class="btn btn-primary print">
                                    <span class="icon-printer"></span> Print 
                                </a>
                            </div>
                            <div class="form-group col-md-1" style="align-items:end;display:grid;">
                                <a href="{{ route('report') }}" class="btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0"
                                role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                              <thead class="thead-theme">
                                    <tr>
                                        <th>JOB NUMBER</th>
                                        <th>CLIENT</th>
                                        <th>CLIENT REF NUM</th>
                                        <th>PO DATE</th>
                                        <th>PO VALUE</th>
                                        <th>JOB TITLE</th>
                                        <th>TOTAL OUTGOING</th>
                                        <th>EXPECTED PROFIT</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div id="footer" class="container" style="display: none">
                        <div class="row" style="justify-content:center;">
                            <div class="col-md-6">
                                <fieldset style="border: 1px black solid">
                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>TOTAL EXPECTED PROFIT</b></legend>
                                    <h3 id="sum_expected" style="color:steelblue;font-weight:bold;text-align:center"></h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                        </div>
                    </div>                  
                    
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js"crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.21/dataRender/datetime.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" crossorigin="anonymous"></script>
<script>
    $(function(){

       jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
            return this.flatten().reduce( function ( a, b ) {
                if ( typeof a === 'string' ) {
                    a = a.replace(/[^\d.-]/g, '') * 1;
                }
                if ( typeof b === 'string' ) {
                    b = b.replace(/[^\d.-]/g, '') * 1;
                }

                return a + b;
            }, 0 );
        } );

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = $("#min").datepicker("getDate")

                var max = $("#max").datepicker("getDate")
                
                var date = data[10].split("/");
                var date1 =`${date[2]}-${date[1]}-${date[0]}`;
                var startDate = new Date(date1);

                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }
                return false;
                }
        );

        $("#min").datepicker({
            onSelect: function () {
                dtbl.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            orientation: "bottom right" ,
            onClose: function (date, inst) {
                //set the other dateTo the min date
                var selectedDate = $("#min").datepicker("getDate");
                var followingDate = new Date(selectedDate.getTime() + 86400000);
                followingDate.toLocaleDateString();
                $("#max").datepicker("option", "minDate", followingDate);
            }
        });

        $("#max").datepicker({
            onSelect: function () {
                dtbl.draw();
            },
            format: 'dd/mm/yyyy',
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            clearBtn: true,
            orientation: "bottom right" ,
        });


        let query = window.location.href;
        var hash = query.split('/');
        var lastSegment = hash.pop() || hash.pop();
        let url = "{{ route('overall_purchase_by_date.view', ':id') }}";
        let urlx = url.replace(':id', lastSegment);

        let table = $('#table');

            const formatter = new Intl.NumberFormat('ms-MY', {
                style: 'currency',
                currency: 'MYR',
                minimumFractionDigits: 2
            });
                    
            var groupColumn = 8;
            var sum;
            var dtbl= table.DataTable({
                processing: true,
                serverSide: false,
                searching:true,
                retrieve:true,
                responsive: true,
                filter:true,
                "dom" : '<"wrapper"Bfltip>',
                buttons: [
                {
                    extend: 'print', text: '<i class="icon-printer"></i> Print', className: 'btn-primary',
                    messageTop: 'The information in this table is copyright to Vertigo Tech Sdn Bhd.',
                    exportOptions: {
                            columns: [ 0, 1, 2,3, 4,5,6,7 ]
                    },
                    customize: function ( win ) {
                        var staff = $("#staff-id option:selected").text();
                        (staff === "Select Staff") ? staff = "All Staff" : ''
                        var start = $("#min").val();
                        var end = $("#max").val();
                        start = (start) ? start : 'All Time'
                        end = (end) ? end : 'All Time'
                        
                        $(win.document.body).find('h1').css('text-align', 'center').html(`Purchase Order Report`);
                        $(win.document.body).find('h1').append(`
                        <div class="row m-5">
                            <div class="col-12 text-center">
                                    <p style="font-size: 18px;">Staff Name: <strong style="padding-right:4em">${staff}</strong>Start Date: <strong style="padding-right:4em">${start}</strong>End Date: <strong>${end}</strong></p>
                            </div>
                        </div>
                        `);

                        var sum_expected = $('h3#sum_expected').html()
                        $(win.document.body).find('table').after(` 
                        <br>                   
                        <div class="row" style="justify-content:center;">
                            <div class="col-md-6">
                                <fieldset style="border: 1px black solid">
                                    <legend style="text-align:center;font-weight:bold;padding: 0.2em 0.8em;width:auto; font-size:15pt"><b>TOTAL EXPECTED PROFIT</b></legend>
                                    <h3 style="color:steelblue;font-weight:bold;text-align:center">${sum_expected}</h3>
                                </fieldset>
                                <br><br><br>
                            </div>
                        </div>
                        `);

                        // $(win.document.body).find('td:nth-child(1), td:nth-child(2), td:nth-child(3), td:nth-child(5), th:nth-child(1), th:nth-child(2), th:nth-child(3), th:nth-child(5)').css('text-align', 'center');
                        $(win.document.body).css( 'font-size', '14px');
                    }
                
                }],
                ajax: {
                    url: urlx,
                    type: 'GET',
                },
                columns: [
                        {data: 'job_number'},
                        {data: 'client_name'},
                        {data: 'client_ref_no'},
                        {data: 'po_date'},
                        {
                            data: 'po_value',
                            render: function(data, type, full){
                                var val = data.replace(/[^0-9\.]/g,'');
                                var po_value = parseFloat(val);
                                var html = '<p>'+formatter.format(po_value)+'</p>';
                                return html;
                            }
                        },
                        {data: 'job_title'},
                        {
                            data: 'total_outgoing',
                            render: function(data, type, full){
                                var val = data.replace(/[^0-9\.]/g,'');
                                var po_value = parseFloat(val);
                                var html = '<p>'+formatter.format(po_value)+'</p>';
                                return html;
                            }
                        },
                        {data: 'expected_profit'},
                        {data: 'dates'},
                        {data: 'staff_id'},
                        {data: 'po_date'},
                    ],
                    columnDefs: [
                        {
                        "visible":false,
                        "targets":[groupColumn,9],
                        },
                        {
                            targets : [4,6,7], 
                            render: $.fn.dataTable.render.number( ',', '.', 2, 'RM ' ) 
                        },  
                        {
                            targets : [3], 
                            type:'date',
                            render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' )
                        },
                        {className: "text-center", "targets":[10]},
                        {visible:false,targets:[10]}
                    ],
            
                order: [[groupColumn, 'asc' ]],
                displayLength: 50,
                drawCallback: function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;
        
                    api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="8" style="font-weight:bold">'+group+'</td></tr>' // colspan based on number of column
                            );
        
                            last = group;
                        }
                    });

                    var sum= api.column( 7, {page:'current'} ).data().sum().toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    $("#footer").show();
                    $("#sum_expected").html("RM "+sum);
                    
                }, 
            }) 

            dtbl.buttons().container().insertAfter($('#printing'));
            $('.dt-buttons .dt-button').addClass('form-group');
            $('.dt-buttons').addClass('col-md-1');

        $('#min, #max').change(function () {
            dtbl.draw();
        });

        $("#staff-id").on('change',function(){
         if($(this).val()!=0)
            dtbl.column(9).search($(this).val()).draw();
         else
            dtbl.column(9).search("").draw();
        })
       
});    

        
    </script>
@endpush