<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('report')->middleware('auth')->group(function() {
    Route::middleware(['auth'])->group(function () {
        Route::get('/index', 'ReportController@index')->name('report');

        Route::get('/detail_financial/{id?}', 'ReportController@detail_financial')->name('detail_financial');
        //get job detail
        Route::get('/getjob/{id}','ReportController@getjob')->name('getjob');

        //PURCHASE REQUEST BY DATE RANGE
        Route::get('/purchase_request_date/{id?}', 'ReportController@request_date')->name('request_date');
        Route::get('/purchase_request_date/{id?}', 'ReportController@request_date')->name('request_date');
        //PURCHASE REQUEST BY DATE RANGE
        Route::get('/purchase_request_date/{id?}', 'ReportController@request_date')->name('request_date');

        //PURCHASE REQUEST TYPE
        Route::get('/purchase_order_type/{id?}', 'ReportController@order_type')->name('order_type');
        Route::get('/purchase_order_service/{id?}', 'ReportController@service')->name('table_service');
        Route::get('/purchase_order_trading/{id?}', 'ReportController@trading')->name('table_trading');
        Route::get('/purchase_order_rental/{id?}', 'ReportController@rental')->name('table_rental');
        Route::get('/purchase_order_overall/{id?}', 'ReportController@overall')->name('table_overall');

        //MEDICAL CLAIM REPORT
        Route::get('medical_claim_report/{id?}', 'ReportController@medical_claim')->name('medical_claim');

        //Overall Purchase By Date
        Route::get('/overallpurchasebydate/view/{id?}', 'ReportController@view_overall_purchase_by_date')->name('overall_purchase_by_date.view');

        Route::get('/outstandingpaymentbydate/view/{id?}', 'ReportController@view_outstanding_payment_by_date')->name('outstanding_payment_by_date.view');
        Route::get('/outgoingpaymentbydate/view/{id?}', 'ReportController@get_incoming_payment_by_date')->name('incoming_payment_by_date.view');
        Route::get('/incomingpaymentbydate/view/{id?}', 'ReportController@get_outgoing_payment_by_date')->name('outgoing_payment_by_date.view');
        // Route::get('/medicalclaim/view', 'ReportController@get_medical_claim_by_date')->name('medical_claim_by_date.view');

        //Leave Application
        Route::get('leave_application_report/{id}', 'ReportController@leave_application_report')->name('leave.report');

        //Site Allowance
        Route::get('site_allowance_report/{id?}', 'ReportController@site_allowance_report')->name('site_allowance.report');

        //In Liue
        Route::get('in_liue_summary_report/{id?}', 'ReportController@in_liue_report')->name('in_liue.report');
    });
});
