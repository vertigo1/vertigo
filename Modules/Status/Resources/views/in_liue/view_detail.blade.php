@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.1vw;
        color: black;
    }

    .text-danger{
        font-size: 1rem;
    }

    #textarea {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        background-color: #e9ecef;
        opacity: 0.7;
        border: 1px solid #ced4da;
        min-height: 100%;
        padding: 2px;
        width: 100%;
        font-weight: 400;
        padding: .375rem .75rem;
    }

    #input {
        -moz-appearance: textfield;
        -webkit-appearance: textfield;
        background-color: #e9ecef;
        opacity: 0.7;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        padding: .375rem .75rem;    
        text-align: center;
        min-height: 38px;
        height: calc(2.25rem + 2px);
        width: 100%;
        font-weight: 400;
        line-height: 1.5;
    }

</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
            <div class="row">
                <div class="col-md-9" id="printSection">
                    <div class="card" style="border: 2px solid lightgrey">
                        <div class="card-header" style="padding:0;height:100%;text-align:right;">
                            <div class="row" style="margin:0;">
                                <div class="col-md-2 font-13 font-13" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                    <img width="100%" height="100%" src="{{$imgbase64}}">
                                </div>
                                <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                    <p style="font-weight:bold;margin-top:2%;font-size:40px;">OFF IN LIUE FORM</p>
                                    <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                                </div>
                                <div class="col-md-3" style="background-color:#868686;color:white">
                                    <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                    <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                                </div>
                                <div class="col-md-1" style="background-color:#868686;color:white">
                                    <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$data[0]->inlieu->id}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <b class="col-md-12 text-danger">* Requestor to attach all / any related documents.</b>
                                <b class="col-md-12 text-danger">* Site visit Safety Briefing not included.</b>
                                <b class="col-md-12 text-danger">* All application form required to be submitted within 1 month from execution date.</b>
                                <b class="col-md-12 text-danger">* Off In-Liue is not applicable during outstation.</b>
                                <b class="col-md-12 text-danger">* 16 working hours equal to 1 day leave.</b>
                                <b class="col-md-12 text-danger">* Only applicable on OFF-Day.</b>
                            </div>
                            <form id="in_liue" method="POST" action="{{route('in_liue.post')}}">
                            @csrf
                            <div class="row">
                                <label for="" class="col-md-2 font-13">STAFF ID</label>
                                <div class="form-group col-md-4">
                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="text" name="staff_id" value="{{$data[0]->inlieu->staff_id}}" readonly>
                                </div>

                                <label for="" class="col-md-2 font-13">JOB TITLE</label>
                                <div class="form-group col-md-4">
                                    <div id="textarea" name="job_title" class="font-13">{{$job_title}}</div>
                                </div>
                            </div>

                            <div class="row">
                                <label for="" class="col-md-2 font-13">DATE APPLY</label>
                                <div class="form-group col-md-4">
                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="date" name="date_apply" value="{{$data[0]->inlieu->date_apply}}" readonly>
                                </div>

                                <label for="" class="col-md-2 font-13">JOB NUMBER</label>
                                <div class="form-group col-md-4">
                                    
                                    <input class="form-control input-group-lg font-13" autocomplete="off" type="text" name="job_number" value="{{$data[0]->inlieu->job_number}}" readonly> 
                                </div>
                            </div>

                            <div class="table-responsive" style="overflow-y: hidden;overflow-x: hidden;">
                                <table id="table-liue" class="table table-bordered font-13" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="vertical-align:middle;text-align:center;width:25%">OFF DAY DESCRIPTION</th>
                                            <th style="vertical-align:middle;text-align:center;width:14%">DATE WORK</th>
                                            <th style="vertical-align:middle;text-align:center;width:13%">START HOUR</th>
                                            <th style="vertical-align:middle;text-align:center;width:13%">END HOUR</th>
                                            <th style="vertical-align:middle;text-align:center;width:10%">TOTAL HOUR</th>
                                            <th style="vertical-align:middle;text-align:center;width:25%">JOB DESCRIPTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $item)

                                        <tr>
                                            <td><div id="textarea" name="off_day_desc" class="font-13">{{$item->off_day_desc}}</div></td>
                                            <td><input class="form-control input-group-lg font-13" type="hidden" name="date_work[]" value="{{$item->date_work}}" readonly></td>
                                            <td><input class="form-control input-group-lg font-13" style="text-align:center;" autocomplete="off" type="text" value="{{$item->start_hour}}" readonly></td>
                                            <td><input class="form-control input-group-lg font-13" style="text-align:center;" autocomplete="off" type="text" value="{{$item->end_hour}}" readonly></td>
                                            <td><input class="form-control input-group-lg font-13" style="text-align:center;" autocomplete="off" type="text" value="{{$item->total_hour}}" readonly></td>
                                            <td><div id="textarea" name="job_description" class="font-13">{{$item->job_description}}</div></td>
                                        </tr>
                                            
                                        @endforeach
                                        
                                    <tbody>
                                </table>
                            </div>

                            <br>

                            <div class="header">
                            <p class="col-md-12" style="text-align:center; font-weight: bold;">APPROVAL SECTION</p>
                        </div>

                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">Requestor to get approval from dedicated Superior</b>
                        </div>

                        <div class="row" style="justify-content:center;grid-gap:30px">
                            <div class="form-group col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" style="margin-bottom:33%;">REQUESTOR</label>
                                <p>------------------------------------------------------------------------------------</p>
                                <p>DATE ----------------------------------------------------------------------------</p>
                            </div>
                            
                            <div class="form-group col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" style="margin-bottom:33%;">APPROVED BY</label>
                                <p>------------------------------------------------------------------------------------</p>
                                <p>DATE ----------------------------------------------------------------------------</p>
                            </div>
                        </div>

                        <br/>

                        <div class="header">
                            <p class="col-md-12" style="text-align:center; font-weight: bold;">SUBMISSION SECTION</p>
                        </div>

                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">Requestor to submit complete form with related documents to Admin</b>
                        </div>

                        <div class="row" style="justify-content:center;grid-gap:30px">                            
                            <div class="form-group col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" style="margin-bottom:33%;">RECEIVED BY (ADMIN)</label>
                                <p>------------------------------------------------------------------------------------</p>
                                <p>DATE ----------------------------------------------------------------------------</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black; font-size:1.1vw;">
                            <ul>
                                <li>
                                    <p>Fill all the required spaces.</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <button type="button" id="printBtn" class="col-md-12 btn btn-info print" href="javascript:;" style="font-size:1.1vw;">Print</button> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('in_liue.view')}}" class="col-md-12  btn btn-outline-danger" style="font-size:1.1vw;">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script>

    $('input[name ="date_work[]"]').each(function(key, value){
            var dateObject = $(this).val(); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            div.css({'text-align': 'center', 'min-height': '0%'}).attr('id','textarea').addClass("textarea font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
            $(this).hide();
        }) 

    $('.print').on('click', function(){

        $( "#printSection" ).removeClass('col-md-9');
        $( "#printSection" ).addClass('col-md-8');

        $('input[name ="date_work[]"]').each(function(key, value){
            var dateObject = $(this).val(); 
            var date = new Date(dateObject)
            var div = $("<div>").appendTo($(this).parent());
            var hh = ("0" + date.getDate()).slice(-2)
            var mm = ("0" + (date.getMonth() + 1)).slice(-2)
            $(this).val(`${hh}-${mm}-${date.getFullYear()}`);
    })  

        window.scrollTo(0,0);

        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            scrollX: 0,
            scrollY: 0,
            x: 0,
            y: 0,
            width: 1900,
            height: 1800
        }; 

        var chart = document.querySelector("#printSection");
        var width = chart.offsetWidth;
        var height = chart.offsetHeight;
        html2canvas(chart, options).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF();

            pdf.addImage(dataURL, 'JPEG', 5, 5, width/6.5, height/6.5, '', 'SLOW');

            pdf.save("In_Liue");
        });

        $( "#printSection" ).removeClass('col-md-8');
        $( "#printSection" ).addClass('col-md-9');
    });
</script>
@endpush