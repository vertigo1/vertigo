@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:70px;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:4%;font-size:40px;">OFF IN LIUE STATUS</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="form-group row" style="justify-content:center;grid-gap:30px">
                            <div class="col-md-4"></div>

                            <div class="col-md-2" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:200px;margin-top:-30px;margin-left:30px;background:white;text-align:center">Grand Total Hours</p>
                                <h1>{{$total}}</h1>
                            </div>

                            <div class="col-md-3"></div>

                            <div class="col-md-2">
                                <a href="{{route('status')}}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>STAFF NAME</th>
                                        <th>DATE APPLY</th>
                                        <th>JOB NUMBER</th>
                                        <th>DATE WORK</th>
                                        <th>START HOUR</th>
                                        <th>END HOUR</th>
                                        <th>TOTAL HOUR</th>
                                        <th>JOB DESCRIPTION</th>
                                        <th>APPROVED</th>
                                        <th>REMARKS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    
    $(() => {
        let table = $('#table');

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        table.DataTable({
            "dom" : '<"wrapper"fl<t>ip>',
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('in_liue.view') }}",
                type: 'GET',
            },
            columns: [
                {data: 'staff_id'},
                {
                    data: 'date_apply',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {data: 'job_number'},
                {
                    data: 'date_work',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {data: 'start_hour'},
                {data: 'end_hour'},
                {data: 'total_hour'},
                {data: 'job_description'},
                {data: 'status'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
            columnDefs: [
                {"className": "text-center",
                "data":'status',
                "targets":[8],
                "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == 1 ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>';
                        }
                        return data;
                    }
                },
               

            ]
        }) 
    })

</script>
@endpush