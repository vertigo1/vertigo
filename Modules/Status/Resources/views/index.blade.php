@extends('layouts.app')
@push('styles')
<style>
 .border-3d {
    background: grey;
    border-radius: 20px;
    box-shadow: 
    1px 1px 0px #000, 
    2px 2px 0px #000, 
    3px 3px 0px #000, 
    4px 4px 0px #000, 
    5px 5px 0px #000, 
    6px 6px 0px #000 !important;
}
.text-white {
    color: white !important;
    }
.text-centre {
    margin: auto;
    }
div.border-3d:hover{
   background-color: #5d5d5d;
}
h2:hover {
    color: black !important;
}
</style>
@endpush

@section('content')
<!-- <div class="row" style="padding-bottom:20px;padding-top:20px">
    <div class="col-md-12">
        <h3><strong>Status</strong></h3>
    </div>
</div> -->
<br/>
<br/>
<br/>

<table align="center">
  <tbody>
    <tr>
      <td>
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="justify-content:center;">
                    <div class="col-md-10" style="margin-bottom:25px;min-height:120px;">
                        <a href="{{route('purchase_request.view')}}">
                            <div class="card h-100 border-3d">
                                <div class="card-body" style="display:flex;">
                                    <div class="row" style="margin:auto">
                                        <div class="col-md-12 text-center">
                                            <h2 class="text-white"><strong>PURCHASE REQUEST STATUS</strong></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
      </td>
      <td rowspan="4" style="vertical-align: top;">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <div class="card" style="border:none;">
                            <div class="card-header" style="text-align:center;background-color:#0027e6;pointer-events:none;width:100%;height:6rem;">
                                <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INFO</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p><strong>The status of following applications must be referred in the</strong></p>
                                        <p class="text-danger"><strong>Purchase Request Status</strong></p>
                                    </div>
                                </div>

                                <br/>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <ol>
                                            <b><strong>1) Outstation</strong></b>
                                        </ol>
                                        <ol>
                                            <b><strong>2) Site Allowance</strong></b>
                                        </ol>
                                        <ol>
                                            <b><strong>3) Medical Claim</strong></b>
                                        </ol>
                                        <ol>
                                            <b><strong>4) Mileage Claim</strong></b>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </td>
    </tr>

    <tr>
      <td>
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="justify-content:center;">
                    <div class="col-md-10" style="margin-bottom:25px;min-height:120px;">
                        <a href="{{route('leave_application.view')}}">
                            <div class="card h-100 border-3d">
                                <div class="card-body" style="display:flex;">
                                    <div class="row" style="margin:auto">
                                        <div class="col-md-12 text-center">
                                            <h2 class="text-white"><strong>LEAVE APPLICATION STATUS</strong></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="justify-content:center;">
                    <div class="col-md-10" style="margin-bottom:25px;min-height:120px;">
                            @if($inliue == 1)
                            <a href="{{route('in_liue.view')}}">
                            @else
                            <a href="javascript:void(0)">
                            @endif
                            <div class="card h-100 border-3d">
                                <div class="card-body" style="display:flex;">
                                    <div class="row" style="margin:auto">
                                        <div class="col-md-12 text-center">
                                            <h2 class="text-white"><strong>OFF IN LIUE STATUS</strong></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="justify-content:center;">
                    <div class="col-md-10" style="margin-bottom:25px;min-height:120px;">
                        <a href="{{route('purchase_order.view')}}">
                        <div class="card h-100 border-3d">
                                <div class="card-body" style="display:flex;">
                                    <div class="row" style="margin:auto">
                                        <div class="col-md-12 text-center">
                                            <h2 class="text-white"><strong>CHECK REGISTERED PURCHASE ORDER</strong></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
      </td>
    </tr>
  </tbody>
</table>
@endsection

@push('scripts')
@endpush