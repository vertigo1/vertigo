@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.1vw;
        color: black;
    }

    .text-danger{
        font-size: 1rem;
    }

    #textarea {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        border: 1px solid #ced4da;
        min-height: 100%;
        padding: 2px;
        width: 100%;
        background: white;
        font-weight: 400;
        padding: .375rem .75rem;
    }

    .input {
        -moz-appearance: textfield;
        -webkit-appearance: textfield;
        background-color: #e9ecef;
        opacity: 0.7;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        padding: .375rem .75rem;    
        text-align: center;
        min-height: 38px;
        height: calc(2.25rem + 2px);
        width: 100%;
        font-weight: 400;
        line-height: 1.5;
    }

</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9" id="printSection">
                <div class="card" style="border: 2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2 font-13 font-13 font-13" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">LEAVE APPLICATION FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$data->id}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="leave" method="POST" action="{{route('leave_application.post')}}">
                        @csrf
                        <div class="form-group row">
                            <b class="col-md-12 text-danger">* Leave form must be submitted at least 5 DAYS before leave date.</b>
                            <b class="col-md-12 text-danger">* Other than annual leaves, please attach relevant supporting documents for reference.</b>
                            <b class="col-md-12 text-danger">* Approval confirmation only valid through system. Please check your Leave Application Status before taking a leave.</b>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">STAFF ID</label>
                            <div class="form-group col-md-3">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="staff_id" value="{{$data->staff_id}}" readonly>
                            </div>
                            <label for="" class="col-md-2 font-13" style="text-align:center;">DATE APPLY</label>
                            <div class="form-group col-md-3">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="date"  name="apply" value="{{$data->date_apply}}" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">START DATE</label>
                            <div class="form-group col-md-3">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="date"  id="start" name="start" value="{{$data->start_date}}" readonly>
                            </div>

                            <label for="" class="col-md-2 font-13" style="text-align:center;">TOTAL DAY</label>

                            <label for="" class="col-md-3 font-13" style="text-align:center;">TYPE OF LEAVE</label>

                            <label for="" class="col-md-2 font-13" style="text-align:center;">TOTAL BALANCE LEAVE</label>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2 font-13">END DATE</label>
                            <div class="form-group col-md-3">
                                <input class="form-control input-group-lg font-13" autocomplete="off" type="date" id="end" name="end" value="{{$data->end_date}}" readonly>
                            </div>

                            <div class="form-group col-md-2">
                                <input class="form-control input-group-lg font-13" style="text-align:center;" autocomplete="off" type="text"  id="total_day" name="total_day" value="{{$data->total_day}}" readonly>
                            </div>

                            <div class="form-group col-md-3">
                                <div id="type_leave" class="input font-13"></div>
                            </div>

                            <div class="form-group col-md-2">
                                <div id="balance" class="input font-13"></div>
                            </div>
                        </div>

                        <div class="row" style="height: 150px;">
                            <label for="" class="col-md-2 font-13">REASON</label>
                            <div class="form-group col-md-10">
                                <div id="textarea" name="job_title" name="reason" class="font-13">{{$data->reason}}</div>
                            </div>
                        </div>

                        <hr/><br/>

                        <div class="header">
                            <p class="col-md-12" style="text-align:center; font-weight: bold;">WORK HAND OVER SECTION</p>
                        </div>

                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">Requestor to get agreement from peer</b>
                        </div>

                        <div class="form-group row">
                            <p class="col-md-12" style="text-align:center">TimeFrame : 3 working days</p>
                        </div>

                        <div class="row" style="justify-content:center;grid-gap:20px;">
                            <div class="form-group col-md-7" style="border:1px solid black;height:230px;">
                                <div class="col-sm-12" style="height:60%;">
                                <br/>   
                                    <label class="font-13" for="">HAND OVER SCOPE OF WORK</label>
                                    <br/>
                                    <div id="textarea" name="work_scope" class="font-13">{{$data->work_scope}}</div>
                                </div>
                            </div>

                            <div class="form-group col-md-4" style="border:1px solid black;height:230px;">
                            <br/>
                                <label class="font-13" for="" >AGREED BY</label>
                                <br/>
                                <div class="row" style="margin-top:10px;">
                                    <label for="" class="col-md-4 font-13">STAFF ID</label>
                                    <div class="form-group col-md-8">
                                        <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  id="total_day" name="" value="{{$data->staff_handover}}" readonly>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <label for="" class="col-md-4 font-13">DATE</label>
                                    <div class="form-group col-md-8">
                                    <p>------------------------------------------------</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr/><br/>

                        <div class="header">
                            <p class="col-md-12" style="text-align:center;font-weight: bold;">SUBMISSION SECTION</p>
                        </div>

                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">Requestor to get approval from dedicated Superior</b>
                        </div>

                        <div class="form-group row">
                            <p class="col-md-12" style="text-align:center">Timeframe : 3 working days</p>
                        </div>

                        <div class="form-row" style="justify-content:center;grid-gap:30px">
                            <div class="col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" style="margin-bottom:33%;">REQUESTOR</label>
                                <p>------------------------------------------------------------------------------</p>
                                <p>DATE ----------------------------------------------------------------------</p>
                            </div>
                            
                            <div class="col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" style="margin-bottom:33%;">APPROVED BY</label>
                                <p>------------------------------------------------------------------------------</p>
                                <p>DATE ----------------------------------------------------------------------</p>
                            </div>
                        </div>
                        <br/>

                        <div class="form-group row">
                            <b class="col-md-12 text-danger" style="text-align:center">System Admin require 2 DAYS to update status in System</b>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <h3 class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black; font-size:1.1vw;">
                            <ul>
                                <li>
                                <p>Fill all the required spaces</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <button type="button" id="printBtn" class="col-md-12 btn btn-info print" href="javascript:;" style="font-size:1.1vw">Print</button> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('leave_application.view')}}" class="col-md-12 btn btn-outline-danger" style="font-size:1.1vw">Back</a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    var data = {!! $data !!};
    var staff = {!! $staff !!};

    if(data.type_leave == 0){
        $('#type_leave').text('Annual Leave');
        $('#balance').text(staff.annual_leave);
    }else if(data.type_leave == 1){
        $('#type_leave').text('In Liue Leave');
        $('#balance').text(staff.liue_leave);
    }else if(data.type_leave == 2){
        $('#type_leave').text('Medical Leave');
        $('#balance').text(staff.medical_leave);
    }else if(data.type_leave == 3){
        $('#type_leave').text('Sick Leave');
        $('#balance').text(staff.medical_leave);
    }else if(data.type_leave == 4){
        $('#type_leave').text('Unpaid Leave');
        $('#balance').text('');
    }else if(data.type_leave == 5){
        $('#type_leave').text('Others');
        $('#balance').text('');
    }

    


    $('.print').on('click', function(){

        $( "#printSection" ).removeClass('col-md-9');
        $( "#printSection" ).addClass('col-md-8');

        window.scrollTo(0,0);

        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            scrollX: 0,
            scrollY: 0,
            x: 0,
            y: 0,
            width: 1900,
            height: 1800
        }; 

        var chart = document.querySelector("#printSection");
        var width = chart.offsetWidth;
        var height = chart.offsetHeight;
        html2canvas(chart, options).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF();

            pdf.addImage(dataURL, 'JPEG', 5, 5, width/6.5, height/6.5, '', 'SLOW');

            pdf.save("Leave_Application");
        });

        $( "#printSection" ).removeClass('col-md-8');
        $( "#printSection" ).addClass('col-md-9');
    });
</script>
@endpush