@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:70px;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:4%;font-size:40px;">LEAVE APPLICATION STATUS</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="form-group row" style="justify-content:center;grid-gap:30px">
                            <div class="col-md-1" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:150px;margin-top:-30px;margin-left:-20px;background:white;text-align:center">Apply Annual</p>
                                <h1>{{$total_annual}}</h1>
                            </div>

                            <div class="col-md-1" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:150px;margin-top:-30px;margin-left:-20px;background:white;text-align:center">Approved Annual</p>
                                <h1>{{$approved_annual}}</h1>
                            </div>

                            <div class="col-md-1" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:150px;margin-top:-30px;margin-left:-20px;background:white;text-align:center">Balance Annual</p>
                                <h1>{{$balanced_annual}}</h1>
                            </div>

                            <div class="col-md-1" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:150px;margin-top:-30px;margin-left:-20px;background:white;text-align:center">Apply In Liue</p>
                                <h1>{{$total_liue}}</h1>
                            </div>

                            <div class="col-md-1" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:150px;margin-top:-30px;margin-left:-20px;background:white;text-align:center">Approved In Liue</p>
                                <h1>{{$approved_liue}}</h1>
                            </div>

                            <div class="col-md-1" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:150px;margin-top:-30px;margin-left:-20px;background:white;text-align:center">Balance In Liue</p>
                                <h1>{{$balanced_liue}}</h1>
                            </div>

                            <div class="col-md-1" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:150px;margin-top:-30px;margin-left:-20px;background:white;text-align:center">Apply Medical</p>
                                <h1>{{$total_medical}}</h1>
                            </div>

                            <div class="col-md-1" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:155px;margin-top:-30px;margin-left:-25px;background:white;text-align:center">Approved Medical</p>
                                <h1>{{$approved_medical}}</h1>
                            </div>

                            <div class="col-md-1" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:150px;margin-top:-30px;margin-left:-20px;background:white;text-align:center">Balance Medical</p>
                                <h1>{{$balanced_medical}}</h1>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <a href="{{route('status')}}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>DATE APPLY</th>
                                        <th>STAFF</th>
                                        <th>STAFF HANDOVER</th>
                                        <th>START DATE</th>
                                        <th>END DATE</th>
                                        <th>TOTAL DAY</th>
                                        <th>LEAVE TYPE</th>
                                        <th>REASON</th>
                                        <th>STATUS</th>
                                        <th>APPROVED DATE</th>
                                        <th>REMARKS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script>

    $(() => {
        let table = $('#table');

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        table.DataTable({
            "dom" : '<"wrapper"fltip>',
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('leave_application.view') }}",
                type: 'GET',
            },
            columns: [
                {
                    data: 'date_apply',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {data: 'staff_id'},
                {data: 'staff_handover'},
                {
                    data: 'start_date',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {
                    data: 'end_date',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {data: 'total_day'},
                {data: 'type_leave'},
                {data: 'reason'},
                {data: 'status'},
                {
                    data: 'approved_date',
                    render: function(data, type, full){
                        if(data == '0000-00-00'){
                            var htmlRender = '<p> - </p>';
                            return htmlRender;
                        }else{
                            var payment_date = new Date(data);
                            var htmlRender = '<p>'+payment_date.toLocaleDateString('ms-MY',option)+'</p>';
                            return htmlRender;
                        }
                    }
                },
                {data:'remarks'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
            columnDefs: [
                {"className": "text-center",
                "data":'status',
                "targets":[8],
                "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == 1 ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>';
                        }
                        return data;
                    }
                },
                {"className": "text-center", "targets": [5]},
                {"className": "text-center",
                "data":'type_leave',
                "targets":[6],
                "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            if(data == 0){var leave = 'Annual Leave';}
                            else if(data == 1){var leave = 'In Liue Leave';}
                            else if(data == 2){var leave = 'Medical Leave';}
                            else if(data == 3){var leave = 'Sick Leave';}
                            else if(data == 4){var leave = 'Unpaid Leave';}
                            else if(data == 5){var leave = 'Others';}
                            return leave;
                        }
                        return data;
                    }
                }
            ]
        }) 
    })

</script>
@endpush