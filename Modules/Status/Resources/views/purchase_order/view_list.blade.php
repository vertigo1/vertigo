@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2 font-13" style="text-align:center;background-color:#868686;padding-left:70px;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:4%;font-size:40px;">CHECK REGISTERED PURCHASE ORDER STATUS</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="row">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <a href="{{route('status')}}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>JOB NUMBER</th>
                                        <th>CLIENT NAME</th>
                                        <th>CLIENT REF NUM</th>
                                        <th>PIC</th>
                                        <th>PO DATE</th>
                                        <th>PO VALUE</th>
                                        <th>PO STATUS</th>
                                        <th>JOB TITLE</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    
    $(() => {
        let table = $('#table');

        const formatter = new Intl.NumberFormat('ms-MY', {
            style: 'currency',
            currency: 'MYR',
            minimumFractionDigits: 2
        });

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        table.DataTable({
            "dom" : '<"wrapper"fltip>',
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('purchase_order.view') }}",
                type: 'GET',
            },
            columns: [
                {data: 'job_number'},
                {data: 'client_name'},
                {data: 'client_ref_no'},
                {data: 'staff_id'},
                {
                    data: 'po_date',
                    name: 'po_date',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {
                    data: 'po_value', 
                    name: 'po_value',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var amount = parseFloat(val);
                        var html = '<p>'+formatter.format(amount)+'</p>';
                        return html;
                    }
                },
                {data: 'po_status'},
                {data: 'job_title'},
                {data: 'action', name: 'action', searchable: false, orderable: false},
            ],
            columnDefs: [
                {
                    "className": "text-center",
                    "data":'po_status',
                    "targets":[6],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == 0 ? '<span class="text-success">CLOSED</span>' : '<span class="text-warning">ONGOING</span>';
                        }
                        return data;
                    }
                },
            ]
        }) 

        // $(".pagination").css({ 'justify-content': "center" }); 
        // $("#table_paginate").css({"display": "inline-block", "text-align":"center"});
    })


</script>
@endpush