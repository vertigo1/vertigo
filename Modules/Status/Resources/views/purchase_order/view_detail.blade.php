@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.1vw;
        color: black;
        font-weight: 300;
    }

    #textarea, .textarea {
    -moz-appearance: textfield-multiline;
    -webkit-appearance: textarea;
    border: 1px solid #ced4da;
    min-height: 38px;
    padding: 2px;
    width: 100%;
    background: #e9ecef;
    font-weight: 300;
    padding: .375rem .75rem;
}
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12" id="printSection">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2 font-13" style="text-align:center;background-color:#868686;padding-left:70px;">
                                <img width="100%" height="100%" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:4%;font-size:40px;">PURCHASE ORDER REGISTRATION FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:30%;margin-top:30%;font-size:20px;">ID {{$data->id}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="purchaseOrder" method="POST" action="{{route('purchase_order.post')}}">
                        @csrf
                            <div class="form-group row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <label for="" class="col-md-2 font-13">JOB NUMBER</label>
                                        <div class="form-group col-md-3">
                                            <input class="form-control input-group-lg font-13" style="opacity:1;" autocomplete="off" type="text"  name="job_number" value="{{$data->job_number}}" readonly>
                                        </div>
                                        <label for="" class="col-md-2 font-13">CLIENT REF NUMBER</label>
                                        <div class="form-group col-md-5">
                                            <input class="form-control input-group-lg font-13" style="opacity:1;" autocomplete="off" type="text"  name="client_ref_no" value="{{$data->client_ref_no}}" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label for="" class="col-md-2 font-13">CLIENT NAME</label>
                                        <div class="form-group col-md-3">
                                            <input class="form-control input-group-lg font-13" style="opacity:1;" autocomplete="off" type="text"  name="client_name" value="{{$data->client_name}}" readonly>
                                        </div>

                                        <label for="" class="col-md-2 font-13">JOB TITLE</label>
                                        <div class="form-group col-md-5">
                                            <div id="textarea" name="job_title" style="opacity:1;" class="font-13">{{$data->job_title}}</div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label for="" class="col-md-2 font-13">PO DATE</label>
                                        <div class="form-group col-md-3">
                                            <input class="form-control input-group-lg font-13" style="opacity:1;" autocomplete="off" type="date"  name="po_date" value="{{$data->po_date}}" readonly>
                                        </div>

                                        <label for="" class="col-md-7"></label>
                                    </div>

                                    <div class="row">
                                        <label for="" class="col-md-2 font-13">PO VALUE</label>
                                        <div class="form-group col-md-3">
                                            <input class="form-control input-group-lg font-13" style="opacity:1;" autocomplete="off" type="text"  name="po_value" value="{{$data->po_value}}" readonly>
                                        </div>

                                        <label for="" class="col-md-2 font-13">PROJECT OWNER</label>
                                        <div class="form-group col-md-5">
                                            <input class="form-control input-group-lg font-13" style="opacity:1;" autocomplete="off" type="text"  name="project_owner" value="{{$data->staff_id}}" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-row" style="justify-content:flex-end;grid-gap:20px;">
                                        <div class="col-md-8" style="border:1px solid black;padding:15px">
                                            <p style="font-weight:bold;width:250px;margin-top:-30px;margin-left:10px;background:white;text-align:center">TYPE OF PURCHASE ORDER</p>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="type_po" id="gridRadios1" value="1" {{$data->type_po == 1 ? 'checked' : 'disabled'}}>
                                                        <label class="form-check-label" style="font-size:1vw;white-space:nowrap" for="gridRadios1">SERVICES</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="type_po" id="gridRadios2" value="2" {{$data->type_po == 2 ? 'checked' : 'disabled'}}>
                                                        <label class="form-check-label" style="font-size:1vw;white-space:nowrap" for="gridRadios2">TRADING</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="type_po" id="gridRadios3" value="3" {{$data->type_po == 3 ? 'checked' : 'disabled'}}>
                                                        <label class="form-check-label" style="font-size:1vw;white-space:nowrap" for="gridRadios3">RENTAL</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="type_po" id="gridRadios4" value="4" {{$data->type_po == 4 ? 'checked' : 'disabled'}}>
                                                        <label class="form-check-label" style="font-size:1vw;white-space:nowrap" for="gridRadios4">OPEX</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-1 hilang"></div>
                                <div class="col-md-6" id="incoming">
                                    <h2>INCOMING CLIENT PAYMENT</h2>
                                    <div class="table-responsive" style="overflow-y: hidden;overflow-x: hidden;">
                                        <table id="table-client" class="table table-bordered font-13" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th style="vertical-align:middle;text-align:center;width:20%"></th>
                                                    <th style="vertical-align:middle;text-align:center;width:40%">EXPECTED DATE</th>
                                                    <th style="vertical-align:middle;text-align:center;width:40%">AMOUNT (RM)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data->incoming as $key => $item)
                                                    <tr>
                                                        <td style="text-align:center;">STAGE {{$key+1}} </td>
                                                        <td><input class="form-control input-group-lg font-13" type="hidden" name="incoming_expected_date[]" value="{{$item->expected_date}}" readonly></td>
                                                        <td><div id="amount" class="textarea font-13" style="text-align:center;">{{$item->amount}}</div></td>
                                                    </tr>
                                                @endforeach
                                                
                                            <tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-1 hilang"></div>
                                <div class="col-md-4 hilang">
                                    <div class="card" style="border:2px solid lightgrey;">
                                        <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                                            <div class="btn" style="color:white;font-size:30px;margin-top:2%">IMPORTANT</div>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group row font-13">
                                                <ul>
                                                    <li>
                                                        <p>Ensure the form has been Saved before leaving this page.</p>
                                                    </li>
                                                <ul>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <button type="button" id="printBtn" class="col-md-12 btn btn-info print" href="javascript:;" style="font-size:1.1vw;">Print</button> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <a href="{{route('purchase_order.view')}}" class="col-md-12 btn btn-outline-danger" style="font-size:1.1vw;">Back</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row remove-justify" style="justify-content:center">
                                <div class="form-group col-md-12" id="outgoing">
                                    <h2>OUTGOING SUBCONT PAYMENT</h2>
                                    <div class="table-responsive" style="overflow-y: hidden;overflow-x: hidden;">
                                        <table id="table-subcont" class="table table-bordered font-13" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th style="vertical-align:middle;text-align:center;width:5%"></th>
                                                    <th style="vertical-align:middle;text-align:center;width:25%">SUBCONT NAME</th>
                                                    <th style="vertical-align:middle;text-align:center;width:40%">DESCRIPTION</th>
                                                    <th style="vertical-align:middle;text-align:center;width:15%">EXPECTED DATE</th>
                                                    <th style="vertical-align:middle;text-align:center;width:15%">AMOUNT (RM)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data->outgoing as $key => $item)
                                                <tr>
                                                    <td style="text-align:center;">{{$key+1}}</td>
                                                    <td><div id="subcont_name" class="textarea font-13">{{$item->subcont_name}}</div></td>
                                                    <td><div id="description" class="textarea font-13">{{$item->description}}</div></td>
                                                    <td><input class="form-control input-group-lg font-13" type="hidden" name="expected_date[]" value="{{$item->expected_date}}" readonly></td>
                                                    <td><div id="amount" class="textarea font-13" style="text-align:center;">{{$item->amount}}</div></td>
                                                </tr>
                                                @endforeach
                                            <tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4" style="display:flex;white-space:nowrap">
                                        <label for="" class="col-md-5 font-13">TOTAL (RM)</label>
                                        <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  style="text-align:center;opacity:1;" value="{{$data->total_outgoing}}" readonly>
                                    </div>
                                </div>
                            <div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

    var count_client = $('#table-client tbody tr').length;
    if(count_client<2){
        $('#table-client > tbody:last-child').append('<tr><td></td><td><div class="textarea"></div></td><td><div class="textarea"></div></td></tr>');
        $('#table-client > tbody:last-child').append('<tr><td></td><td><div class="textarea"></div></td><td><div class="textarea"></div></td></tr>');
    }

    var count_cont = $('#table-subcont tbody tr').length;
    if(count_cont<2){
        $('#table-subcont > tbody:last-child').append('<tr><td></td><td><div class="textarea"></div></td><td><div class="textarea"></div></td><td><div class="textarea"></div></td><td><div class="textarea"></div></td></tr>');
        $('#table-subcont > tbody:last-child').append('<tr><td></td><td><div class="textarea"></div></td><td><div class="textarea"></div></td><td><div class="textarea"></div></td><td><div class="textarea"></div></td></tr>');
    }

    $('input[name ="expected_date[]"], input[name ="incoming_expected_date[]"]').each(function(key, value){
        var dateObject = $(this).val(); 
        var date = new Date(dateObject)
        var div = $("<div>").appendTo($(this).parent());
        var hh = ("0" + date.getDate()).slice(-2)
        var mm = ("0" + (date.getMonth() + 1)).slice(-2)
        div.css({'text-align': 'center', 'min-height': '0%'}).addClass("textarea font-13").text(`${hh}-${mm}-${date.getFullYear()}`);
        $(this).hide();
    }) 

    $('.print').on('click', function(){

        $( "#printSection" ).removeClass('col-md-12');
        $( "#printSection" ).addClass('col-md-8');
        // $( "#outgoing" ).removeClass('col-md-12');
        // $( "#outgoing" ).addClass('col-md-8');
        $( "#incoming" ).removeClass('col-md-6');
        $( "#incoming" ).addClass('col-md-12');
        $(".hilang").hide()
        $(".remove-justify").css("justify-content","")

        window.scrollTo(0,0);

        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            x: 0,
            y: 0,
            width: 1900,
            height: 1800
        }; 

        var chart = document.querySelector("#printSection");
        var width = chart.offsetWidth;
        var height = chart.offsetHeight;
        html2canvas(chart, options).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF();

            pdf.addImage(dataURL, 'JPEG', 5, 5, width/3.3, height/6.5, '', 'SLOW');

            pdf.save("Purchase_Order");
        });

        $(".hilang").show()
        $(".remove-justify").css("justify-content","center")
        $( "#incoming" ).removeClass('col-md-12');
        $( "#incoming" ).addClass('col-md-6');
        $( "#printSection" ).removeClass('col-md-8');
        $( "#printSection" ).addClass('col-md-12');
    });
</script>
@endpush