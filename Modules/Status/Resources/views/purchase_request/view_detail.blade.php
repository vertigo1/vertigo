@extends('form::layouts.master')
@push('styles')
<style>
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }

    /* LAPTOP */
    @media screen 
    and (min-device-width: 1000px) 
    and (max-device-width: 1600px) 
    and (-webkit-min-device-pixel-ratio: 1) { 
        #laptop { display: block; }   /* hide it elsewhere */
        #monitor { display: none; }   /* hide it elsewhere */
        .label-req { margin-bottom: 60%; }
        .label-cheq { margin-bottom: 45%; }
        .label-subm { margin-bottom: 40%; }
    }

    /* MONITOR */
    @media screen 
    and (min-device-width: 1600px) 
    and (max-device-width: 2000px) 
    and (-webkit-min-device-pixel-ratio: 1) { 
        #laptop { display: none; }   /* hide it elsewhere */
        #monitor { display: block; }   /* hide it elsewhere */
        .label-req { margin-bottom: 40%; }
        .label-cheq { margin-bottom: 30%; }
        .label-subm { margin-bottom: 25%; }
    }

    input,label{
        font-weight:bold;
        color:black;
    }

    .font-13 {
        font-size: 1.1vw;
        color: black;
    }

    #textarea {
        -moz-appearance: textfield-multiline;
        -webkit-appearance: textarea;
        border: 1px solid #ced4da;
        min-height: 100%;
        padding: 2px;
        width: 100%;
        background: white;
        font-weight: 400;
        padding: .375rem .75rem;
    }

</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9" id="printSection">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2 font-13" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{$imgbase64}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">PURCHASE REQUEST FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-3" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                            <div class="col-md-1" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;transform:rotate(270deg);text-align:left;margin-left:1%;margin-top:40%;font-size:20px;">ID {{$data->id}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <form id="purchaseReq" method="POST" action="{{route('purchase_request.post')}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="col-md-3 font-13">STAFF ID</label>
                                    <div class="form-group col-md-9">
                                        <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="staff_id" value="{{$data->staff_id}}" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="" class="col-md-3 font-13">SUPPLIER</label>
                                    <div class="form-group col-md-9">
                                        <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="supplier" value="{{$data->supplier}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="col-md-3 font-13">JOB TITLE</label>
                                    <div class="form-group col-md-9" style="min-height:110px;">
                                        <div id="textarea" name="job_title" id="job_title" class="font-13">{{$job_title}}</div>
                                    </div>
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="col-md-3 font-13">DATE</label>
                                    <div class="form-group col-md-9">
                                        <input class="form-control input-group-lg font-13" autocomplete="off" type="date"  name="date" value="{{$data->date}}" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                <label for="" class="col-md-3 font-13">AMOUNT</label>
                                    <div class="form-group col-md-9">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">RM</div>
                                            <input class="form-control input-group-lg font-13" autocomplete="off" type="text"  name="amount" value="{{$data->amount}}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="col-md-3 font-13">PURPOSE DESCRIPTION</label>
                                    <div class="form-group col-md-9" style="min-height:110px;">
                                        <div id="textarea" name="purpose" id="purpose" class="font-13">{{$data->description}}</div>
                                    </div>
                                </div>
                            </div>     
                        </div>
                        <br/>
                        <div class="row" style="justify-content:center;grid-gap:20px;">
                            <div class="col-md-5" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:100px;margin-top:-30px;margin-left:10px;background:white;text-align:center">Category</p>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="1" {{$data->category == 1 ? 'checked' : 'disabled'}}>
                                            <label class="form-check-label" for="gridRadios1">
                                                Project Expenses
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="2"{{$data->category == 2 ? 'checked' : 'disabled'}}>
                                            <label class="form-check-label" for="gridRadios2">
                                                Workshop Expenses
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="3" {{$data->category == 3 ? 'checked' : 'disabled'}}>
                                            <label class="form-check-label" for="gridRadios3">
                                                Office Expenses
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios4" value="4" {{$data->category == 4 ? 'checked' : 'disabled'}}>
                                            <label class="form-check-label" for="gridRadios4">
                                                Investment
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios5" value="5" {{$data->category == 5 ? 'checked' : 'disabled'}}>
                                            <label class="form-check-label" for="gridRadios5">
                                                Transport
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios6" value="6" {{$data->category == 6 ? 'checked' : 'disabled'}}>
                                            <label class="form-check-label" for="gridRadios6">
                                                Monthly Commitment
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="" style="color:red" >Enter a Job Number</label>
                                        <input class="form-control input-group-lg font-13" autocomplete="off" type="text" name="" value="{{$data->job_number}}" readonly>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-5" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:125px;margin-top:-30px;margin-left:10px;background:white;text-align:center">Attachment</p>
                                <p style="color:red">Please attach a complete supporting documents during approval submission from superior</p>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck1" name="gridCheck1">
                                            <label class="form-check-label" for="gridCheck1">
                                                Client's PO
                                            </label>    
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck2" name="gridCheck2">
                                            <label class="form-check-label" for="gridCheck2">
                                                Our Quotation
                                            </label>    
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck3" name="gridCheck3">
                                            <label class="form-check-label" for="gridCheck3">
                                                Supplier Quotation
                                            </label>    
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck4" name="gridCheck4">
                                            <label class="form-check-label" for="gridCheck4">
                                                Out Outgoing PO
                                            </label>    
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck5" name="gridCheck5">
                                            <label class="form-check-label" for="gridCheck5">
                                                Costing Sheet
                                            </label>    
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck6" name="gridCheck6">
                                            <label class="form-check-label" for="gridCheck6">
                                                Timesheet
                                            </label>    
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck7" name="gridCheck7">
                                            <label class="form-check-label" for="gridCheck7">
                                                Bill / Receipt
                                            </label>    
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck8" name="gridCheck8">
                                            <label class="form-check-label" for="gridCheck8">
                                                OT Calculation Form
                                            </label>    
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck9" name="gridCheck9">
                                            <label class="form-check-label" for="gridCheck9">
                                                Mileage Form
                                            </label>    
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck10" name="gridCheck10">
                                            <label class="form-check-label" for="gridCheck10">
                                                Out-Station Form 
                                            </label>    
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck11" name="gridCheck11">
                                            <label class="form-check-label" for="gridCheck11">
                                                Others - any related docs
                                            </label>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="header">
                            <p class="col-md-12" style="text-align:center;font-weight: bold;">APPROVAL SECTION</p>
                        </div>

                        <div class="form-group row">
                            <p class="col-md-12" style="text-align:center;color:red">Requestor to get approval from dedicated Superior</p>
                        </div>

                        <div class="form-group row" style="justify-content:center;grid-gap:5px">
                            <div class="col-md-3">
                            </div>
                            
                            <div class="col-md-4">
                            <p  style="text-align:center;color:black">TimeFrame : 3 working days</p>
                            </div>
                            
                            <div class="col-md-4">
                            <p  style="text-align:center;color:black">TimeFrame : 3 working days</p>
                            </div>
                        </div>

                        <div class="row" style="justify-content:center;grid-gap:5px">
                            <div class="form-group col-md-3" style="border:1px solid black;height:275px;">
                                <label for="" class="label-req">REQUESTOR</label>
                                <p id="laptop">-----------------------------------------</p>
                                <p id="laptop">DATE ---------------------------------</p>
                                <p id="monitor">-----------------------------------------------------</p>
                                <p id="monitor">DATE ---------------------------------------------</p>
                            </div>
                            
                            <div class="form-group col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" class="label-cheq">CHECKED BY</label>
                                <p id="laptop">---------------------------------------------------------</p>
                                <p id="laptop">DATE ------------------------------------------------</p>
                                <p id="monitor">-------------------------------------------------------------------------------</p>
                                <p id="monitor">DATE -----------------------------------------------------------------------</p>
                            </div>
                            
                            <div class="form-group col-md-4" style="border:1px solid black;height:275px;">
                                <label for="" class="label-cheq">APPROVED BY</label>
                                <p id="laptop">---------------------------------------------------------</p>
                                <p id="laptop">DATE ------------------------------------------------</p>
                                <p id="monitor">-------------------------------------------------------------------------------</p>
                                <p id="monitor">DATE -----------------------------------------------------------------------</p>
                            </div>
                        </div>

                        <br>

                        <div class="header">
                            <p class="col-md-12" style="text-align:center;font-weight: bold;">SUBMISSION SECTION</p>
                        </div>

                        <div class="form-group row">
                            <p class="col-md-12" style="text-align:center;color:red">Requestor to submit complete form with related documents to Finance</p>
                        </div>

                        <div class="form-group row">
                            <p class="col-md-12" style="text-align:center;color:black">Payment TimeFrame : 7 working days</p>
                        </div>

                        <div class="row" style="justify-content:center;grid-gap:30px">
                            <div class="form-group col-md-4" style="border:1px solid black;height: 250px;">
                                <label for="" class="label-subm">RECEIVED BY (FINANCE)</label>
                                <p id="laptop">----------------------------------------------------------</p>
                                <p id="laptop">DATE --------------------------------------------------</p>
                                <p id="monitor">----------------------------------------------------------------------------</p>
                                <p id="monitor">DATE --------------------------------------------------------------------</p>
                            </div>
                            
                            <div class="form-group col-md-4" style="border:1px solid black;height: 250px;">
                                <label for="" class="label-subm">REQUESTOR</label>
                                <p id="laptop">----------------------------------------------------------</p>
                                <p id="laptop">DATE --------------------------------------------------</p>
                                <p id="monitor">----------------------------------------------------------------------------</p>
                                <p id="monitor">DATE --------------------------------------------------------------------</p>
                            </div>
                        </div>

                        <div class="row">
                            <p class="col-md-12" style="text-align:center;color:red">Latest submission claim MUST be 2 months after work / acivities completion</p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="text-align:center;background-color:salmon;pointer-events:none;width:100%;height:6rem;">
                        <div class="btn" style="color:white;font-size:30px;margin-top:2%">INSTRUCTIONS</div>
                    </div>
                    <div class="card-body">
                        <div class="form-group row" style="color:black;font-size:1.1vw;">
                            <ul>
                                <li>
                                    <p>Fill all the required spaces.</p>
                                </li>
                                <li>
                                    <p>Save form before proceed to Print for audit purposes.</p>
                                </li>
                                <li>
                                    <p>Print form before proceed with Superior approval.</p>
                                </li>
                                <li>
                                    <p>Ensure the form has been Saved before leaving this page.</p>
                                </li>
                            <ul>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <button type="button" id="printBtn" class="col-md-12 btn btn-info print" href="javascript:;" style="font-size:1.1vw">Print</button> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="{{route('purchase_request.view')}}" class="col-md-12 btn btn-outline-danger" style="font-size:1.1vw">Back</a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('.print').on('click', function(){

        window.scrollTo(0,0);

        let options = {
            async: true,
            logging: true, letterRendering: 1, // allowTaint: false useCORS: true
            foreignObjectRendering: true,
            scrollX: 0,
            scrollY: 0,
            x: 0,
            y: 0,
            // width: 1880,
            // height: 1900
            windowWidth: window.innerWidth,
            windowHeight: window.innerHeight,
            scale: 2,
        };  

        var chart = document.querySelector("#printSection");
        var width = window.screen.width;
        var height = window.screen.height;
        html2canvas(chart, options).then(canvas => {
            var dataURL = canvas.toDataURL('image/jpeg',1.0);

            var pdf = new jsPDF({compress:true});

            var pageWidth = pdf.internal.pageSize.width;
            var pageHeight = pdf.internal.pageSize.height;

            var widthRatio = pageWidth / canvas.width;
            var heightRatio = pageHeight / canvas.height;
            var ratio = widthRatio > heightRatio ? heightRatio : widthRatio;

            if(canvas.width <= 1740){
                var canvasWidth = canvas.width * (ratio + 0.050);
                var canvasHeight = canvas.height * (ratio);
                pdf.addImage(dataURL, 'JPEG', 0, 2, canvasWidth, canvasHeight, undefined, 'FAST');
            }else if(canvas.width <= 2184){
                var canvasWidth = canvas.width * (ratio + 0.019);
                var canvasHeight = canvas.height * (ratio);
                pdf.addImage(dataURL, 'JPEG', 0, 2, canvasWidth, canvasHeight, undefined, 'FAST');
            }else if(canvas.width <= 2400){
                var canvasWidth = canvas.width * (ratio + 0.017);
                var canvasHeight = canvas.height * (ratio);
                pdf.addImage(dataURL, 'JPEG', 0, 2, canvasWidth, canvasHeight, undefined, 'FAST');
            }else{
                var canvasWidth = canvas.width * (ratio);
                var canvasHeight = canvas.height * (ratio);
                pdf.addImage(dataURL, 'JPEG', 0, 2, canvasWidth, canvasHeight, undefined, 'FAST');
            } }

            //pdf.addImage(dataURL, 'JPEG', 5, 5, width/4, height/2.8, '', 'SLOW');

            pdf.save("Purchase_Request");
        });

    });
</script>
@endpush