@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2 font-13" style="text-align:center;background-color:#868686;padding-left:70px;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:4%;font-size:40px;">PURCHASE REQUEST APPLICATION STATUS</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="row">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <a href="{{route('status')}}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>BANK SLIP</th>
                                        <th>DATE</th>
                                        <th>REQUESTOR</th>
                                        <th>JOB NUMBER</th>
                                        <th>SUPPLIER</th>
                                        <th>AMOUNT</th>
                                        <th>PURPOSE DESCRIPTION</th>
                                        <th>PAID</th>
                                        <th>PAYMENT DATE</th>
                                        <th>REMARKS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    
    $(() => {
        let table = $('#table');

        const formatter = new Intl.NumberFormat('ms-MY', {
            style: 'currency',
            currency: 'MYR',
            minimumFractionDigits: 2
        });

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        table.DataTable({
        "dom" : '<"wrapper"fltip>',
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('purchase_request.view') }}",
            type: 'GET',
        },
        columns: [
            {data: 'bank_slip'},
            {
                data: 'date',
                render: function(data, type, full){
                    var date = new Date(data);
                    var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                    return htmlRender;
                }
            },
            {data: 'staff_id'},
            {data: 'job_number'},
            {data: 'supplier'},
            {
                data: 'amount', 
                render: function(data, type, full){
                    var val = data.replace(/[^0-9\.]/g,'');
                    var amount = parseFloat(val);
                    var html = '<p>'+formatter.format(amount)+'</p>';
                    return html;
                }
            },
            {data: 'description'},
            {data: 'payment_status'},
            {
                data: 'payment_date',
                render: function(data, type, full){
                    if(data == '0000-00-00'){
                        var htmlRender = '<p> - </p>';
                        return htmlRender;
                    }else{
                        var payment_date = new Date(data);
                        var htmlRender = '<p>'+payment_date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                }
            },
            {
                data: 'payment_remarks',
                render: function(data, type, full){
                    if(data == ''){
                        var htmlRender = '<p> - </p>';
                        return htmlRender;
                    }else{
                        var htmlRender = '<p>'+data+'</p>';
                        return htmlRender;
                    }
                }
            },
            {data: 'action', name: 'action', searchable: false, orderable: false}
        ],
        columnDefs: [
            {"className": "text-center",
            "data":'payment_status',
            "targets":[7],
            "render": function (data, type, row, meta) {
                    if (type == 'display') {
                        if(data == 0){
                            return '<span class="text-danger">No</span>';
                        }else if(data == 1){
                            return '<span class="text-success">Yes</span>';
                        }else if(data == 2){
                            return '<span class="text-danger">Reject</span>';
                        }
                    }
                    return data;
                }
            },
            {"className": "text-center",
            "data":'bank_slip',
            "targets":[0],
            "render": function (data, type, row, meta) {
                    if (type == 'display') {
                        return data == 0 ? '-' : `<a target="_blank" href="{{asset('storage/bank_slip/`+data+`')}}"><i class="fas fa-file fa-2x"></i></a>`;
                    }
                    return data;
                }
            },
            {"className": "text-center", "targets":[9]}

        ]
    }) 
    })

</script>
@endpush