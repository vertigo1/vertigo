<?php

namespace Modules\Status\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use Modules\Form\Entities\PurchaseRequest;
use Modules\Form\Entities\LeaveApplication;
use Modules\Form\Entities\InLiue;
use Modules\Form\Entities\Work;
use Modules\Form\Entities\PurchaseOrder;
use DB,DataTables,Auth;
use Carbon\Carbon;


class StatusController extends Controller
{
    public function index(){
        return view('status::index');
    }

    public function view_purchase_request(){
        if(request()->ajax()) {
            $data = PurchaseRequest::where('staff_id','=',Auth::user()->staff_id)->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $view = route('request.id',$row->id);
                        $btn = '<a href="'. $view .'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-primary btn-sm btn-icon mr-2"><i class="fa fa-eye"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('status::purchase_request.view_list');
    }

    public function view_request_ID($id) {
        $data = PurchaseRequest::find($id);

        if($data->job_number == 13000) { $job_title = "Workshop Expenses"; }
        else if($data->job_number == 14000) { $job_title = "Office Expenses"; }
        else if($data->job_number == 15000) { $job_title = "Investment"; }
        else if($data->job_number == 16000) { $job_title = "Transport"; }
        else if($data->job_number == 17000) { $job_title = "Monthly Commitment"; }
        else {

        $job = PurchaseOrder::where('job_number',$data->job_number)->first();
        $job_title = $job->job_title;

        }
        return view('status::purchase_request.view_detail',compact('data','job_title'));

    }

    public function view_leave_application(){
        $total_annual = LeaveApplication::where('staff_id','=',Auth::user()->staff_id)->where('type_leave',0)->get()->sum('total_day');
        $approved_annual = LeaveApplication::where('status',1)->where('staff_id','=',Auth::user()->staff_id)->where('type_leave',0)->get()->sum('total_day');
        $balanced_annual = User::where('staff_id','=',Auth::user()->staff_id)->get('annual_leave')->pluck('annual_leave')->first();

        $total_liue = LeaveApplication::where('staff_id','=',Auth::user()->staff_id)->where('type_leave',1)->get()->sum('total_day');
        $approved_liue = LeaveApplication::where('status',1)->where('staff_id','=',Auth::user()->staff_id)->where('type_leave',1)->get()->sum('total_day');
        $balanced_liue = User::where('staff_id','=',Auth::user()->staff_id)->get('liue_leave')->pluck('liue_leave')->first();

        $total_medical = LeaveApplication::where('staff_id','=',Auth::user()->staff_id)->where('type_leave',2)->get()->sum('total_day');
        $approved_medical = LeaveApplication::where('status',1)->where('staff_id','=',Auth::user()->staff_id)->where('type_leave',2)->get()->sum('total_day');
        $balanced_medical = User::where('staff_id','=',Auth::user()->staff_id)->get('medical_leave')->pluck('medical_leave')->first();

        if(request()->ajax()) {
            $data = LeaveApplication::where('staff_id','=',Auth::user()->staff_id)->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $view = route('leave.id',$row->id);
                        $btn = '<a href="'. $view .'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-primary btn-sm btn-icon mr-2"><i class="fa fa-eye"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('status::leave_application.view_list',compact('total_annual', 'approved_annual', 'balanced_annual','total_liue', 'approved_liue', 'balanced_liue','total_medical', 'approved_medical', 'balanced_medical'));
    }

    public function view_leave_ID($id) {
        $data = LeaveApplication::find($id);
        $staff = User::where('staff_id',$data->staff_id)->get()->first();
        
        return view('status::leave_application.view_detail',compact('data','staff'));
    }


    public function view_in_liue(){

        $works = DB::table('work')->join('off_in_liue', 'off_in_liue.form_id', '=', 'work.form_id')
                                            ->where('off_in_liue.staff_id', '=', Auth::user()->staff_id)
                                            ->where('work.status',1)
                                            ->get();
    
        $sumSeconds = 0;
        foreach($works as $work) {
            $explodedTime = explode(':', $work->total_hour);
            $seconds = $explodedTime[0]*3600+$explodedTime[1]*60;
            $sumSeconds += $seconds;
        }
        $hours = floor($sumSeconds/3600);
        $minutes = floor(($sumSeconds % 3600)/60);

        $total = sprintf('%02d:%02d', $hours,$minutes );

        if(request()->ajax()) {
            $data = DB::table('work')->join('off_in_liue', 'off_in_liue.form_id', '=', 'work.form_id')
                                        ->where('off_in_liue.staff_id', '=', Auth::user()->staff_id)
                                        ->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $view = route('liue.id',$row->form_id);
                        $btn = '<a href="'. $view .'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-primary btn-sm btn-icon mr-2"><i class="fa fa-eye"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('status::in_liue.view_list',compact('total'));
    }

    public function view_in_liue_ID($id) {
        // $data = Work::with('inlieu')->where('inliue.staff_id','=',Auth::user()->staff_id)->where('form_id', $id)->get();

        $data = Work::with('inlieu')->where('form_id', $id)->get();

        if($data) {
            if($data[0]->inlieu->job_number == 13000) { $job_title = "Workshop Expenses"; }
            else if($data[0]->inlieu->job_number == 14000) { $job_title = "Office Expenses"; }
            else if($data[0]->inlieu->job_number == 15000) { $job_title = "Investment"; }
            else if($data[0]->inlieu->job_number == 16000) { $job_title = "Transport"; }
            else if($data[0]->inlieu->job_number == 17000) { $job_title = "Monthly Commitment"; }
            else {
                $job = PurchaseOrder::where('job_number',$data[0]->inlieu->job_number)->first();
                $job_title = $job->job_title;
            }
        }
        return view('status::in_liue.view_detail',compact('data','job_title'));
    }

    public function view_purchase_order(){
        if(request()->ajax()) {
            $data = PurchaseOrder::where('staff_id',Auth::user()->staff_id)->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $view = route('order.id',$row->form_id);
                        $btn = '<a href="'. $view .'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-primary btn-sm btn-icon mr-2"><i class="fa fa-eye"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('status::purchase_order.view_list');
    }

    public function view_order_ID($id) {

        $data = PurchaseOrder::with('incoming','outgoing')->where('form_id',$id)->get()->first();
        // dd($data);
        // dd($data->incoming[0]->expected_date);        
        return view('status::purchase_order.view_detail',compact('data'));
    }
}
