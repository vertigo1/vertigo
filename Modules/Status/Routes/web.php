<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('status')->group(function() {
    Route::middleware(['auth'])->group(function () {
        Route::get('/index', 'StatusController@index')->name('status');

        //Purchase Request
        Route::get('/purchase_request/view', 'StatusController@view_purchase_request')->name('purchase_request.view');
        Route::get('/request_view/{id}', 'StatusController@view_request_ID')->name('request.id');

        // ------------------------------------------------------ //

        //Leave Application
        Route::get('/leave_application/view', 'StatusController@view_leave_application')->name('leave_application.view');
        Route::get('/leave_view/{id}', 'StatusController@view_leave_ID')->name('leave.id');
        // ------------------------------------------------------ //

        //Off In Liue
        Route::get('/in_liue/view', 'StatusController@view_in_liue')->name('in_liue.view');
        Route::get('/in_liue_view/{id}', 'StatusController@view_in_liue_ID')->name('liue.id');
        // ------------------------------------------------------ //

        //Purchase Order
        Route::get('/purchase_order/view', 'StatusController@view_purchase_order')->name('purchase_order.view');
        Route::get('/order_view/{id}', 'StatusController@view_order_ID')->name('order.id');
        // ------------------------------------------------------ //
    });
});
