<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('update')->group(function() {
    Route::middleware(['auth'])->group(function () {
        Route::get('/index', 'UpdateController@index')->name('update');

        //Purchase Order
        Route::get('/purchase_order/update', 'UpdateController@update_purchase_order')->name('purchase_order.update');
        Route::post('/order_save/{id}', 'UpdateController@save_order')->name('save.order');
        // ------------------------------------------------------ //

        //Purchase Request
        Route::get('/purchase_request/update', 'UpdateController@update_purchase_request')->name('purchase_request.update');
        Route::post('/request_save/{id}', 'UpdateController@save_request')->name('save.request');
        // ------------------------------------------------------ //

        //Leave Application
        Route::get('/leave_application/update', 'UpdateController@update_leave_application')->name('leave_application.update');
        Route::post('/leave_application_save/{id}', 'UpdateController@save_leave_application')->name('save.leave_application');
        // ------------------------------------------------------ //

        //Off In Liue
        Route::get('/in_liue/update', 'UpdateController@update_in_liue')->name('in_liue.update');
        Route::post('/in_liue_save/{id}', 'UpdateController@save_in_liue')->name('save.in_liue');
        // ------------------------------------------------------ //

        //Staff Management
        Route::get('/staff_management/update', 'UpdateController@update_staff_management')->name('staff_management.update');
        Route::post('/staff_management_save/{id}', 'UpdateController@save_staff_management')->name('save.staff_management');
        Route::post('/staff_management_create/', 'UpdateController@create_staff_management')->name('create.staff_management');
        Route::get('/staff_management_delete/{id}', 'UpdateController@delete_staff_management')->name('delete.staff_management');
        // ------------------------------------------------------ //
    });
});
