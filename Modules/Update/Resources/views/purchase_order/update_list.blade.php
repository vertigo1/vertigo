@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">PURCHASE ORDER UPDATE FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="form-group">
                            <div class="col-md-2">
                                <a href="{{route('update')}}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>JOB NUMBER</th>
                                        <th>CLIENT NAME</th>
                                        <th>CLIENT REF NUM</th>
                                        <th>PIC</th>
                                        <th>PO DATE</th>
                                        <th>PO VALUE</th>
                                        <th>PO STATUS</th>
                                        <th>JOB TITLE</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="orderModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" style="background-color:salmon">
        <h5 class="modal-title" style="color:white">Editing Selected Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="purchaseOrd" method="POST">
        @csrf
        <div class="modal-body" style="margin-top:2%">
            <div class="form-group">
                <div class="form-group row">
                    <div class="col-md-8">
                        <div class="row">
                            <label for="" class="col-md-2">JOB NUMBER</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg" autocomplete="off" type="text" id="job_number" name="job_number">
                            </div>
                            <label for="" class="col-md-2">CLIENT REF NUMBER</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg" autocomplete="off" type="text" id="client_ref_no" name="client_ref_no">
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2">CLIENT NAME</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg" autocomplete="off" type="text" id="client_name" name="client_name">
                            </div>

                            <label for="" class="col-md-2">JOB TITLE</label>
                            <div class="form-group col-md-4">
                                <textarea class="form-control input-group-lg" autocomplete="off" type="text" id="job_title" rows="3" name="job_title"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2">PO DATE</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg" autocomplete="off" type="date" id="po_date" name="po_date">
                            </div>

                            <label for="" class="col-md-7"></label>
                        </div>

                        <div class="row">
                            <label for="" class="col-md-2">PO VALUE</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg" autocomplete="off" type="text" id="po_value" name="po_value">
                            </div>

                            <label for="" class="col-md-2">PIC</label>
                            <div class="form-group col-md-4">
                                <input class="form-control input-group-lg" autocomplete="off" type="text" id="pic" name="pic">
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-2">PO STATUS</label>
                            <div class="form-group col-md-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="po_status" id="status1" value="1">
                                    <label class="form-check-label" for="status1">ONGOING</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="po_status" id="status2" value="0">
                                    <label class="form-check-label" for="status2">CLOSED</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-row" style="justify-content:center;grid-gap:20px;">
                            <div class="col-md-8" style="border:1px solid black;padding:15px">
                                <p style="font-weight:bold;width:250px;margin-top:-30px;margin-left:-22px;background:white;text-align:center">TYPE OF PURCHASE ORDER</p>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type_po" id="po1" value="1">
                                            <label class="form-check-label" for="po1">SERVICES</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type_po" id="po2" value="2">
                                            <label class="form-check-label" for="po2">TRADING</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type_po" id="po3" value="3" >
                                            <label class="form-check-label" for="po3">RENTAL</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type_po" id="po4" value="4" >
                                            <label class="form-check-label" for="po4">OPEX</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-group row">
                    <div class="col-md-12" style="text-align:center">
                        <h4>INCOMING CLIENT PAYMENT</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th style="vertical-align:middle;text-align:center;width:20%"></th>
                                        <th style="vertical-align:middle;text-align:center;width:20%">EXPECTED DATE</th>
                                        <th style="vertical-align:middle;text-align:center;width:20%">AMOUNT (RM)</th>
                                        <th style="vertical-align:middle;text-align:center;width:20%">STATUS</th>
                                        <th style="vertical-align:middle;text-align:center;width:20%">REMARKS</th>
                                        <th style="display:none;"></th>
                                    </tr>
                                </thead>
                                <tbody id="table-client">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-group row" style="justify-content:center">
                    <div class="col-md-12" style="text-align:center">
                        <h4>OUTGOING SUBCONT PAYMENT</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th style="vertical-align:middle;text-align:center;width:5%"></th>
                                        <th style="vertical-align:middle;text-align:center;width:30%">SUBCONT NAME</th>
                                        <th style="vertical-align:middle;text-align:center;width:45%">DESCRIPTION</th>
                                        <th style="vertical-align:middle;text-align:center;width:10%">EXPECTED DATE</th>
                                        <th style="vertical-align:middle;text-align:center;width:10%">AMOUNT (RM)</th>
                                        <th style="vertical-align:middle;text-align:center;width:20%">STATUS</th>
                                        <th style="vertical-align:middle;text-align:center;width:20%">REMARKS</th>
                                    </tr>
                                </thead>
                                <tbody id="table-subcont">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="" id="svbtn" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    const formatter = new Intl.NumberFormat('ms-MY', {
        style: 'currency',
        currency: 'MYR',
        minimumFractionDigits: 2
    });

    $(() => {
        validateForm();
        
        let table = $('#table');

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        table.DataTable({
            "dom" : '<"wrapper"fl<t>ip>',
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('purchase_order.update') }}",
                type: 'GET',
            },
            columns: [
                {data: 'job_number'},
                {data: 'client_name'},
                {data: 'client_ref_no'},
                {data: 'staff_id'},
                {
                    data: 'po_date',
                    name: 'po_date',
                    render: function(data, type, full){
                        var po_date = new Date(data);
                        var htmlRender = '<p>'+po_date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {
                    data: 'po_value', 
                    name: 'po_value',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var po_value = parseFloat(val);
                        var html = '<p>'+formatter.format(po_value)+'</p>';
                        return html;
                    }
                },
                {data: 'po_status'},
                {data: 'job_title'},
                {data: 'action', name: 'action', orderable: false},
            ],
            columnDefs: [
                {
                    "className": "text-center",
                    "data":'po_status',
                    "targets":[6],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == 0 ? '<span class="text-success">CLOSED</span>' : '<span class="text-warning">ONGOING</span>';
                        }
                        return data;
                    }
                },
            ]
        }) 

        $("#svbtn").on('click', function() {
            $("#orderModal").hide();
            $('input:checkbox:not(:checked)').prop('checked', true).val(0);
        })
    });

    let validateForm = () => {
        $('#purchaseOrd').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            }).then((result) => {
                                location.reload();
                            })
                        } else {
                            swal("Failed!", resp.msg, "error");
                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })    
            },
        });
    }

    $(document).on('click', '.openModal', function(e) {
        var link = $(this);
        var id  = link.data("id");
        var form_id  = link.data("form_id");
        var job_number = link.data("job_number");
        var client_ref_no = link.data("client_ref_no");
        var client_name = link.data("client_name");
        var job_title = link.data("job_title");
        var po_date = link.data("po_date");
        var po_value = link.data("po_value");
        var pic = link.data("pic");
        var po_status = link.data("po_status");
        var type_po = link.data("type_po");

        var url = '{{route("save.order", ":id")}}';
        var route = url.replace(":id",id);
        $('#purchaseOrd').attr('action', route);

        $("#job_number").val(job_number);
        $("#client_ref_no").val(client_ref_no);
        $("#client_name").val(client_name);
        $("#job_title").val(job_title);
        $("#po_date").val(po_date);
        $("#po_value").val(po_value);
        $("#pic").val(pic);

        if(po_status == 0){
            $("#status1").prop('checked',false);
            $("#status2").prop('checked',true);
        }else if(po_status == 1){
            $("#status1").prop('checked',true);
            $("#status2").prop('checked',false);
        }

        if(type_po == 1){
            $("#po1").prop('checked',true);
            $("#po2").prop('checked',false);
            $("#po3").prop('checked',false);
        }else if(type_po == 2){
            $("#po1").prop('checked',false);
            $("#po2").prop('checked',true);
            $("#po3").prop('checked',false);
        }else if(type_po == 3){
            $("#po1").prop('checked',false);
            $("#po2").prop('checked',false);
            $("#po3").prop('checked',true);
        }

        var incoming = {!! $incoming !!};
        var outgoing = {!! $outgoing !!};

        var html = '';
        var count = $('#table-client tbody tr').length+1;
        for(var i = 0; i<incoming.length; i++){
            if(incoming[i].form_id == form_id){
                if(incoming[i].status == '1'){
                    var status_in = 'checked';
                }else{
                    var status_in = '';
                }
                html += '<tr>'+
                            '<td>STAGE '+count+'</td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="date" name="expected_date_in[]" value="'+incoming[i].expected_date+'"></td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="text" name="amount_in[]" value="'+incoming[i].amount+'"></td>'+
                            '<td><div class="form-check"><input class="form-check-input" type="checkbox" id="status_'+i+'" name="in_status[]" value="1" '+status_in+'><label class="form-check-label" for="status_'+i+'"></label></div></td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="text" name="remarks[]" value="'+incoming[i].remarks+'"></td>'+
                            '<td style="display:none;"><input type="hidden" name="in_payment[]" value="'+incoming[i].id+'"></td>'+
                        '</tr>'; 
                count++;
            }
        }
        $('#table-client').empty();
        $('#table-client').append(html);

        var html1 = '';
        var index = $('#table-subcont tbody tr').length+1;
        for(var i = 0; i<outgoing.length; i++){
            if(outgoing[i].form_id == form_id){
                if(outgoing[i].status == '1'){
                    var status_out = 'checked';
                }else{
                    var status_out = '';
                }
                html1 += '<tr>'+
                            '<td>'+index+'</td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="text" name="subcont_name[]" value="'+outgoing[i].subcont_name+'"></td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="text" name="description[]" value="'+outgoing[i].description+'"></td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="date" name="expected_date_out[]" value="'+outgoing[i].expected_date+'"></td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="text" name="amount_out[]" value="'+outgoing[i].amount+'"></td>'+
                            '<td><div class="form-check"><input class="form-check-input" type="checkbox" id="status_outgoing'+i+'" name="out_status[]" value="1" '+status_out+'><label class="form-check-label" for="status_outgoing'+i+'"></label></div></td>'+
                            '<td><input class="form-control input-group-lg" autocomplete="off" type="text" name="outgoing_remarks[]" value="'+outgoing[i].remarks+'"></td>'+
                            '<td><input type="hidden" name="out_payment[]" value="'+outgoing[i].id+'"></td>'+
                        '</tr>';
                index++;
            }
        }
        $('#table-subcont').empty();
        $('#table-subcont').append(html1);
    });
</script>
@endpush