@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">STAFF MANAGEMENT UPDATE FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="form-group row" style="justify-content:center;grid-gap:30px">
                            <div class="col-md-2">
                                <a href="javascript:void(0)" class="col-md-12 btn btn-danger" data-toggle="modal" data-target="#addModal">Add New Staff</a>
                            </div>

                            <div class="col-md-2">
                                <a href="{{route('update')}}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>STAFF ID</th>
                                        <th>POSITION</th>
                                        <th>SALARY</th>
                                        <th>SITE ALLOWANCE</th>
                                        <th>MEDICAL CLAIM (BY MONTH)</th>
                                        <th>IN LIUE</th>
                                        <th>ANNUAL LEAVE</th>
                                        <th>MEDICAL LEAVE</th>
                                        <th>IN LIUE LEAVE</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" style="background-color:salmon">
        <h5 class="modal-title" style="color:white;">Adding Selected Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="staffAdd" method="POST" action="{{route('create.staff_management')}}">
        @csrf
        <div class="modal-body" style="margin-top:2%">
            <div class=" row">
                <label class="col-md-2">STAFF ID</label>
                <div class="form-group col-md-4">
                    <input class="form-control input-group-lg" autocomplete="off" type="text"name="staff_id">
                </div>
                <label for="" class="col-md-2">SALARY</label>
                <div class="form-group col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text">RM</div>
                        <input class="form-control input-group-lg" autocomplete="off" type="text" id="salary" name="salary">
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">PASSWORD</label>
                <div class="form-group col-md-4">
                    <input class="form-control input-group-lg" autocomplete="off" type="password" name="password">
                </div>
                <label for="" class="col-md-2">SITE ALLOWANCE</label>
                <div class="form-group col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text">RM</div>
                        <input class="form-control input-group-lg" autocomplete="off" type="text" id="site_allowance" name="site_allowance">
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">POSITION</label>
                <div class="form-group col-md-4">
                    <select class="form-control input-group-lg" id="add_position" name="position">
                        <option value="">Select Position</option>
                        <option value="Director">Director</option>
                        <option value="Finance">Finance</option>
                        <option value="HR">HR</option>
                        <option value="Operation (Service)">Operation (Service)</option>
                        <option value="Engineer">Engineer</option>
                        <option value="Trading">Trading</option>
                        <option value="Technicians">Technicians</option>
                        <option value="Clerk">Clerk</option>
                        <option value="Office Support">Office Support</option>
                        <option value="Contract Staff">Contract Staff</option>
                        <option value="Intern">Intern</option>
                    </select>
                </div>
                <label for="" class="col-md-2">MEDICAL CLAIM(MONTHLY ENTITLED)</label>
                <div class="form-group col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text">RM</div>
                        <input class="form-control input-group-lg" autocomplete="off" type="text" id="medical_claim" name="medical_claim">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>BRANCH LOCATION</label>
                        <select class="form-control input-group-lg" id="add_region" name="region">
                            <option value="">Select Branch Location</option>
                            <option value="KMM">Kemaman</option>
                            <option value="MLK">Melaka</option>
                            <option value="SBH">Sabah</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>VMS ACCESSIBILITY</label>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="accessibility[]" id="add_access1" value="1">
                                    <label class="form-check-label" for="add_access1">FORM</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="accessibility[]" id="add_access2" value="2">
                                    <label class="form-check-label" for="add_access2">STATUS</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="accessibility[]" id="add_access3" value="3" >
                                    <label class="form-check-label" for="add_access3">UPDATE</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="accessibility[]" id="add_access4" value="4" >
                                    <label class="form-check-label" for="add_access4">REPORT</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <div class="form-row" style="justify-content:center;grid-gap:20px;">
                        <div class="col-md-8" style="border:1px solid black;padding:15px">
                            <p style="font-weight:bold;width:100px;margin-top:-30px;margin-left:0px;background:white;text-align:center">IN LIUE</p>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="in_liue" id="add_liue1" value="1">
                                        <label class="form-check-label" for="add_liue1">APPLICABLE</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="in_liue" id="add_liue2" value="2">
                                        <label class="form-check-label" for="add_liue2">NON - APPLICABLE</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="form-row" style="justify-content:center;grid-gap:20px;">
                        <div class="col-md-8" style="border:1px solid black;padding:15px">
                            <p style="font-weight:bold;width:100px;margin-top:-30px;margin-left:0x;background:white;text-align:center">LEAVE</p>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-check-label" for="add_type1">ANNUAL</label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input class="form-control" autocomplete="off" type="text" name="annual_count">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-check-label" for="add_type2">MEDICAL</label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input class="form-control" autocomplete="off" type="text" name="medical_count">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-check-label" for="add_type3">IN LIUE</label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input class="form-control" autocomplete="off" type="text" name="liue_count">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 form-group row"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="staffModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" style="background-color:salmon">
        <h5 class="modal-title" style="color:white">Editing Selected Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="staffReq" method="POST">
        @csrf
        <div class="modal-body" style="margin-top:2%">
            <div class="row">
                <label class="col-md-2">STAFF ID</label>
                <div class="form-group col-md-4">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="staff_id" name="staff_id">
                </div>
                <label for="" class="col-md-2">SALARY</label>
                <div class="form-group col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text">RM</div>
                        <input class="form-control input-group-lg" autocomplete="off" type="text" id="salary" name="salary">
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">PASSWORD</label>
                <div class="form-group col-md-4">
                    <input class="form-control input-group-lg" autocomplete="off" type="password" id="password" name="password">
                </div>
                <label for="" class="col-md-2">SITE ALLOWANCE</label>
                <div class="form-group col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text">RM</div>
                        <input class="form-control input-group-lg" autocomplete="off" type="text" id="site_allowance" name="site_allowance">
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">POSITION</label>
                <div class="form-group col-md-4">
                    <select class="form-control input-group-lg" id="position" name="position">
                        <option value="">Select Position</option>
                        <option value="Director">Director</option>
                        <option value="Finance">Finance</option>
                        <option value="HR">HR</option>
                        <option value="Operation (Service)">Operation (Service)</option>
                        <option value="Engineer">Engineer</option>
                        <option value="Trading">Trading</option>
                        <option value="Technicians">Technicians</option>
                        <option value="Clerk">Clerk</option>
                        <option value="Office Support">Office Support</option>
                        <option value="Contract Staff">Contract Staff</option>
                        <option value="Intern">Intern</option>
                    </select>
                </div>

                <label for="" class="col-md-2">MEDICAL CLAIM(MONTHLY ENTITLED)</label>
                <div class="form-group col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text">RM</div>
                        <input class="form-control input-group-lg" autocomplete="off" type="text" id="medical_claim" name="medical_claim">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <div class="form-group">
                        <label>BRANCH LOCATION</label>
                        <select class="form-control input-group-lg" id="region" name="region">
                            <option value="">Select Branch Location</option>
                            <option value="KMM">Kemaman</option>
                            <option value="MLK">Melaka</option>
                            <option value="SBH">Sabah</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>VMS ACCESSIBILITY</label>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="accessibility[]" id="access1" value="1">
                                    <label class="form-check-label" for="access1">FORM</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="accessibility[]" id="access2" value="2">
                                    <label class="form-check-label" for="access2">STATUS</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="accessibility[]" id="access3" value="3" >
                                    <label class="form-check-label" for="access3">UPDATE</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="accessibility[]" id="access4" value="4" >
                                    <label class="form-check-label" for="access4">REPORT</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <div class="form-row" style="justify-content:center;grid-gap:20px;">
                        <div class="col-md-8" style="border:1px solid black;padding:15px">
                            <p style="font-weight:bold;width:100px;margin-top:-30px;margin-left:0px;background:white;text-align:center">IN LIUE</p>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="in_liue" id="liue1" value="1">
                                        <label class="form-check-label" for="liue1">APPLICABLE</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="in_liue" id="liue2" value="2">
                                        <label class="form-check-label" for="liue2">NON - APPLICABLE</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="form-row" style="justify-content:center;grid-gap:20px;">
                        <div class="col-md-8" style="border:1px solid black;padding:15px">
                            <p style="font-weight:bold;width:100px;margin-top:-30px;margin-left:0x;background:white;text-align:center">LEAVE</p>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-check-label" for="type1">ANNUAL</label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input class="form-control" autocomplete="off" type="text" id="annual_count" name="annual_count">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-check-label" for="type2">MEDICAL</label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input class="form-control" autocomplete="off" type="text" id="medical_count" name="medical_count">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-check-label" for="type3">IN LIUE</label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input class="form-control" autocomplete="off" type="text" id="liue_count" name="liue_count">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 form-group row"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    var current_month = '{!! $month !!}';
    $(() => {
        validateAddForm();
        validateEditForm();

        let table = $('#table');

        const formatter = new Intl.NumberFormat('ms-MY', {
            style: 'currency',
            currency: 'MYR',
            minimumFractionDigits: 2
        });

        table.DataTable({
            "dom" : '<"wrapper"fl<t>ip>',
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('staff_management.update') }}",
                type: 'GET',
            },
            columns: [
                {data: 'staff_id'},
                {data: 'position'},
                {
                    data: 'salary',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var amount = parseFloat(val);
                        var html = '<p>'+formatter.format(amount)+'</p>';
                        return html;
                    }
                },
                {
                    data: 'site_allowance',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var amount = parseFloat(val);
                        var html = '<p>'+formatter.format(amount)+'</p>';
                        return html;
                    }
                },
                {
                    data: 'id',
                    render: function(data, type, full){
                        // var val = data.replace(/[^0-9\.]/g,'');
                        if(current_month == 'January'){ var value = full.January;}
                        else if(current_month == 'February'){ var value = full.February;}
                        else if(current_month == 'March'){ var value = full.March;}
                        else if(current_month == 'April'){ var value = full.April;}
                        else if(current_month == 'May'){ var value = full.May;}
                        else if(current_month == 'June'){ var value = full.June;}
                        else if(current_month == 'July'){ var value = full.July;}
                        else if(current_month == 'August'){ var value = full.August;}
                        else if(current_month == 'September'){ var value = full.September;}
                        else if(current_month == 'October'){ var value = full.October;}
                        else if(current_month == 'November'){ var value = full.November;}
                        else{ var value = full.December;}
                        var amount = parseFloat(value);
                        var html = '<p>'+formatter.format(amount)+'</p>';
                        return html;
                    }
                },
                {data: 'in_liue'},
                {data: 'annual_leave'},
                {data: 'medical_leave'},
                {data: 'liue_leave'},
                {data: 'action', name: 'action', orderable: false},
            ],
            columnDefs: [
                {
                    "className": "text-center",
                    "data":'in_liue',
                    "targets":[5],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == 1 ? '<span class="text-secondary">APPLICABLE</span>' : '<span class="text-secondary">NON - APPLICABLE</span>';
                        }
                        return data;
                    }
                },
            ]
        }) 
    });

    let validateAddForm = () => {
        $('#staffAdd').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            }).then((result) => {
                                location.reload();
                            })
                        } else {
                            swal("Failed!", resp.msg, "error");
                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })    
            },
            rules: {
                staff_id : {
                    required:true
                },
                salary : {
                    required:true
                },
                password : {
                    required:true
                },
                site_allowance : {
                    required:true
                },
                position : {
                    required:true
                },
                medical_claim : {
                    required:true
                },
                region : {
                    required:true
                },
                in_liue : {
                    required:true
                },
                annual_count : {
                    required:true
                },
                medical_count : {
                    required:true
                },
                liue_count : {
                    required:true
                },
            }
        });
    }

    let validateEditForm = () => {
        $('#staffReq').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            }).then((result) => {
                                location.reload();
                            })
                        } else {
                            swal("Failed!", resp.msg, "error");
                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })    
            },
            rules: {
                staff_id : {
                    required:true
                },
                salary : {
                    required:true
                },
                site_allowance : {
                    required:true
                },
                position : {
                    required:true
                },
                medical_claim : {
                    required:true
                },
                region : {
                    required:true
                },
                in_liue : {
                    required:true
                },
                annual_count : {
                    required:true
                },
                medical_count : {
                    required:true
                },
                liue_count : {
                    required:true
                },
            }
        });
    }

    $(document).on('click', '.openModal', function(e) {
        var link = $(this);
        var id  = link.data("id");
        var staff_id = link.data("staff_id");
        var salary = link.data("salary");
        var site_allowance = link.data("site_allowance");
        var position = link.data("position");
        var medical_claim = link.data("medical_claim");
        var in_liue = link.data("in_liue");
        var annual_leave = link.data("annual_leave");
        var medical_leave = link.data("medical_leave");
        var liue_leave = link.data("liue_leave");
        var region = link.data("region");
        var accessibility = link.data("accessibility");

        var url = '{{route("save.staff_management", ":id")}}';
        var route = url.replace(":id",id);
        $('#staffReq').attr('action', route);

        $("#staffModal #staff_id").val(staff_id);
        $("#staffModal #salary").val(salary);
        $("#staffModal #site_allowance").val(site_allowance);
        $("#staffModal #position").val(position);
        $("#staffModal #medical_claim").val(medical_claim);
        $('#staffModal #position').val(position);
        $('#staffModal #region').val(region);

        if(in_liue == '1'){
            $('#staffModal #liue1').prop('checked',true);
        }
        if(in_liue == '2'){
            $('#staffModal #liue2').prop('checked',true);
        }

        var access = accessibility.toString().split(',');
        for(var i = 0; i<access.length; i++){
            if(access[i] == '1'){
                $('#staffModal #access1').prop('checked',true);
            }
            if(access[i] == '2'){
                $('#staffModal #access2').prop('checked',true);
            }
            if(access[i] == '3'){
                $('#staffModal #access3').prop('checked',true);
            }
            if(access[i] == '4'){
                $('#staffModal #access4').prop('checked',true);
            }
        }

        $('#staffModal #annual_count').val(annual_leave);
        $('#staffModal #medical_count').val(medical_leave);
        $('#staffModal #liue_count').val(liue_leave);
    });

    $('#addModal #salary').keyup(function() {
        var val = this.value;
        val = val.replace(/[^0-9\.]/g,'');
        
        if(val != "") {
            valArr = val.split('.');
            valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
            val = valArr.join('.');
        }
        
        this.value = val;
    });

    $('#addModal #site_allowance').keyup(function() {
        var val = this.value;
        val = val.replace(/[^0-9\.]/g,'');
        
        if(val != "") {
            valArr = val.split('.');
            valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
            val = valArr.join('.');
        }
        
        this.value = val;
    });

    $('#addModal #medical_claim').keyup(function() {
        var val = this.value;
        val = val.replace(/[^0-9\.]/g,'');
        
        if(val != "") {
            valArr = val.split('.');
            valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
            val = valArr.join('.');
        }
        
        this.value = val;
    });

    $('#staffModal #salary').keyup(function() {
        var val = this.value;
        val = val.replace(/[^0-9\.]/g,'');
        
        if(val != "") {
            valArr = val.split('.');
            valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
            val = valArr.join('.');
        }
        
        this.value = val;
    });

    $('#staffModal #site_allowance').keyup(function() {
        var val = this.value;
        val = val.replace(/[^0-9\.]/g,'');
        
        if(val != "") {
            valArr = val.split('.');
            valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
            val = valArr.join('.');
        }
        
        this.value = val;
    });

    $('#staffModal #medical_claim').keyup(function() {
        var val = this.value;
        val = val.replace(/[^0-9\.]/g,'');
        
        if(val != "") {
            valArr = val.split('.');
            valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
            val = valArr.join('.');
        }
        
        this.value = val;
    });
</script>
@endpush