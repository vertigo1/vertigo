@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">OFF IN LIUE UPDATE FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <a href="{{route('update')}}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>STAFF NAME</th>
                                        <th>DATE APPLY</th>
                                        <th>JOB NUMBER</th>
                                        <th>DATE WORK</th>
                                        <th>START HOUR</th>
                                        <th>END HOUR</th>
                                        <th>TOTAL HOUR</th>
                                        <th>JOB DESCRIPTION</th>
                                        <th>APPROVED</th>
                                        <th>REMARKS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="liueModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" style="background-color:salmon">
        <h5 class="modal-title" style="color:white">Editing Selected Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="inLiue" method="POST">
        @csrf
        <input id="work_id" name="work_id" type="hidden">
        <div class="modal-body" style="margin-top:2%">
            <div class="row">
                <label class="col-md-2">STAFF NAME</label>
                <div class="form-group col-md-4">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="staff_name" name="staff_name">
                </div>
                <label for="" class="col-md-2">DATE APPLY</label>
                <div class="form-group col-md-4">
                    <input class="form-control input-group-lg" autocomplete="off" type="date" id="date_apply" name="date_apply">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">JOB NUMBER</label>
                <div class="form-group col-md-4">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="job_number" name="job_number">
                </div>
                <label for="" class="col-md-2">JOB TITLE</label>
                <div class="form-group col-md-4">
                    <textarea class="form-control input-group-lg" autocomplete="off" type="text" id="job_title" rows="3" name="job_title" readonly></textarea>
                </div>
            </div>

            <div class="col-md-12 form-group row"></div>

            <div class="form-group row">
                <div class="table-responsive">
                    <table id="table-liue" class="table table-bordered" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="vertical-align:middle;text-align:center;width:10%">DATE WORK</th>
                                <th style="vertical-align:middle;text-align:center;width:10%">START HOUR</th>
                                <th style="vertical-align:middle;text-align:center;width:10%">END HOUR</th>
                                <th style="vertical-align:middle;text-align:center;width:15%">TOTAL HOUR</th>
                                <th style="vertical-align:middle;text-align:center;width:25%">JOB DESCRIPTION</th>
                                <th style="vertical-align:middle;text-align:center;width:5%">STATUS</th>
                                <th style="vertical-align:middle;text-align:center;width:25%">REMARKS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input class="form-control input-group-lg" autocomplete="off" type="date" id="date_work" name="date_work"></td>
                                <td><input class="form-control input-group-lg" autocomplete="off" type="time" id="start_hour" name="start_hour"></td>
                                <td><input class="form-control input-group-lg" autocomplete="off" type="time" id="end_hour" name="end_hour"></td>
                                <td><input class="form-control input-group-lg" autocomplete="off" type="text" id="total_hour" name="total_hour"></td>
                                <td><input class="form-control input-group-lg" autocomplete="off" type="text" id="job_description" name="job_description"></td>
                                <td>
                                    <input class="form-check-input" type="checkbox" id="status" name="status" value="1">
                                    <label class="form-check-label" for="status"></label>
                                </td>
                                <td><input class="form-control input-group-lg" autocomplete="off" type="text" id="remarks" name="remarks"></td>
                            </tr>
                        <tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    $(() => {
        validateForm();
        
        let table = $('#table');

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        table.DataTable({
            "dom" : '<"wrapper"fl<t>ip>',
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('in_liue.update') }}",
                type: 'GET',
            },
            columns: [
                {data: 'staff_id'},
                {
                    data: 'date_apply',
                    name: 'date_apply',
                    render: function(data, type, full){
                        var date_apply = new Date(data);
                        var htmlRender = '<p>'+date_apply.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {data: 'job_number'},
                {
                    data: 'date_work',
                    name: 'date_work',
                    render: function(data, type, full){
                        var date_work = new Date(data);
                        var htmlRender = '<p>'+date_work.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {data: 'start_hour'},
                {data: 'end_hour'},
                {data: 'total_hour'},
                {data: 'job_description'},
                {data: 'status'},
                {
                    data: 'remarks',
                    name: 'remarks',
                    render: function(data, type, full){
                        if(data == ''){
                            var htmlRender = '<p> - </p>';
                            return htmlRender;
                        }else{
                            var htmlRender = '<p>'+data+'</p>';
                            return htmlRender;
                        }
                    }
                },
                {data: 'action', name: 'action', orderable: false},
            ],
            columnDefs: [
                {
                    "className": "text-center",
                    "data":'status',
                    "targets":[8],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == 1 ? '<div class="btn btn-secondary btn-sm btn-icon mr-2"><i class="fas fa-check"></i></div>' : '<div class="btn btn-secondary btn-sm btn-icon mr-2"><i class="fas fa-times"></i></div>';
                        }
                        return data;
                    }
                },
            ]
        }) 
    });

    let validateForm = () => {
        $('#inLiue').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            }).then((result) => {
                                location.reload();
                            })
                        } else {
                            swal("Failed!", resp.msg, "error");
                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })    
            },
        });
    }

    $(document).on('click', '.openModal', function(e) {
        var link = $(this);
        var id  = link.data("id");
        var work_id  = link.data("work_id");
        var staff_name = link.data("staff_name");
        var date_apply = link.data("date_apply");
        var job_number = link.data("job_number");
        var job_title = link.data("job_title");
        var date_work = link.data("date_work");
        var start_hour = link.data("start_hour");
        var end_hour = link.data("end_hour");
        var total_hour = link.data("total_hour");
        var job_description = link.data("job_description");
        var status = link.data("status");
        var remarks = link.data("remarks");

        var url = '{{route("save.in_liue", ":id")}}';
        var route = url.replace(":id",id);
        $('#inLiue').attr('action', route);

        $("#work_id").val(work_id);
        $("#staff_name").val(staff_name);
        $("#date_apply").val(date_apply);
        $("#job_number").val(job_number);
        $("#job_title").val(job_title);
        $("#date_work").val(date_work);
        $("#start_hour").val(start_hour);
        $("#end_hour").val(end_hour);
        $("#total_hour").val(total_hour);
        $("#job_description").val(job_description);
        $("#remarks").val(remarks);

        if(status == 0){
            $("#status").prop('checked',false);   
        }else if(status == 1){
            $("#status").prop('checked',true);   
        }
    });
</script>
@endpush