@extends('layouts.app')
@push('styles')
<style>
 .card-no-border .card {
    background: grey;
    border-radius: 20px;
    box-shadow: 
    1px 1px 0px #000, 
    2px 2px 0px #000, 
    3px 3px 0px #000, 
    4px 4px 0px #000, 
    5px 5px 0px #000, 
    6px 6px 0px #000;
}
.text-danger {
    color: white !important;
    }
a {
    color: #ffff35;
    }
.text-centre {
    margin: auto;
    }

.card:hover{
   background-color: #5d5d5d;
}
h2:hover {
    color: black !important;
}
</style>
@endpush

@section('content')
<!-- <div class="row" style="padding-bottom:20px;padding-top:20px">
    <div class="col-md-12">
        <h3><strong>Update</strong></h3>
    </div>
</div> -->

<br>
<br>
<br>

@if(Auth::user()->position != 'HR')
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a href="{{route('purchase_order.update')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px;">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>UPDATE PURCHASE ORDER</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{route('purchase_request.update')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>UPDATE PURCHASE REQUEST</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endif

@if(Auth::user()->position == 'Director' || Auth::user()->position == 'Finance' || Auth::user()->position == 'HR')
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a href="{{route('leave_application.update')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>UPDATE LEAVE FORM</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                @if($inliue == 1)
                <a href="{{route('in_liue.update')}}">
                @else
                <a href="javascript:void(0)">
                @endif
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>UPDATE IN LIUE FORM</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endif

@if(Auth::user()->position != 'HR')
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom:30px;justify-content:center;">
            <div class="col-md-4">
                <a href="{{route('staff_management.update')}}">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row" style="padding-top:30px;padding-bottom:10px">
                                <div class="col-md-12 text-center">
                                    <h2 class="text-danger"><strong>MANAGE STAFF</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>
@endif

@endsection

@push('scripts')
@endpush