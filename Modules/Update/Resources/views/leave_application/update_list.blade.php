@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                            <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">STAFF LEAVE APPLICATION UPDATE FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <a href="{{route('update')}}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>STAFF NAME</th>
                                        <th>DATE APPLY</th>
                                        <th>START DATE</th>
                                        <th>END DATE</th>
                                        <th>TOTAL DAYS</th>
                                        <th>REASON</th>
                                        <th>LEAVE TYPE</th>
                                        <th>APPROVED DATE</th>
                                        <th>UPDATE DATE</th>
                                        <th>STATUS</th>
                                        <th>REMARKS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="leaveModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" style="background-color:salmon">
        <h5 class="modal-title" style="color:white">Editing Selected Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="leaveReq" method="POST">
        @csrf
        <div class="modal-body" style="margin-top:2%">
            <div class="row">
                <label class="col-md-2">STAFF NAME</label>
                <div class="form-group col-md-3">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="staff_name" name="staff_name">
                </div>
                <label for="" class="col-md-2">DATE APPLY</label>
                <div class="form-group col-md-5">
                    <input class="form-control input-group-lg" autocomplete="off" type="date" id="date_apply" name="date_apply">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">START DATE</label>
                <div class="form-group col-md-3">
                    <input class="form-control input-group-lg" autocomplete="off" type="date" id="start_date" name="start_date">
                </div>
                <label class="col-md-2">TOTAL DAYS</label>
                <label class="col-md-5">TYPE OF LEAVE (ANNUAL/EMERGENCY/SICK/UNPAID/OTHERS)</label>
            </div>

            <div class="row">
                <label class="col-md-2">END DATE</label>
                <div class="form-group col-md-3">
                    <input class="form-control input-group-lg" autocomplete="off" type="date" id="end_date" name="end_date">
                </div>
                <div class="form-group col-md-2">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="total_day" name="total_day">
                </div>
                <div class="form-group col-md-5">
                    <select class="form-control input-group-lg" id="type_leave" name="type_leave" disabled>
                        <option value="0">Annual Leave</option>
                        <option value="1">In Liue Leave</option>
                        <option value="2">Medical Leave</option>
                        <option value="3">Sick Leave</option>
                        <option value="4">Unpaid Leave</option>
                        <option value="5">Others</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">REASON</label>
                <div class="form-group col-md-10">
                    <textarea class="form-control" autocomplete="off" rows="5" id="reason" name="reason"></textarea>
                </div>
            </div>

            <div class="col-md-12 form-group row"></div>

            <div class="row">
                <div class="col-md-3">
                    <label class="col-md-12">APPROVED DATE</label>
                    <div class="form-group col-md-12">
                        <input class="form-control input-group-lg" autocomplete="off" type="date" id="approved_date" name="approved_date">
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="col-md-12">UPDATE DATE</label>
                    <div class="form-group col-md-12">
                        <input class="form-control input-group-lg" autocomplete="off" type="date" id="update_date" name="update_date">
                    </div>
                </div>
                <div class="col-md-1">
                    <label class="col-md-12">STATUS</label>
                    <div class="form-group col-md-12">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="status" name="status" value="1">
                            <label class="form-check-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <label class="col-md-12">REMARKS</label>
                    <div class="form-group col-md-12">
                        <input class="form-control input-group-lg" autocomplete="off" type="text" id="remarks" name="remarks">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    $(() => {
        validateForm();

        let table = $('#table');

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        table.DataTable({
            "dom" : '<"wrapper"fl<t>ip>',
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('leave_application.update') }}",
                type: 'GET',
            },
            columns: [
                {data: 'staff_id'},
                {
                    data: 'date_apply',
                    render: function(data, type, full){
                        var date_apply = new Date(data);
                        var htmlRender = '<p>'+date_apply.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {
                    data: 'start_date',
                    render: function(data, type, full){
                        var start_date = new Date(data);
                        var htmlRender = '<p>'+start_date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {
                    data: 'end_date',
                    render: function(data, type, full){
                        var end_date = new Date(data);
                        var htmlRender = '<p>'+end_date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {data: 'total_day'},
                {data: 'reason'},
                {data: 'type_leave'},
                {
                    data: 'approved_date',
                    render: function(data, type, full){
                        if(data == '0000-00-00'){
                            var htmlRender = '<p> - </p>';
                            return htmlRender;
                        }else{
                            var approved_date = new Date(data);
                            var htmlRender = '<p>'+approved_date.toLocaleDateString('ms-MY',option)+'</p>';
                            return htmlRender;
                        }
                    }
                },
                {
                    data: 'update_date',
                    render: function(data, type, full){
                        if(data == '0000-00-00'){
                            var htmlRender = '<p> - </p>';
                            return htmlRender;
                        }else{
                            var update_date = new Date(data);
                            var htmlRender = '<p>'+update_date.toLocaleDateString('ms-MY',option)+'</p>';
                            return htmlRender;
                        }
                    }
                },
                {data: 'status'},
                {data: 'remarks'},
                {data: 'action', name: 'action', orderable: false},
            ],
            columnDefs: [
                {
                    "className": "text-center",
                    "data":'type_leave',
                    "targets":[6],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {

                            return data == 0 ? '<span class="text-secondary">Annual Leave</span>' : data == 1 ? '<span class="text-secondary">In Liue Leave</span>' : data == 2 ? '<span class="text-secondary">Emergency Leave</span>' : 
                            data == 3 ? '<span class="text-secondary">Sick Leave</span>' : data == 4 ? '<span class="text-secondary">Unpaid Leave</span>' :  '<span class="text-secondary">Others</span>'  ;
                        }
                        return data;
                    }
                },
                {
                    "className": "text-center",
                    "data":'status',
                    "targets":[9],
                    "render": function (data, type, row, meta) {
                        console.log(data);
                        if (type == 'display') {
                            return data == 1 ? '<div class="btn btn-secondary btn-sm btn-icon mr-2"><i class="fas fa-check"></i></div>' : '<div class="btn btn-secondary btn-sm btn-icon mr-2"><i class="fas fa-times"></i></div>';
                        }
                        return data;
                    }
                },
                {
                    "className": "text-center",
                    "data":'remarks',
                    "targets":[10],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == '' ? '<span class="text-secondary"> - </span>' : '<span class="text-secondary">'+data+'</span>';
                        }
                        return data;
                    }
                },
            ]
        }) 
    });

    let validateForm = () => {
        $('#leaveReq').validate({
            submitHandler: (form, e) => {
                e.preventDefault();

                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: (resp) => {
                        if(true === resp.status) {
                            swal({
                                title: 'Success!',
                                text: resp.msg,
                                icon: 'success'
                            }).then((result) => {
                                location.reload();
                            })
                        } else {
                            swal("Failed!", resp.msg, "error");
                        }
                    },
                    error: (err) => {
                        swal("Failed!", err, "error");
                    },
                    complete: () => {
                    }
                })
            },
        });
    }

    function myfunc(){
        var start= $("#start_date").val();
        var end= $("#end_date").val();
        var diff = new Date(Date.parse(end) - Date.parse(start));

        var days = diff/1000/60/60/24;

        $("#total_day").prop('value', days);
    }

    $("#start_date").on('change', () => {
        $("#end_date").prop({"readonly": false, "value": ''});
        $("#total_day").prop('value', '');
    })

    $("#end_date").on('change', () => {
        let edate = $("#end").val();
        myfunc();
    })

    $(document).on('click', '.openModal', function(e) {
        var link = $(this);
        var id  = link.data("id");
        var staff_name = link.data("staff_name");
        var date_apply = link.data("date_apply");
        var start_date = link.data("start_date");
        var end_date = link.data("end_date");
        var total_day = link.data("total_day");
        var type_leave = link.data("type_leave");
        console.log(type_leave);
        var reason = link.data("reason");
        var approved_date = link.data("approved_date");
        var update_date = link.data("update_date");
        var status = link.data("status");
        var remarks = link.data("remarks");

        var url = '{{route("save.leave_application", ":id")}}';
        var route = url.replace(":id",id);
        $('#leaveReq').attr('action', route);

        $("#staff_name").val(staff_name);
        $("#date_apply").val(date_apply);
        $("#start_date").val(start_date);
        $("#end_date").val(end_date);
        $("#total_day").val(total_day);
        $("#type_leave").val(type_leave);
        $("#reason").text(reason);
        $("#approved_date").val(approved_date);
        $("#update_date").val(update_date);
        $("#remarks").val(remarks);

        if(status == 1){
            $("#status").prop('checked',true);
        }else{
            $("#status").prop('checked',false);
        }
    });
</script>
@endpush