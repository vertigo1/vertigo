@extends('form::layouts.master')
@push('styles')
<style>
    .pagination{
        justify-content: center !important;
    }
    @media screen and (max-width: 767px) {
        p{
            text-align:center !important;
            transform:none !important;
            margin:1% !important;
            padding:1% !important;
        }
    }
</style>
@endpush

@section('content')           
<br>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:2px solid lightgrey;">
                    <div class="card-header" style="padding:0;height:100%;text-align:right;">
                        <div class="row" style="margin:0;">
                        <div class="col-md-2" style="text-align:center;background-color:#868686;padding-left:1.1%;">
                                <img width="100%" height="100%" src="{{asset('img/Logo 2 VMS.jpg')}}">
                            </div>
                            <div class="col-md-6" style="text-align:left;font-size:25px;background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:2%;font-size:40px;">PURCHASE REQUEST UPDATE FORM</p>
                                <p style="font-weight:bold;margin-top:4%;font-size:25px;">VERTIGO MANAGEMENT SYSTEM (VMS)</p>
                            </div>
                            <div class="col-md-4" style="background-color:#868686;color:white">
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->isoFormat('dddd, D MMMM YYYY')}}</p>
                                <p style="font-weight:bold;margin-top:5%;">{{\Carbon\Carbon::parse(now())->format('h:i:s A')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-top:2%">
                        <div class="form-group">
                            <div class="col-md-2">
                                <a href="{{route('update')}}" class="col-md-12 btn btn-outline-danger">Homepage</a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                <thead class="thead-theme">
                                    <tr>
                                        <th>ID</th>
                                        <th>DATE</th>
                                        <th>REQUESTOR</th>
                                        <th>JOB NUMBER</th>
                                        <th>SUPPLIER</th>
                                        <th>AMOUNT</th>
                                        <th>PURPOSE DESCRIPTION</th>
                                        <th>PAID</th>
                                        <th>PAYMENT DATE</th>
                                        <th>REMARKS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="requestModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" style="background-color:salmon">
        <h5 class="modal-title" style="color:white">Editing Selected Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form enctype='multipart/form-data' id="purchaseReq" method="POST">
        @csrf
        <div class="modal-body" style="margin-top:2%">
            <div class="row">
                <label class="col-md-2">REQUESTOR</label>
                <div class="form-group col-md-3">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="requestor" name="requestor">
                </div>
                <label for="" class="col-md-2">SUPPLIER</label>
                <div class="form-group col-md-5">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="supplier" name="supplier">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">JOB NUMBER</label>
                <div class="form-group col-md-3">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="job_number" name="job_number">
                </div>
                <label for="" class="col-md-2">JOB TITLE</label>
                <div class="form-group col-md-5">
                    <textarea class="form-control input-group-lg" autocomplete="off" type="text" id="job_title" rows="3" name="job_title" readonly></textarea>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">DATE</label>
                <div class="form-group col-md-3">
                    <input class="form-control input-group-lg" autocomplete="off" type="date" id="date" name="date">
                </div>
                <label for="" class="col-md-2">PURPOSE DESCRIPTION</label>
                <div class="form-group col-md-5">
                    <input class="form-control input-group-lg" autocomplete="off" type="text" id="description" name="description">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2">AMOUNT</label>
                <div class="form-group col-md-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text">RM</div>
                        <input class="form-control input-group-lg" autocomplete="off" type="text" id="amount" name="amount">
                    </div>
                </div>
            </div>

            <div class="col-md-12 form-group row"></div>

            <div class="row">
                <div class="form-group col-md-3 div-bank_slip" style="text-align:center;"></div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label class="col-md-12">PAYMENT DATE</label>
                        <div class="col-md-12">
                            <input class="form-control input-group-lg" autocomplete="off" type="date" id="payment_date" name="payment_date">
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label class="col-md-12">PAYMENT STATUS</label>
                        <div class="col-md-12 row">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="payment_status" id="payment_1" value="1">
                                <label class="form-check-label" for="payment_1">APPROVED</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="payment_status" id="payment_2" value="2">
                                <label class="form-check-label" for="payment_2">REJECT</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12">PAYMENT REMARKS</label>
                        <div class="col-md-12">
                            <input class="form-control input-group-lg" autocomplete="off" type="text" id="payment_remarks" name="payment_remarks" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    $(() => {
        let table = $('#table');

        const formatter = new Intl.NumberFormat('ms-MY', {
            style: 'currency',
            currency: 'MYR',
            minimumFractionDigits: 2
        });

        const option =  { year: 'numeric', month: 'numeric', day: 'numeric' };

        table.DataTable({
            "dom" : '<"wrapper"fl<t>ip>',
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('purchase_request.update') }}",
                type: 'GET',
            },
            columns: [
                {data: 'id'},
                {
                    data: 'date',
                    name: 'date',
                    render: function(data, type, full){
                        var date = new Date(data);
                        var htmlRender = '<p>'+date.toLocaleDateString('ms-MY',option)+'</p>';
                        return htmlRender;
                    }
                },
                {data: 'staff_id'},
                {data: 'job_number'},
                {data: 'supplier'},
                {
                    data: 'amount', 
                    name: 'amount',
                    render: function(data, type, full){
                        var val = data.replace(/[^0-9\.]/g,'');
                        var amount = parseFloat(val);
                        var html = '<p>'+formatter.format(amount)+'</p>';
                        return html;
                    }
                },
                {data: 'description'},
                {data: 'payment_status'},
                {
                    data: 'payment_date',
                    name: 'payment_date',
                    render: function(data, type, full){
                        if(data == '0000-00-00'){
                            var htmlRender = '<p> - </p>';
                            return htmlRender;
                        }else{
                            var payment_date = new Date(data);
                            var htmlRender = '<p>'+payment_date.toLocaleDateString('ms-MY',option)+'</p>';
                            return htmlRender;
                        }
                    }
                },
                {data: 'payment_remarks'},
                {data: 'action', name: 'action', orderable: false},
            ],
            columnDefs: [
                {
                    "className": "text-center",
                    "data":'payment_status',
                    "targets":[7],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            if(data == 0){
                                return '<span class="text-danger">No</span>';
                            }else if(data == 1){
                                return '<span class="text-success">Yes</span>';
                            }else if(data == 2){
                                return '<span class="text-danger">Reject</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    "className": "text-center",
                    "data":'payment_remarks',
                    "targets":[9],
                    "render": function (data, type, row, meta) {
                        if (type == 'display') {
                            return data == '' ? '<span class="text-secondary"> - </span>' : '<span class="text-secondary">'+data+'</span>';
                        }
                        return data;
                    }
                },
            ]
        }) 
    });

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $(document).on('click', '.openModal', function(e) {
        var link = $(this);
        var id  = link.data("id");
        var requestor = link.data("requestor");
        var supplier = link.data("supplier");
        var job_number = link.data("job_number");
        var job_title = link.data("job_title");
        var date = link.data("date");
        var description = link.data("description");
        var amount = link.data("amount");
        var bank_slip = link.data("bank_slip");
        var payment_date = link.data("payment_date");
        var payment_status = link.data("payment_status");
        var payment_remarks = link.data("payment_remarks");

        if(bank_slip != ''){
            var html = `<div class="custom-file">
                            <div>
                                <i class="fas fa-file fa-7x"></i>
                            </div>
                            <div>
                                <a target="_blank" href="{{asset('storage/bank_slip/`+bank_slip+`')}}">`+bank_slip+`</a>
                            </div>
                        </div>`;
        }else{
            var html = `<div class="custom-file">
                            <input type="file" class="custom-file-input" id="bank_slip" name="bank_slip">
                            <label class="custom-file-label" for="bank_slip">Upload Bank Slip</label>
                        </div>`;
        }

        $('.div-bank_slip').empty();
        $('.div-bank_slip').append(html);

        var url = '{{route("save.request", ":id")}}';
        var route = url.replace(":id",id);
        $('#purchaseReq').attr('action', route);

        $("#requestor").val(requestor);
        $("#supplier").val(supplier);
        $("#job_number").val(job_number);
        $("#job_title").val(job_title);
        $("#date").val(date);
        $("#description").val(description);
        $("#amount").val(amount);
        $("#payment_date").val(payment_date);
        $("#payment_remarks").val(payment_remarks);

        if(payment_status == 1){
            $("#payment_1").attr('checked',true);
            $("#payment_2").attr('checked',false);
        }else if(payment_status == 2){
            $("#payment_1").attr('checked',false);
            $("#payment_2").attr('checked',true);
        }else if(payment_status == 0){
            $("#payment_1").attr('checked',false);
            $("#payment_2").attr('checked',false);
        }
    });
</script>
@endpush