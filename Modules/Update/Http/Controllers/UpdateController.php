<?php

namespace Modules\Update\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Form\Entities\PurchaseOrder;
use Modules\Form\Entities\PurchaseRequest;
use Modules\Form\Entities\LeaveApplication;
use Modules\Form\Entities\InLiue;
use Modules\Form\Entities\IncomingPayment;
use Modules\Form\Entities\OutgoingPayment;
use Modules\Form\Entities\Work;
use Modules\Form\Entities\MedicalAmount;
use App\User;
use Auth, DB, DataTables, File, Hash;

class UpdateController extends Controller
{
    public function index(){
        return view('update::index');
    }

    public function update_purchase_order(){
        $incoming = IncomingPayment::all();
        $outgoing = OutgoingPayment::all();
        if(request()->ajax()) {
            $data = PurchaseOrder::all();

            return Datatables::of($data)->addIndexColumn()->addColumn('action', function($row){
                        $list = 'data-id="'.$row->id.'"
                                data-form_id="'.$row->form_id.'"
                                data-job_number="'.$row->job_number.'" 
                                data-client_ref_no="'.$row->client_ref_no.'" 
                                data-client_name="'.$row->client_name.'" 
                                data-job_title="'.$row->job_title.'"
                                data-po_date="'.$row->po_date.'"
                                data-po_value="'.$row->po_value.'" 
                                data-po_status="'.$row->po_status.'"
                                data-pic="'.$row->staff_id.'"
                                data-type_po="'.$row->type_po.'"';
                        $btn = '<div class="row"><a href="javascript:void(0)" '.$list.' data-toggle="modal" data-target="#orderModal" class="btn btn-primary btn-sm btn-icon mr-2 openModal"><i class="fas fa-pencil-alt"></i></a></div>';
                        return $btn;
                    })->rawColumns(['action'])->make(true);
        }
        return view('update::purchase_order.update_list',compact('incoming','outgoing'));
    }

    public function save_order($id, Request $request) {
        try {
            $data = PurchaseOrder::find($id);

            $data->job_number = $request->job_number;
            $data->client_ref_no = $request->client_ref_no;
            $data->client_name = $request->client_name;
            $data->job_title = $request->job_title;
            $data->po_date = $request->po_date;
            $data->po_value = $request->po_value;
            $data->po_status = $request->po_status;
            $data->staff_id = $request->pic;
            $data->type_po = $request->type_po;
            $saved = $data->save();

            for($i = 0; $i<count($request->in_payment); $i++){
                $incoming = IncomingPayment::find($request->in_payment[$i]);

                $incoming->expected_date = $request->expected_date_in[$i];
                $incoming->amount = $request->amount_in[$i];
                $incoming->status = $request->in_status[$i];
                $incoming->remarks = $request->remarks[$i];

                $incoming->save();
            }

            for($i = 0; $i<count($request->out_payment); $i++){
                $outgoing = OutgoingPayment::find($request->out_payment[$i]);

                $outgoing->subcont_name = $request->subcont_name[$i];
                $outgoing->description = $request->description[$i];
                $outgoing->expected_date = $request->expected_date_out[$i];
                $outgoing->amount = $request->amount_out[$i];
                $outgoing->status = $request->out_status[$i];
                $outgoing->remarks = $request->outgoing_remarks[$i];

                $outgoing->save();
            }

            if($saved) {
                $status = true;
                $msg = 'Success! Purchase Order updated.';
                $url = route('purchase_order.update');
            }
        }catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }

        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function update_purchase_request(){
        if(request()->ajax()) {
            $data = PurchaseRequest::all();

            return Datatables::of($data)->addIndexColumn()->addColumn('action', function($row){
                        $job = PurchaseOrder::where('job_number',$row->job_number)->first();
                        $job_title = $job->job_title;

                        $list = 'data-id="'.$row->id.'"
                                 data-requestor="'.$row->staff_id.'" 
                                 data-supplier="'.$row->supplier.'" 
                                 data-job_number="'.$row->job_number.'" 
                                 data-job_title="'.$job_title.'" 
                                 data-date="'.$row->date.'" 
                                 data-description="'.$row->description.'" 
                                 data-amount="'.$row->amount.'"
                                 data-bank_slip="'.$row->bank_slip.'"
                                 data-payment_date="'.$row->payment_date.'"
                                 data-payment_status="'.$row->payment_status.'"
                                 data-payment_remarks="'.$row->payment_remarks.'"';
                        $btn = '<div class="row"><a href="javascript:void(0)" '.$list.' data-toggle="modal" data-target="#requestModal" class="btn btn-primary btn-sm btn-icon mr-2 openModal"><i class="fas fa-pencil-alt"></i></a></div>';
                        return $btn;
                    })->rawColumns(['action'])->make(true);
        }
        return view('update::purchase_request.update_list');
    }

    public function save_request($id, Request $request) {
        $data = PurchaseRequest::find($id);

        if($request->bank_slip != null){
            if($data->bank_slip != null){
                $delete = public_path('storage/bank_slip/'.$data->bank_slip);
                File::delete($delete);
            }
            $bank_slip = 'bank_slip-'.time(). '.' .$request->bank_slip->getClientOriginalExtension();
            $request->bank_slip->move(public_path('storage/bank_slip/'), $bank_slip);
            $data->bank_slip = $bank_slip;
        }
        $data->staff_id = $request->requestor;
        $data->supplier = $request->supplier;
        $data->job_number = $request->job_number;
        $data->date = $request->date;
        $data->description = $request->description;
        $data->amount = $request->amount;
        $data->payment_date = $request->payment_date;
        $data->payment_status = $request->payment_status;
        $data->payment_remarks = $request->payment_remarks;
        $data->save();

        return redirect('/update/purchase_request/update');
    }

    public function update_leave_application(){
        if(request()->ajax()) {
            $data = LeaveApplication::all();

            return Datatables::of($data)->addIndexColumn()->addColumn('action', function($row){
                        $list = 'data-id="'.$row->id.'"
                                data-staff_name="'.$row->staff_id.'"
                                data-date_apply="'.$row->date_apply.'"
                                data-start_date="'.$row->start_date.'"
                                data-end_date="'.$row->end_date.'"
                                data-total_day="'.$row->total_day.'"
                                data-type_leave="'.$row->type_leave.'"
                                data-reason="'.$row->reason.'"
                                data-approved_date="'.$row->approved_date.'"
                                data-update_date="'.$row->update_date.'"
                                data-status="'.$row->status.'"
                                data-remarks="'.$row->remarks.'"';
                        $btn = '<div class="row"><a href="javascript:void(0)" '.$list.' data-toggle="modal" data-target="#leaveModal" class="btn btn-primary btn-sm btn-icon mr-2 openModal"><i class="fas fa-pencil-alt"></i></a></div>';
                        return $btn;
                    })->rawColumns(['action'])->make(true);
        }
        return view('update::leave_application.update_list');
    }

    public function save_leave_application($id, Request $request) {
        try {
            $data = LeaveApplication::find($id);

            $data->staff_id = $request->staff_name;
            $data->date_apply = $request->date_apply;
            $data->start_date = $request->start_date;
            $data->end_date = $request->end_date;
            $data->total_day = $request->total_day;
            // $data->type_leave = $request->type_leave;
            $data->reason = $request->reason;
            $data->approved_date = $request->approved_date;
            $data->update_date = $request->update_date;
            $data->status = $request->status;
            $data->remarks = $request->remarks;
            $saved = $data->save();

            $user = User::where('staff_id',$request->staff_name)->get()->first();

            if($request->status == 1){
                if($data->type_leave == 0){
                    $total = $request->total_day;
                    $user->annual_leave = intval($user->annual_leave) - intval($total);
                }else if($data->type_leave == 1){
                    $total = $request->total_day;
                    $user->liue_leave = intval($user->liue_leave) - intval($total);
                }else if($data->type_leave == 2) {
                    $total = $request->total_day;
                    $user->medical_leave = intval($user->medical_leave) - intval($total);
                }else if($data->type_leave == 3) {
                    $total = $request->total_day;
                    $user->medical_leave = intval($user->medical_leave) - intval($total);   
                }
            }
            $user->save();

            if($saved) {
                $status = true;
                $msg = 'Success! Leave Application updated.';
                $url = route('leave_application.update');
            }
        }catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }
        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function update_in_liue(){
        if(request()->ajax()) {
            $data = DB::table('off_in_liue')->join('work', 'work.form_id', '=', 'off_in_liue.form_id')->get();

            return Datatables::of($data)->addIndexColumn()->addColumn('action', function($row){
                        $job = PurchaseOrder::where('job_number',$row->job_number)->first();
                        $job_title = $job->job_title;

                        $liue = InLiue::where('form_id',$row->form_id)->first();
                        $work = Work::find($row->id);

                        $list = 'data-id="'.$liue->id.'"
                                data-work_id="'.$work->id.'"
                                data-staff_name="'.$row->staff_id.'" 
                                data-date_apply="'.$row->date_apply.'" 
                                data-job_number="'.$row->job_number.'" 
                                data-job_title="'.$job_title.'"
                                data-date_work="'.$row->date_work.'"
                                data-start_hour="'.$row->start_hour.'" 
                                data-end_hour="'.$row->end_hour.'"
                                data-total_hour="'.$row->total_hour.'" 
                                data-job_description="'.$row->job_description.'"
                                data-status="'.$row->status.'"
                                data-remarks="'.$row->remarks.'"';
                        $btn = '<div class="row"><a href="javascript:void(0)" '.$list.' data-toggle="modal" data-target="#liueModal" class="btn btn-primary btn-sm btn-icon mr-2 openModal"><i class="fas fa-pencil-alt"></i></a></div>';
                        return $btn;
                    })->rawColumns(['action'])->make(true);
        }
        return view('update::in_liue.update_list');
    }

    public function save_in_liue($id, Request $request) {
        try {
            $data = InLiue::find($id);
            $work = Work::find($request->work_id);

            $data->staff_id = $request->staff_name;
            $data->job_number = $request->job_number;
            $data->date_apply = $request->date_apply;

            $work->date_work = $request->date_work;
            $work->start_hour = $request->start_hour;
            $work->end_hour = $request->end_hour;
            $work->total_hour = $request->total_hour;
            $work->job_description = $request->job_description;
            $work->remarks = $request->remarks;
            $work->status = $request->status;
            $data->save();
            $saved = $work->save();

            if($saved) {
                $status = true;
                $msg = 'Success! In Liue updated.';
                $url = route('in_liue.update');
            }
        }catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }
        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function update_staff_management(){
        $month = now()->monthName;
        if(request()->ajax()) {
            $data = DB::table('profile')->join('medical_amount', 'medical_amount.staff_id', '=', 'profile.staff_id')
                    ->select('profile.*','medical_amount.January','medical_amount.February','medical_amount.March','medical_amount.April','medical_amount.May','medical_amount.June','medical_amount.July','medical_amount.August','medical_amount.September','medical_amount.October','medical_amount.November','medical_amount.December')
                    ->distinct('staff_id')->get();

            return Datatables::of($data)->addIndexColumn()->addColumn('action', function($row){
                        if(now()->monthName == 'January'){$medical = $row->January;}
                        else if(now()->monthName == 'February'){$medical = $row->February;}
                        else if(now()->monthName == 'March'){$medical = $row->March;}
                        else if(now()->monthName == 'April'){$medical = $row->April;}
                        else if(now()->monthName == 'May'){$medical = $row->May;}
                        else if(now()->monthName == 'June'){$medical = $row->June;}
                        else if(now()->monthName == 'July'){$medical = $row->July;}
                        else if(now()->monthName == 'August'){$medical = $row->August;}
                        else if(now()->monthName == 'September'){$medical = $row->September;}
                        else if(now()->monthName == 'October'){$medical = $row->October;}
                        else if(now()->monthName == 'November'){$medical = $row->November;}
                        else{$medical = $row->December;}

                        $list = 'data-id="'.$row->id.'"
                                 data-staff_id="'.$row->staff_id.'"
                                 data-salary="'.$row->salary.'"
                                 data-site_allowance="'.$row->site_allowance.'"
                                 data-position="'.$row->position.'"
                                 data-medical_claim="'.$medical.'"
                                 data-in_liue="'.$row->in_liue.'"
                                 data-annual_leave="'.$row->annual_leave.'"
                                 data-medical_leave="'.$row->medical_leave.'"
                                 data-liue_leave="'.$row->liue_leave.'"
                                 data-region="'.$row->region.'"
                                 data-accessibility="'.$row->accessibility.'"';

                        $url = route("delete.staff_management",['id' => $row->id]);

                        $btn = '<div class="row"> <a href="javascript:void(0)" '.$list.' data-toggle="modal" data-target="#staffModal" class="btn btn-primary btn-sm btn-icon mr-2 openModal"><i class="fas fa-pencil-alt"></i></a>
                                <a href="'.$url.'" class="btn btn-danger btn-sm btn-icon mr-2"><i class="fas fa-trash-alt"></i></a></div>';
                        return $btn;
                    })->rawColumns(['action'])->make(true);
        }
        return view('update::staff_management.update_list',compact('month'));
    }

    public function save_staff_management($id, Request $request) {
        try {
            $data = User::find($id);

            $data->staff_id = $request->staff_id;
            $data->salary = $request->salary;
            if($request->password != null){
                $data->password = Hash::make($request->password);
            }
            $data->site_allowance = $request->site_allowance;
            $data->position = $request->position;
            if($data->isDirty('medical_claim')){
                $data->medical_claim = $request->medical_claim;
            }
            $data->region = $request->region;
            $data->accessibility = implode(',',$request->accessibility);
            $data->in_liue = $request->in_liue;
            $data->annual_leave = $request->annual_count;
            $data->medical_leave = $request->medical_count;
            $data->liue_leave = $request->liue_count;
            $saved = $data->save();

            $medical = MedicalAmount::where('staff_id',$request->staff_id)->get()->first();
            
            $medical->January = $request->medical_claim;
            $medical->February = $request->medical_claim;
            $medical->March = $request->medical_claim;
            $medical->April = $request->medical_claim;
            $medical->May = $request->medical_claim;
            $medical->June = $request->medical_claim;
            $medical->July = $request->medical_claim;
            $medical->August = $request->medical_claim;
            $medical->September = $request->medical_claim;
            $medical->October = $request->medical_claim;
            $medical->November = $request->medical_claim;
            $medical->December = $request->medical_claim;

            $medical->save();

            if($saved) {
                $status = true;
                $msg = 'Success! Staff updated.';
                $url = route('staff_management.update');
            }
        }
        catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }
        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function create_staff_management(Request $request){
        try{
            $data = new User();

            $data->staff_id = $request->staff_id;
            $data->salary = $request->salary;
            $data->password = Hash::make($request->password);
            $data->site_allowance = $request->site_allowance;
            $data->position = $request->position;
            $data->medical_claim = $request->medical_claim;
            $data->region = $request->region;
            $data->accessibility = implode(',',$request->accessibility);
            $data->in_liue = $request->in_liue;
            if($request->annual_count != null){
                $data->annual_leave = $request->annual_count;
            }else{
                $data->annual_leave = 0;
            }

            if($request->medical_count != null){
                $data->medical_leave = $request->medical_count;
            }else{
                $data->medical_leave = 0;
            }

            if($request->liue_count != null){
                $data->liue_leave = $request->liue_count;
            }else{
                $data->liue_leave = 0;
            }
            $saved = $data->save();

            $medical = new MedicalAmount();
            $medical->staff_id = $request->staff_id;
            $medical->January = $request->medical_claim;
            $medical->February = $request->medical_claim;
            $medical->March = $request->medical_claim;
            $medical->April = $request->medical_claim;
            $medical->May = $request->medical_claim;
            $medical->June = $request->medical_claim;
            $medical->July = $request->medical_claim;
            $medical->August = $request->medical_claim;
            $medical->September = $request->medical_claim;
            $medical->October = $request->medical_claim;
            $medical->November = $request->medical_claim;
            $medical->December = $request->medical_claim;
            $medical->save();

            if($saved) {
                $status = true;
                $msg = 'Success! New Staff created.';
                $url = route('staff_management.update');
            }
        }
        catch(\Exception $e) {
            $status = false;
            $msg = $e->getMessage();
            $url = null;
        }
        return response()->json(['status' => $status, 'msg' => $msg, 'url' => $url]);
    }

    public function delete_staff_management($id){
        $data = User::find($id);
        $data->delete();

        return redirect('/update/staff_management/update');
    }
}
